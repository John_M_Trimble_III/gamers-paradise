Rails.application.routes.draw do
  mount ActionCable.server => "/cable"

  root 'video_games#explore'
  get '/sitemap', to: 'static_pages#sitemap'


  # Static Pages
  match 'privacy' => 'static_pages#privacy', via: :get
  match 'about_us' => 'static_pages#about_us', via: :get

  get 'feed', :to => "feed#index"
  resources :video_games do 
    get :history, on: :member

  	resources :listings
    resources :critic_reviews
  end
  
  match 'select2_search' => 'video_games#select_search', :via => :get
  	
  resources :drafts do 
    post :accept, on: :member
    post :deny, on: :member
  end
  
  resources :users, only: [] do
      resources :shelves
  end
  
  resources :shelves, only: [:show]


  resources :shelved_games


  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  devise_scope :user do
    match 'users/omniuath_finish_signup' => 'users/omniauth_callbacks#finish_signup', via: :get
    match 'users/omniuath_create_signup' => 'users/omniauth_callbacks#create_signup', via: :post
  end



  resources :gifs, only: [:index]

  resources :franchises

  resources :series

  resources :platforms

  resources :game_modes

  resources :game_engines

  resources :ratings

  resources :genres

  resources :player_perspectives

  resources :companies

  resources :follows do 
    get :followers, on: :member
  end

  resources :questions do 
    resources :answers
  end 

  resources :lists do 
    resources :list_video_games
  end

  resources :users do 
    post 'mark_as_favorite/:video_game_id', to: 'users#mark_game_as_favorite', as: :mark_as_favorite
    post 'unmark_as_favorite', to: 'users#unmark_game_as_favorite', as: :unmark_as_favorite    

    post 'update_avatar', to: 'users#update_avatar'
    post 'remove_avatar', to: 'users#remove_avatar'
    post 'update_banner', to: 'users#update_banner'
    post 'remove_banner', to: 'users#remove_banner'


  end

  match 'leaderboard' => 'users#leaderboard', :via => :get
  match 'scorecard' => 'users#scorecard', :via => :get
  match 'mark_as_seen/:id' => 'rewards#mark_as_seen', via: :post
  match 'rewards/mark_all_as_seen' => 'rewards#mark_all_as_seen', via: :post

  resources :comments 

  resources :pages

  resources :groups do 
    resources :memberships, shallow: true do 
      post :approve, on: :member
      post :revoke, on: :member
      post :promote, on: :member
      post :demote, on: :member
    end
  end

  resources :friendships do
    post :accept, on: :member
    post :deny, on: :member
  end

  resources :member_reviews
  # Social Shit
  resources :posts
  resources :attachments, only: [:create, :destroy]

  resources :topics do 
    resources :topic_comments, shallow: true
  end


  resources :referrals, only: [:index, :show]

  get 'twitch/videos', :to => 'twitch#get_videos'


  get '/search/', to: 'search#index', as: 'search'


  # Shit for Notifications
  resources :notifications, only: [:show, :delete] do 
    post :mark_as_read
  end
  match 'mark_all_as_read' => 'notifications#mark_all_as_read', :via => :post


  resources :list_video_games, only: [:show]
  resources :answers, only: [:show]
  resources :likes, only: [:show]
  resources :contributions, only: [:show, :index] do 
      get :breakdown, on: :member
  end

  match 'like_thing', to: 'likes#like', via: :post
  match 'unlike_thing', to: 'likes#unlike', via: :delete
  
  match 'upvote_thing', to: 'votes#upvote', via: :post
  match 'unvote_thing', to: 'votes#unvote', via: :delete



  namespace :admin do
    root :to => 'dashboard#index'
    resources :blog_posts
    resources :blog_categories
    resources :users
  end

  resources :blog, only: [:index, :show]

  # match 'wikis' => 'wikis#index', via: [:get, :post, :put, :patch, :delete]  
  resources :wikis, except: [:show]
  match 'wikis/:id' => 'wikis#show', via: [:get, :post, :put, :patch, :delete]
  match 'wikis/:id/*path' => 'wikis#show', via: [:get, :post, :put, :patch, :delete]
  match 'store/*path' => 'store#index', via: [:get, :post, :put, :patch, :delete]
  match 'store' => 'store#index', via: [:get, :post, :put, :patch, :delete]
  post '/tinymce_assets' => 'tinymce_assets#create'


 scope '/api' do
    scope '/v1' do
      scope '/orders' do
        post '/' => 'api_orders#create'
      end
    end
  end

require "sidekiq/web"

mount Sidekiq::Web => "/sidekiq"

end
