# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.gamersparadise.io"
SitemapGenerator::Sitemap.create_index = true

SitemapGenerator::Sitemap.adapter = SitemapGenerator::S3Adapter.new(fog_provider: 'AWS',
                                                                    aws_access_key_id: ENV['AWS_ACCESS_KEY_ID'],
                                                                    aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
                                                                    fog_directory: ENV['S3_BUCKET_NAME'],
                                                                    fog_region: ENV['AWS_REGION'])

SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.sitemaps_host = "https://gamersparadise.s3.amazonaws.com/"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.ping_search_engines("https://www.gamersparadise.com/sitemap")


SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end


  Company.find_each do |company|
    add company_path(company), lastmod: company.updated_at
  end

  Franchise.find_each do |franchise|
    add franchise_path(franchise), lastmod: franchise.updated_at
  end

  GameEngine.find_each do |game_engine|
    add game_engine_path(game_engine), lastmod: game_engine.updated_at
  end

  GameMode.find_each do |game_mode|
    add game_mode_path(game_mode), lastmod: game_mode.updated_at
  end

  Genre.find_each do |genre|
    add genre_path(genre), lastmod: genre.updated_at
  end

  Group.find_each do |group|
    add group_path(group), lastmod: group.updated_at
  end

  List.find_each do |list|
    add list_path(list), lastmod: list.updated_at
  end

  MemberReview.find_each do |member_review|
    add member_review_path(member_review), lastmod: member_review.updated_at
  end

  Page.find_each do |page|
    add page_path(page), lastmod: page.updated_at
  end

  Platform.find_each do |platform|
    add platform_path(platform), lastmod: platform.updated_at
  end

  PlayerPerspective.find_each do |player_perspective|
    add player_perspective_path(player_perspective), lastmod: player_perspective.updated_at
  end

  Question.find_each do |question|
    add question_path(question), lastmod: question.updated_at
  end

  Series.find_each do |series|
    add series_path(series), lastmod: series.updated_at
  end

  Topic.find_each do |topic|
    add topic_path(topic), lastmod: topic.updated_at
  end

  User.find_each do |user|
    add user_path(user), lastmod: user.updated_at
  end

  VideoGame.find_each do |video_game|
    add video_game_path(video_game), lastmod: video_game.updated_at
  end

  BlogPost.find_each do |blog_post|
    add blog_path(blog_post), lastmod: blog_post.updated_at
  end

end
