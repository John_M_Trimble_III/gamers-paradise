class ConnectGamesToShelvesAndUser < ActiveRecord::Migration[5.1]
  def change
  	add_reference :shelves, :user, foreign_key: true

  	add_reference :shelved_games, :shelf, foreign_key: true
  	add_reference :shelved_games, :video_game, foreign_key: true

  end
end
