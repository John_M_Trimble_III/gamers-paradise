class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.text :question
      t.references :questionable, polymorphic: true
      t.references :user
      t.timestamps
    end
  end
end
