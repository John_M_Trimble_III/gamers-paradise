class CreateRewards < ActiveRecord::Migration[5.1]
	def change
    	create_table :rewards do |t|
    	
    		t.references :rewardable, polymorphic: true, index: true
    		t.references :user, index: true
    		t.integer :points, default: 0
    		t.timestamps
    	end
	    Contribution.all.each do |contribution|
	    	reward = contribution.rewards.build
	    	reward.user = contribution.user
	    	reward.points = contribution.points
	    	reward.save
	   	end

	    Referral.all.each do |referral|
			reward = referral.rewards.build
	    	reward.user = referral.referrer
	    	reward.points = referral.points
	    	reward.save
	    end    	
	end
end
