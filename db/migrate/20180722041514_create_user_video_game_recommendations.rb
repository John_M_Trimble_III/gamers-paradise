class CreateUserVideoGameRecommendations < ActiveRecord::Migration[5.1]
  def change
    create_table :user_video_game_recommendations do |t|
      t.references :user
      t.references :video_game
      t.timestamps
    end
  end
end
