class AddTypeOfsToCompaniesVideoGameAsArray < ActiveRecord::Migration[5.1]
  def change
  	add_column :companies_video_games, :type_ofs, :string, array: true, default: []

  end
end
