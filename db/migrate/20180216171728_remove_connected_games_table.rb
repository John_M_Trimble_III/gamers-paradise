class RemoveConnectedGamesTable < ActiveRecord::Migration[5.1]
  def change
  	drop_table :connected_games
  end
end
