class CreateMultiplayerModes < ActiveRecord::Migration[5.1]
  def change
    create_table :multiplayer_modes do |t|
      t.integer :platform_id
      t.integer :online_co_op_max_players
      t.integer :offline_co_op_max_players
      t.integer :online_max_players
      t.integer :offline_max_players
      t.boolean :is_online_co_op
      t.boolean :is_offline_co_op
      t.boolean :is_lan_co_op
      t.boolean :is_co_op_campaign
      t.boolean :is_online_split_screen
      t.boolean :is_offline_split_screen
      t.boolean :is_drop_in_drop_out
      t.timestamps
    end
  end
end
