class RemoveOfficialTrailerFromVidoeGames < ActiveRecord::Migration[5.1]
  def change
  	remove_column :video_games, :official_trailer, :string
  end
end
