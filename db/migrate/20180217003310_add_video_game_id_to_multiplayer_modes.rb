class AddVideoGameIdToMultiplayerModes < ActiveRecord::Migration[5.1]
  def change
  	add_column :multiplayer_modes, :video_game_id, :integer
  end
end
