class CreateVideoGameWikis < ActiveRecord::Migration[5.1]
	def change
    	create_table :video_game_wikis do |t|
    		t.references :video_game
    		t.references :wiki
      		t.timestamps
    	end
  	end
end
