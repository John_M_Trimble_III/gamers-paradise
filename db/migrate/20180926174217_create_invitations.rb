class CreateInvitations < ActiveRecord::Migration[5.1]
  def change
    create_table :invitations do |t|
    	t.references :sender
    	t.references :receiver
    	t.references :invitable, polymorphic: true


      	t.timestamps
    end
  end
end
