class AddFlagToGroupsForRequireApprovalForMembership < ActiveRecord::Migration[5.1]
  def change
  	add_column :groups, :require_approval, :boolean, default: false
  end
end
