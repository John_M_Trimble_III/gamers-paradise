class AddVideoGamesCountToShelf < ActiveRecord::Migration[5.1]
  def change
  	add_column :shelves, :video_games_count, :integer, default: 0  	
  end
end
