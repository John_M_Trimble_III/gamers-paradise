class CreateListVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :list_video_games do |t|
      t.references :video_game
      t.references :list
      t.references :user
      t.timestamps
    end
  end
end
