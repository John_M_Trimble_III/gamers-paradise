class AddPointsIncrementorToUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :points, :integer, default: 0

    User.reset_column_information 
  	User.all.each do |user|
  		user.points = user.get_points
  		user.save
  	end

  end
end
