class CreateVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :video_games do |t|


      t.string   	:name
      t.datetime   	:release_date
      t.text		:description
      t.string		:official_website


      t.timestamps
    end
  end
end
