class CreateRateds < ActiveRecord::Migration[5.1]
  def change
    create_table :rateds do |t|
	  # Polymorphic fields

	  t.integer :rateable_id
      t.string :rateable_type
      t.timestamps
    end
  end
end
