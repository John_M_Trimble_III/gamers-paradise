class SetDefaultForAverageRating < ActiveRecord::Migration[5.1]
  def change
  	change_column :video_games, :average_rating, :float, default: 0.0, nullable: false
  end
end
