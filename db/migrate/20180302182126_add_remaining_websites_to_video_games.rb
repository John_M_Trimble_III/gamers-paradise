class AddRemainingWebsitesToVideoGames < ActiveRecord::Migration[5.1]
  def change
  	add_column :video_games, :wikia_page, :string
  	add_column :video_games, :wikipedia_page, :string
  	add_column :video_games, :facebook_page, :string
  	add_column :video_games, :twitter_profile, :string
  	add_column :video_games, :twitch_channel, :string
  	add_column :video_games, :instagram_profile, :string
  	add_column :video_games, :youtube_channel, :string
  	add_column :video_games, :itunes_page_iphone, :string
  	add_column :video_games, :itunes_page_ipad, :string
  	add_column :video_games, :google_play_store_page, :string
  	add_column :video_games, :steam_store_page, :string
  	add_column :video_games, :subreddit_page, :string
  end
end
