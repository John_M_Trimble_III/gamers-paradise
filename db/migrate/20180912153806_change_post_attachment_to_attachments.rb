class ChangePostAttachmentToAttachments < ActiveRecord::Migration[5.1]
  def change
  	rename_table :post_attachments, :attachments
  	add_reference :attachments, :attachable, polymorphic: true, index: true
  	remove_column :attachments, :post_id
  end
end
