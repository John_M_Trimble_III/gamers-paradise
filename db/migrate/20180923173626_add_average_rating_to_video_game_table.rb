include VideogamesHelper

class AddAverageRatingToVideoGameTable < ActiveRecord::Migration[5.1]
  def change
	add_column :video_games, :average_rating, :float

	VideoGame.all.each do |video_game|
		video_game.update_column(:average_rating, get_avg_rating(video_game))
	end
  end
end
