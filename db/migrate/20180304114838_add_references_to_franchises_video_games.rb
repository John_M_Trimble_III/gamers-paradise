class AddReferencesToFranchisesVideoGames < ActiveRecord::Migration[5.1]
  def change
  	  	add_reference :franchises_video_games, :video_game, index: true
  	  	add_reference :franchises_video_games, :franchise, index: true

  end
end
