class AddStatusToVideoGames < ActiveRecord::Migration[5.1]
  def change
  	add_column :video_games, :status, :integer, default: 0
  end
end
