class AddFavoriteGameToUser < ActiveRecord::Migration[5.1]
  def change
  	add_reference :users, :favorite_game, index: true,  foreign_key: { to_table: :video_games }
  end
end
