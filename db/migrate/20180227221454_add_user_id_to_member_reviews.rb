class AddUserIdToMemberReviews < ActiveRecord::Migration[5.1]
  def change
    add_reference :member_reviews, :user, index: true
  end
end
