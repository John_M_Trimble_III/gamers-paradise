class AddCounterCachesToUser < ActiveRecord::Migration[5.1]
	def change
  		add_column :users, :video_games_count, :integer, default: 0
  		add_column :users, :friends_count, :integer, default: 0  	
  	end
end
