class ConnectPlatformsThroughTable < ActiveRecord::Migration[5.1]
  def change
	create_table :platforms_video_games do |t|  
		t.timestamps
    end
  	add_reference :platforms_video_games, :video_game, foreign_key: true
  	add_reference :platforms_video_games, :platform, foreign_key: true
  end
end
