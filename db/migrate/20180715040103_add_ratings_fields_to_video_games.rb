class AddRatingsFieldsToVideoGames < ActiveRecord::Migration[5.1]
  def change
  	add_column :video_games, :esrb_rating, :integer
  	add_column :video_games, :esrb_themes, :string, array: true, default: []
  	add_column :video_games, :esrb_synopsis, :text
  	add_column :video_games, :pegi_rating, :integer
  	add_column :video_games, :pegi_themes, :string, array: true, default: []
  	add_column :video_games, :pegi_synopsis, :text
  end
end
