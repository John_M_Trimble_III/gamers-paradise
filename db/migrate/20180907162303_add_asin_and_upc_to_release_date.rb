class AddAsinAndUpcToReleaseDate < ActiveRecord::Migration[5.1]
  def change
  	add_column :release_dates, :asin, :string 	
  	add_column :release_dates, :upc, :string
  end
end
