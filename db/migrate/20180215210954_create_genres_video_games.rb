class CreateGenresVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :genres_video_games do |t|
      t.references :video_game, foreign_key: true
      t.references :genre, foreign_key: true

      t.timestamps
    end
  end
end
