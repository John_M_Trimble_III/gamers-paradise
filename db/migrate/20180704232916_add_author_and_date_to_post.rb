class AddAuthorAndDateToPost < ActiveRecord::Migration[5.1]
  def change
 	add_column :posts, :date_published, :datetime  
 	add_column :posts, :author, :string  	  	  	
  end
end
