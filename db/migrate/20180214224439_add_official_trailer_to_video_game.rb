class AddOfficialTrailerToVideoGame < ActiveRecord::Migration[5.1]
  def change
  	add_column :video_games, :official_trailer, :string
  end
end
