class ConnectTablesTogether < ActiveRecord::Migration[5.1]
  def change
  	add_reference :listings, :user, foreign_key: true
  	add_reference :listings, :video_game, foreign_key: true
  	add_reference :listings, :platform, foreign_key: true
  end
end
