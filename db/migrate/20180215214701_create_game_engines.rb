class CreateGameEngines < ActiveRecord::Migration[5.1]
  def change
    create_table :game_engines do |t|
      t.string 	:name
      t.text	:description
      t.timestamps
    end
  end
end
