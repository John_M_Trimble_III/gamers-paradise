class CreateDrafts < ActiveRecord::Migration[5.1]
  def change
    create_table :drafts do |t|
      t.json :object # stored json of the 
      t.string :type_of # create update destroy
      t.integer :user_id # users who requested the change

	  # Polymorphic fields
	  t.integer :original_id
    t.string :original_type
    t.integer :modified_id
    t.string :modified_type

      t.timestamps
    end
  end
end
