class CreateReleaseDates < ActiveRecord::Migration[5.1]
  def change
    create_table :release_dates do |t|
      t.integer :platform_id
      t.string :region
      t.datetime :date
      t.references :releaseable, polymorphic: true, index: true      
      t.timestamps
    end
  end
end
