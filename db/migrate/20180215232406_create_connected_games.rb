class CreateConnectedGames < ActiveRecord::Migration[5.1]
  def change
    create_table :connected_games do |t|
	  t.integer :parent_id
	  t.integer :child_id

      t.timestamps
    end
  end
end
