class CreateShelvedGames < ActiveRecord::Migration[5.1]
  def change
    create_table :shelved_games do |t|

      t.timestamps
    end
  end
end
