class CreateListings < ActiveRecord::Migration[5.1]
  def change
    create_table :listings do |t|

      t.decimal :price, :precision => 8, :scale => 2
      t.string :currency
      t.timestamps
    end
  end
end
