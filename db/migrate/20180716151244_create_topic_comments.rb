class CreateTopicComments < ActiveRecord::Migration[5.1]
  def change
    create_table :topic_comments do |t|
      t.references :user, index: true
      t.references :topic, index: true
      t.text :comment
      t.timestamps
    end
  end
end
