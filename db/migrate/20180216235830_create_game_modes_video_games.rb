class CreateGameModesVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :game_modes_video_games do |t|
      t.integer :video_game_id
      t.integer :game_mode_id
      t.timestamps
    end
  end
end
