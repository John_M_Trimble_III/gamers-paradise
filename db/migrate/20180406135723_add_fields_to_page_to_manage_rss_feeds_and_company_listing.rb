class AddFieldsToPageToManageRssFeedsAndCompanyListing < ActiveRecord::Migration[5.1]
  def change
	add_reference :pages, :company
	add_column :pages, :rss_url, :string
  end
end
