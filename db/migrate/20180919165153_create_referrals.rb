class CreateReferrals < ActiveRecord::Migration[5.1]
	def change
		create_table :referrals do |t|
    		t.references :referrer, index: true
			t.references :referee, index: true    		
      		t.timestamps
    	end
	end
end
