class RemovePlatformsVideoGames < ActiveRecord::Migration[5.1]
  def change
  	drop_table :platforms_video_games
  end
end
