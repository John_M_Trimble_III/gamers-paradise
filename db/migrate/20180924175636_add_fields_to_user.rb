class AddFieldsToUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :daily_summary_email_opt_in, :boolean, default: true
  end
end
