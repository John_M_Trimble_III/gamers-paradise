class CreateVideoGameRelationships < ActiveRecord::Migration[5.1]
  def change
    create_table :video_game_relationships do |t|
    	t.integer :parent_id
    	t.integer :child_id
      t.timestamps
    end
  end
end
