class AddWikiLogoToWiki < ActiveRecord::Migration[5.1]
  def change
    add_column :wikis, :wiki_logo, :string  	
  end
end
