class CreateSimilarVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :similar_video_games do |t|
  	  t.references :video_game
  	  t.references :recommendation, references: :video_games
      t.timestamps
    end
  end
end
