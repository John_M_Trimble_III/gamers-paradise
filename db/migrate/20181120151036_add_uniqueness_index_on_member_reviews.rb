class AddUniquenessIndexOnMemberReviews < ActiveRecord::Migration[5.1]
  	def change
  		add_index :member_reviews, [:user_id, :video_game_id], unique: true
  	end
end
