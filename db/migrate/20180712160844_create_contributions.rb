class CreateContributions < ActiveRecord::Migration[5.1]
  def change
    create_table :contributions do |t|
      t.references :contributable, polymorphic: true, index: true
      t.references :user, index: true
      t.text :content
      t.string :field
      t.timestamps
    end
  end
end
