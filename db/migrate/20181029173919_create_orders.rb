class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
    	t.references :user
    	t.decimal :total_value,  :precision => 8, :scale => 2
    	t.text :order_number
    	t.text :external_id
      	t.timestamps
    end
  end
end
