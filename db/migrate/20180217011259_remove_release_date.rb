class RemoveReleaseDate < ActiveRecord::Migration[5.1]
  def change
  	remove_column :video_games, :release_date
  end
end
