class AddRateableToRatings < ActiveRecord::Migration[5.1]
  def change
	change_table :ratings do |t|
		t.references :rateable, polymorphic: true
    end  	
  end
end
