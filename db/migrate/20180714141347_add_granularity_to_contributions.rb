class AddGranularityToContributions < ActiveRecord::Migration[5.1]
  def change
    add_column :contributions, :action, :integer, index: true # Used to determine action taken add/removed/link/modified/unlink
    add_column :contributions, :points, :integer, default: 1
  end
end
