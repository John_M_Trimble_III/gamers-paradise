class AddUserDetails < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :username, :string
  	add_column :users, :my_story, :text
  	add_column :users, :birthday, :datetime
  	add_column :users, :facebook_profile, :string 
  	add_column :users, :twitter_profile, :string 
  	add_column :users, :twitch_profile, :string 
  	add_column :users, :instagram_profile, :string 
  	add_column :users, :youtube_profile, :string 
  	add_column :users, :steam_profile, :string 
  	add_column :users, :linkedin_profile, :string 
  	add_column :users, :pinterest_profile, :string 
  	add_column :users, :soundcloud_profile, :string 
  	add_column :users, :google_plus_profile, :string 
  	add_column :users, :reddit_profile, :string 
  	add_column :users, :battlenet_profile, :string 
  	add_column :users, :origin_profile, :string 
  	add_column :users, :uplay_profile, :string
  	add_column :users, :discord_profile, :string 
  end
end
