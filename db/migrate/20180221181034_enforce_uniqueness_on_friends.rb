class EnforceUniquenessOnFriends < ActiveRecord::Migration[5.1]
  def change
  	add_index :friendships, [:user_id, :friend_id], unique: true, :name => 'enforce_uniqueness_on_friendships'
  	add_index :friendships, [:friend_id, :user_id], unique: true, :name => 'enforce_uniqueness_on_friendships_inverted'
  end
end
