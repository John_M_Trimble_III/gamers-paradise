class CreateScreenshots < ActiveRecord::Migration[5.1]
  def change
    create_table :screenshots do |t|
  	  t.string :screenshot
  	  t.integer :screenshotable_id
  	  t.string :screenshotable_type

      t.timestamps
    end
  end
end
