class CreateCriticReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :critic_reviews do |t|
      t.references :video_game
      t.integer :rating
      t.text :summary
      t.references :company	
      t.timestamps
    end
  end
end
