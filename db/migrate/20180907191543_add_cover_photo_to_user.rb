class AddCoverPhotoToUser < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :cover_photo, :string
  end
end
