class CreateGameEnginesVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :game_engines_video_games do |t|
      t.references :video_game, foreign_key: true
      t.references :game_engine, foreign_key: true

      t.timestamps
    end
  end
end
