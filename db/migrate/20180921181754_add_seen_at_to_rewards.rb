class AddSeenAtToRewards < ActiveRecord::Migration[5.1]
  def change
  	add_column :rewards, :seen_at, :datetime

  	Reward.reset_column_information 
  	Reward.all.update_all(seen_at: Time.now )

  end
end
