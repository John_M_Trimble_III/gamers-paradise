class AddMetaDescriptionMetaKeywordsToBlogPost < ActiveRecord::Migration[5.1]
  	def change
  		add_column :blog_posts, :meta_description, :text
  		add_column :blog_posts, :meta_keywords, :text
  	end
end
