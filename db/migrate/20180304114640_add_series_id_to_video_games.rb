class AddSeriesIdToVideoGames < ActiveRecord::Migration[5.1]
  def change
  	add_reference :video_games, :series, index: true
  end
end
