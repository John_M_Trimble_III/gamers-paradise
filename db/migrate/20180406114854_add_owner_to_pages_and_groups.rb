class AddOwnerToPagesAndGroups < ActiveRecord::Migration[5.1]
  def change
  	add_reference :pages, :owner, polymorphic: true, index: true
  	add_reference :groups, :owner, polymorphic: true, index: true
  end
end
