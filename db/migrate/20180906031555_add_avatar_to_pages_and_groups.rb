class AddAvatarToPagesAndGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :avatar, :string
    add_column :groups, :avatar, :string
  end
end
