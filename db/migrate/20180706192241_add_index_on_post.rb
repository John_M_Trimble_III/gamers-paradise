class AddIndexOnPost < ActiveRecord::Migration[5.1]
  def change
	add_index :posts, [:external_id, :owner_id, :owner_type], unique: true  	
  end
end
