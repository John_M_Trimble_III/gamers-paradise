class CreateTopics < ActiveRecord::Migration[5.1]
  def change
    create_table :topics do |t|
      t.references :user, index: true
      t.references :topicable, polymorphic: true, index: true
      t.text :topic
	  t.boolean :is_poll, null: false
      t.timestamps
    end
  end
end
