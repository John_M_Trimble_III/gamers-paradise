class SetWebsiteDefaultsToNull < ActiveRecord::Migration[5.1]
  def change
  	change_column :video_games, :wikia_page, :string, default: nil
  	change_column :video_games, :wikipedia_page, :string, default: nil
  	change_column :video_games, :facebook_page, :string, default: nil
  	change_column :video_games, :twitter_profile, :string, default: nil
  	change_column :video_games, :twitch_channel, :string, default: nil
  	change_column :video_games, :instagram_profile, :string, default: nil
  	change_column :video_games, :youtube_channel, :string, default: nil
  	change_column :video_games, :itunes_page_iphone, :string, default: nil
  	change_column :video_games, :itunes_page_ipad, :string, default: nil
  	change_column :video_games, :google_play_store_page, :string, default: nil
  	change_column :video_games, :steam_store_page, :string, default: nil
  	change_column :video_games, :subreddit_page, :string, default: nil
  end
end
