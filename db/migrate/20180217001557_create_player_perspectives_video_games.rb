class CreatePlayerPerspectivesVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :player_perspectives_video_games do |t|
      t.integer :player_perspective_id
      t.integer :video_game_id      
      t.timestamps
    end
  end
end
