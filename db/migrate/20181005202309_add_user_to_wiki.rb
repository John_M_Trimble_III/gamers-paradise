class AddUserToWiki < ActiveRecord::Migration[5.1]
	def change
		add_reference :wikis, :user, foreign_key: true
	end
end
