class AddVideoGameToMemberReview < ActiveRecord::Migration[5.1]
  def change
  	    add_reference :member_reviews, :video_game, index: true
  end
end
