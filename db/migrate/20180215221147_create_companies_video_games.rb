class CreateCompaniesVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :companies_video_games do |t|
      t.references :video_game, foreign_key: true
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
