class EnforceUniquenessOnFollow < ActiveRecord::Migration[5.1]
  def change
  	add_index :follows, [:follower_id, :follower_type, :followable_id, :followable_type], unique: true, :name => 'enforce_uniqueness_on_follows'
  end
end
