class AddEditableFieldToShelves < ActiveRecord::Migration[5.1]
  def change
  	add_column :shelves, :editable, :boolean, :default => true
  end
end
