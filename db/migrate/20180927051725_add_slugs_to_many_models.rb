class AddSlugsToManyModels < ActiveRecord::Migration[5.1]
  def change
  	add_column :blog_posts, :slug, :string, unique: true
    add_index :blog_posts, :slug, unique: true  	

  	add_column :groups, :slug, :string, unique: true
    add_index :groups, :slug, unique: true 

  	add_column :pages, :slug, :string, unique: true
    add_index :pages, :slug, unique: true 

  	add_column :video_games, :slug, :string, unique: true
    add_index :video_games, :slug, unique: true 

  	add_column :lists, :slug, :string, unique: true
    add_index :lists, :slug, unique: true             
  end
end
