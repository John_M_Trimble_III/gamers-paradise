class CreateSeriesVideoGames < ActiveRecord::Migration[5.1]
  def change
    create_table :series_video_games do |t|
      t.references :video_game, index: true
      t.references :series, index: true      
      t.timestamps
    end
    remove_column :video_games, :series_id
  end
end
