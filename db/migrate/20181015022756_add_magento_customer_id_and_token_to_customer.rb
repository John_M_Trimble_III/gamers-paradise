class AddMagentoCustomerIdAndTokenToCustomer < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :magento_id, :integer
  	add_column :users, :magento_token, :text
  end
end
