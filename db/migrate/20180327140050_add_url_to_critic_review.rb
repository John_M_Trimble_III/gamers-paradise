class AddUrlToCriticReview < ActiveRecord::Migration[5.1]
  def change
  	add_column :critic_reviews, :url, :text
  end
end
