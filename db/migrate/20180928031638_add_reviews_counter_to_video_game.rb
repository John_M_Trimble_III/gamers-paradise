class AddReviewsCounterToVideoGame < ActiveRecord::Migration[5.1]
	def change
		add_column :video_games, :ratings_count, :integer, default: 0
		add_column :video_games, :member_reviews_count, :integer, default: 0
	end
end
