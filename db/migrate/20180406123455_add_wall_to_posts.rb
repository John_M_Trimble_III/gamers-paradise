class AddWallToPosts < ActiveRecord::Migration[5.1]
  def change
  	add_reference :posts, :wall, polymorphic: true, index: true
  end
end
