class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.text :content
      t.string :owner_type
      t.integer :owner_id
      t.timestamps
    end
  end
end
