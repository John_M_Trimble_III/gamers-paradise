class CreateMemberReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :member_reviews do |t|
      t.text :body
      t.integer :rating, default: 0
      t.timestamps
    end
  end
end
