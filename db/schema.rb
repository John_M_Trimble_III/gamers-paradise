# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181120151036) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.text "answer"
    t.bigint "question_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_answers_on_question_id"
    t.index ["user_id"], name: "index_answers_on_user_id"
  end

  create_table "attachments", force: :cascade do |t|
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "youtube_id"
    t.string "attachable_type"
    t.bigint "attachable_id"
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id"
  end

  create_table "authentication_providers", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_name_on_authentication_providers"
  end

  create_table "blog_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "blog_posts", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.integer "blog_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "featured_image"
    t.bigint "author_id"
    t.string "slug"
    t.text "meta_description"
    t.text "meta_keywords"
    t.index ["author_id"], name: "index_blog_posts_on_author_id"
    t.index ["slug"], name: "index_blog_posts_on_slug", unique: true
  end

  create_table "characters", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text "body"
    t.string "commentable_type"
    t.integer "commentable_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.index ["ancestry"], name: "index_comments_on_ancestry"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "website"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies_video_games", force: :cascade do |t|
    t.bigint "video_game_id"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type_ofs", default: [], array: true
    t.index ["company_id"], name: "index_companies_video_games_on_company_id"
    t.index ["video_game_id"], name: "index_companies_video_games_on_video_game_id"
  end

  create_table "contributions", force: :cascade do |t|
    t.string "contributable_type"
    t.bigint "contributable_id"
    t.bigint "user_id"
    t.text "content"
    t.string "field"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "action"
    t.integer "points", default: 1
    t.index ["contributable_type", "contributable_id"], name: "index_contributions_on_contributable_type_and_contributable_id"
    t.index ["user_id"], name: "index_contributions_on_user_id"
  end

  create_table "critic_reviews", force: :cascade do |t|
    t.bigint "video_game_id"
    t.integer "rating"
    t.text "summary"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "url"
    t.index ["company_id"], name: "index_critic_reviews_on_company_id"
    t.index ["video_game_id"], name: "index_critic_reviews_on_video_game_id"
  end

  create_table "drafts", force: :cascade do |t|
    t.json "object"
    t.string "type_of"
    t.integer "user_id"
    t.integer "original_id"
    t.string "original_type"
    t.integer "modified_id"
    t.string "modified_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "follows", force: :cascade do |t|
    t.string "followable_type", null: false
    t.bigint "followable_id", null: false
    t.string "follower_type", null: false
    t.bigint "follower_id", null: false
    t.boolean "blocked", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followable_id", "followable_type"], name: "fk_followables"
    t.index ["followable_type", "followable_id"], name: "index_follows_on_followable_type_and_followable_id"
    t.index ["follower_id", "follower_type", "followable_id", "followable_type"], name: "enforce_uniqueness_on_follows", unique: true
    t.index ["follower_id", "follower_type"], name: "fk_follows"
    t.index ["follower_type", "follower_id"], name: "index_follows_on_follower_type_and_follower_id"
  end

  create_table "franchises", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "franchises_video_games", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "video_game_id"
    t.bigint "franchise_id"
    t.index ["franchise_id"], name: "index_franchises_video_games_on_franchise_id"
    t.index ["video_game_id"], name: "index_franchises_video_games_on_video_game_id"
  end

  create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.datetime "updated_at"
    t.index ["deleted_at"], name: "index_friendly_id_slugs_on_deleted_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "friendships", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "friend_id"
    t.boolean "confirmed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["friend_id", "user_id"], name: "enforce_uniqueness_on_friendships_inverted", unique: true
    t.index ["friend_id"], name: "index_friendships_on_friend_id"
    t.index ["user_id", "friend_id"], name: "enforce_uniqueness_on_friendships", unique: true
    t.index ["user_id"], name: "index_friendships_on_user_id"
  end

  create_table "game_engines", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "game_engines_video_games", force: :cascade do |t|
    t.bigint "video_game_id"
    t.bigint "game_engine_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_engine_id"], name: "index_game_engines_video_games_on_game_engine_id"
    t.index ["video_game_id"], name: "index_game_engines_video_games_on_video_game_id"
  end

  create_table "game_modes", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "game_modes_video_games", force: :cascade do |t|
    t.integer "video_game_id"
    t.integer "game_mode_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "genres", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "genres_video_games", force: :cascade do |t|
    t.bigint "video_game_id"
    t.bigint "genre_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["genre_id"], name: "index_genres_video_games_on_genre_id"
    t.index ["video_game_id"], name: "index_genres_video_games_on_video_game_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "owner_type"
    t.bigint "owner_id"
    t.string "avatar"
    t.string "banner"
    t.boolean "require_approval", default: false
    t.string "slug"
    t.index ["owner_type", "owner_id"], name: "index_groups_on_owner_type_and_owner_id"
    t.index ["slug"], name: "index_groups_on_slug", unique: true
  end

  create_table "images", force: :cascade do |t|
    t.string "alt"
    t.string "hint"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "impressions", force: :cascade do |t|
    t.string "impressionable_type"
    t.integer "impressionable_id"
    t.integer "user_id"
    t.string "controller_name"
    t.string "action_name"
    t.string "view_name"
    t.string "request_hash"
    t.string "ip_address"
    t.string "session_hash"
    t.text "message"
    t.text "referrer"
    t.text "params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index"
    t.index ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index"
    t.index ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index"
    t.index ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index"
    t.index ["impressionable_type", "impressionable_id", "params"], name: "poly_params_request_index"
    t.index ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index"
    t.index ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index"
    t.index ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index"
    t.index ["user_id"], name: "index_impressions_on_user_id"
  end

  create_table "invitations", force: :cascade do |t|
    t.bigint "sender_id"
    t.bigint "receiver_id"
    t.string "invitable_type"
    t.bigint "invitable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invitable_type", "invitable_id"], name: "index_invitations_on_invitable_type_and_invitable_id"
    t.index ["receiver_id"], name: "index_invitations_on_receiver_id"
    t.index ["sender_id"], name: "index_invitations_on_sender_id"
  end

  create_table "likes", force: :cascade do |t|
    t.string "likeable_type"
    t.bigint "likeable_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["likeable_type", "likeable_id"], name: "index_likes_on_likeable_type_and_likeable_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "list_video_games", force: :cascade do |t|
    t.bigint "video_game_id"
    t.bigint "list_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["list_id"], name: "index_list_video_games_on_list_id"
    t.index ["user_id"], name: "index_list_video_games_on_user_id"
    t.index ["video_game_id"], name: "index_list_video_games_on_video_game_id"
  end

  create_table "listings", force: :cascade do |t|
    t.decimal "price", precision: 8, scale: 2
    t.string "currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "video_game_id"
    t.bigint "platform_id"
    t.index ["platform_id"], name: "index_listings_on_platform_id"
    t.index ["user_id"], name: "index_listings_on_user_id"
    t.index ["video_game_id"], name: "index_listings_on_video_game_id"
  end

  create_table "lists", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["slug"], name: "index_lists_on_slug", unique: true
    t.index ["user_id"], name: "index_lists_on_user_id"
  end

  create_table "member_reviews", force: :cascade do |t|
    t.text "body"
    t.integer "rating", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "video_game_id"
    t.index ["user_id", "video_game_id"], name: "index_member_reviews_on_user_id_and_video_game_id", unique: true
    t.index ["user_id"], name: "index_member_reviews_on_user_id"
    t.index ["video_game_id"], name: "index_member_reviews_on_video_game_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "group_id"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.index ["group_id"], name: "index_memberships_on_group_id"
    t.index ["user_id"], name: "index_memberships_on_user_id"
  end

  create_table "multiplayer_modes", force: :cascade do |t|
    t.integer "platform_id"
    t.integer "online_co_op_max_players"
    t.integer "offline_co_op_max_players"
    t.integer "online_max_players"
    t.integer "offline_max_players"
    t.boolean "is_online_co_op"
    t.boolean "is_offline_co_op"
    t.boolean "is_lan_co_op"
    t.boolean "is_co_op_campaign"
    t.boolean "is_online_split_screen"
    t.boolean "is_offline_split_screen"
    t.boolean "is_drop_in_drop_out"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "video_game_id"
  end

  create_table "navigation_groups", force: :cascade do |t|
    t.string "name"
    t.bigint "navigation_id"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["navigation_id"], name: "index_navigation_groups_on_navigation_id"
  end

  create_table "navigation_links", force: :cascade do |t|
    t.string "text"
    t.string "link"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "linkable_type"
    t.bigint "linkable_id"
    t.bigint "navigation_group_id"
    t.index ["linkable_type", "linkable_id"], name: "index_navigation_links_on_linkable_type_and_linkable_id"
    t.index ["navigation_group_id"], name: "index_navigation_links_on_navigation_group_id"
  end

  create_table "navigations", force: :cascade do |t|
    t.bigint "wiki_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["wiki_id"], name: "index_navigations_on_wiki_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "actor_id"
    t.integer "recipient_id"
    t.datetime "read_at"
    t.string "action"
    t.integer "notifiable_id"
    t.string "notifiable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "user_id"
    t.decimal "total_value", precision: 8, scale: 2
    t.text "order_number"
    t.text "external_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "pages", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "owner_type"
    t.bigint "owner_id"
    t.bigint "company_id"
    t.string "rss_url"
    t.string "banner"
    t.string "avatar"
    t.string "slug"
    t.index ["company_id"], name: "index_pages_on_company_id"
    t.index ["owner_type", "owner_id"], name: "index_pages_on_owner_type_and_owner_id"
    t.index ["slug"], name: "index_pages_on_slug", unique: true
  end

  create_table "platforms", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "player_perspectives", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "player_perspectives_video_games", force: :cascade do |t|
    t.integer "player_perspective_id"
    t.integer "video_game_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.text "content"
    t.string "owner_type"
    t.integer "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "wall_type"
    t.bigint "wall_id"
    t.string "external_id"
    t.string "external_link"
    t.text "title"
    t.datetime "date_published"
    t.string "author"
    t.index ["external_id", "owner_id", "owner_type"], name: "index_posts_on_external_id_and_owner_id_and_owner_type", unique: true
    t.index ["wall_type", "wall_id"], name: "index_posts_on_wall_type_and_wall_id"
  end

  create_table "questions", force: :cascade do |t|
    t.text "question"
    t.string "questionable_type"
    t.bigint "questionable_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["questionable_type", "questionable_id"], name: "index_questions_on_questionable_type_and_questionable_id"
    t.index ["user_id"], name: "index_questions_on_user_id"
  end

  create_table "ratings", force: :cascade do |t|
    t.integer "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.string "rateable_type"
    t.bigint "rateable_id"
    t.index ["rateable_type", "rateable_id"], name: "index_ratings_on_rateable_type_and_rateable_id"
    t.index ["user_id"], name: "index_ratings_on_user_id"
  end

  create_table "referrals", force: :cascade do |t|
    t.bigint "referrer_id"
    t.bigint "referee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "points"
    t.index ["referee_id"], name: "index_referrals_on_referee_id"
    t.index ["referrer_id"], name: "index_referrals_on_referrer_id"
  end

  create_table "release_dates", force: :cascade do |t|
    t.integer "platform_id"
    t.string "region"
    t.datetime "date"
    t.string "releaseable_type"
    t.bigint "releaseable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "asin"
    t.string "upc"
    t.index ["releaseable_type", "releaseable_id"], name: "index_release_dates_on_releaseable_type_and_releaseable_id"
  end

  create_table "rewards", force: :cascade do |t|
    t.string "rewardable_type"
    t.bigint "rewardable_id"
    t.bigint "user_id"
    t.integer "points", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "seen_at"
    t.index ["rewardable_type", "rewardable_id"], name: "index_rewards_on_rewardable_type_and_rewardable_id"
    t.index ["user_id"], name: "index_rewards_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "screenshots", force: :cascade do |t|
    t.string "screenshot"
    t.integer "screenshotable_id"
    t.string "screenshotable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "series", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "series_video_games", force: :cascade do |t|
    t.bigint "video_game_id"
    t.bigint "series_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["series_id"], name: "index_series_video_games_on_series_id"
    t.index ["video_game_id"], name: "index_series_video_games_on_video_game_id"
  end

  create_table "shelved_games", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "shelf_id"
    t.bigint "video_game_id"
    t.index ["shelf_id"], name: "index_shelved_games_on_shelf_id"
    t.index ["video_game_id"], name: "index_shelved_games_on_video_game_id"
  end

  create_table "shelves", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.boolean "editable", default: true
    t.integer "video_games_count", default: 0
    t.index ["user_id"], name: "index_shelves_on_user_id"
  end

  create_table "similar_video_games", force: :cascade do |t|
    t.bigint "video_game_id"
    t.bigint "recommendation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["recommendation_id"], name: "index_similar_video_games_on_recommendation_id"
    t.index ["video_game_id"], name: "index_similar_video_games_on_video_game_id"
  end

  create_table "topic_comments", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "topic_id"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["topic_id"], name: "index_topic_comments_on_topic_id"
    t.index ["user_id"], name: "index_topic_comments_on_user_id"
  end

  create_table "topics", force: :cascade do |t|
    t.bigint "user_id"
    t.string "topicable_type"
    t.bigint "topicable_id"
    t.text "topic"
    t.boolean "is_poll", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["topicable_type", "topicable_id"], name: "index_topics_on_topicable_type_and_topicable_id"
    t.index ["user_id"], name: "index_topics_on_user_id"
  end

  create_table "user_authentications", force: :cascade do |t|
    t.integer "user_id"
    t.integer "authentication_provider_id"
    t.string "uid"
    t.string "token"
    t.datetime "token_expires_at"
    t.text "params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["authentication_provider_id"], name: "index_user_authentications_on_authentication_provider_id"
    t.index ["user_id"], name: "index_user_authentications_on_user_id"
  end

  create_table "user_video_game_recommendations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "video_game_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_video_game_recommendations_on_user_id"
    t.index ["video_game_id"], name: "index_user_video_game_recommendations_on_video_game_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role"
    t.string "username"
    t.text "my_story"
    t.datetime "birthday"
    t.string "facebook_profile"
    t.string "twitter_profile"
    t.string "twitch_profile"
    t.string "instagram_profile"
    t.string "youtube_profile"
    t.string "steam_profile"
    t.string "linkedin_profile"
    t.string "pinterest_profile"
    t.string "soundcloud_profile"
    t.string "google_plus_profile"
    t.string "reddit_profile"
    t.string "battlenet_profile"
    t.string "origin_profile"
    t.string "uplay_profile"
    t.string "discord_profile"
    t.string "avatar"
    t.bigint "favorite_game_id"
    t.string "cover_photo"
    t.string "referral_token"
    t.integer "points", default: 0
    t.boolean "daily_summary_email_opt_in", default: true
    t.string "slug"
    t.integer "video_games_count", default: 0
    t.integer "friends_count", default: 0
    t.string "spree_api_key", limit: 48
    t.integer "ship_address_id"
    t.integer "bill_address_id"
    t.integer "magento_id"
    t.text "magento_token"
    t.string "api_token"
    t.index ["api_token"], name: "index_users_on_api_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["favorite_game_id"], name: "index_users_on_favorite_game_id"
    t.index ["referral_token"], name: "index_users_on_referral_token", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["slug"], name: "index_users_on_slug", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", limit: 191, null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "video_game_relationships", force: :cascade do |t|
    t.integer "parent_id"
    t.integer "child_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "video_game_wikis", force: :cascade do |t|
    t.bigint "video_game_id"
    t.bigint "wiki_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["video_game_id"], name: "index_video_game_wikis_on_video_game_id"
    t.index ["wiki_id"], name: "index_video_game_wikis_on_wiki_id"
  end

  create_table "video_games", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "official_website"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "box_art"
    t.integer "type_of"
    t.integer "status", default: 0
    t.string "wikia_page"
    t.string "wikipedia_page"
    t.string "facebook_page"
    t.string "twitter_profile"
    t.string "twitch_channel"
    t.string "instagram_profile"
    t.string "youtube_channel"
    t.string "itunes_page_iphone"
    t.string "itunes_page_ipad"
    t.string "google_play_store_page"
    t.string "steam_store_page"
    t.string "subreddit_page"
    t.text "story"
    t.integer "esrb_rating"
    t.string "esrb_themes", default: [], array: true
    t.text "esrb_synopsis"
    t.integer "pegi_rating"
    t.string "pegi_themes", default: [], array: true
    t.text "pegi_synopsis"
    t.float "average_rating", default: 0.0
    t.string "slug"
    t.integer "ratings_count", default: 0
    t.integer "member_reviews_count", default: 0
    t.index ["slug"], name: "index_video_games_on_slug", unique: true
  end

  create_table "videos", force: :cascade do |t|
    t.string "video"
    t.integer "videoable_id"
    t.string "videoable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wiki_categories", force: :cascade do |t|
    t.string "name"
    t.string "ancesry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ancesry"], name: "index_wiki_categories_on_ancesry"
  end

  create_table "wiki_pages", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "wiki_id"
    t.boolean "is_home_page", default: false
    t.index ["wiki_id"], name: "index_wiki_pages_on_wiki_id"
  end

  create_table "wikis", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "wiki_logo"
    t.bigint "user_id"
    t.index ["user_id"], name: "index_wikis_on_user_id"
  end

  add_foreign_key "companies_video_games", "companies"
  add_foreign_key "companies_video_games", "video_games"
  add_foreign_key "friendships", "users"
  add_foreign_key "friendships", "users", column: "friend_id"
  add_foreign_key "game_engines_video_games", "game_engines"
  add_foreign_key "game_engines_video_games", "video_games"
  add_foreign_key "genres_video_games", "genres"
  add_foreign_key "genres_video_games", "video_games"
  add_foreign_key "listings", "platforms"
  add_foreign_key "listings", "users"
  add_foreign_key "listings", "video_games"
  add_foreign_key "ratings", "users"
  add_foreign_key "shelved_games", "shelves"
  add_foreign_key "shelved_games", "video_games"
  add_foreign_key "shelves", "users"
  add_foreign_key "users", "video_games", column: "favorite_game_id"
  add_foreign_key "wikis", "users"
end
