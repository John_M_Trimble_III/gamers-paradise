# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

   # 198.times do |n|
   #    user = User.new(
   #      email:     Faker::Internet.email,
   #      password:  "password")
   #    user.save
   #  end

require 'csv'
# Video Games
filepath = Rails.root.join('lib', 'seeds', 'game_file.csv')
CSV.foreach(filepath, :headers => true) do |row|
	VideoGame.create(name: row["Name"])
end

# Series
filepath = Rails.root.join('lib', 'seeds', 'franchise.csv')
CSV.foreach(filepath, :headers => true) do |row|
	Franchise.create(name: row["Name"])
end


# Need To Add Genres Etc.