# Preview all emails at http://localhost:3000/rails/mailers/user_summary_mailer
class UserSummaryMailerPreview < ActionMailer::Preview
	def daily_summary_mailer
		UserSummaryMailer.daily_summary_email(User.first)
	end

end
