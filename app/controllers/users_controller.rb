class UsersController < ApplicationController
   	before_action :authenticate_user!, only: [:mark_game_as_favorite, :unmark_game_as_favorite, :scorecard]

	def index
		@users = User.all.where.not(id: user_signed_in? ? current_user.id : nil).order(created_at: :asc).page(params[:page]).per(10)
	end

	def show
		@user = User.friendly.find(params[:id])	
		@contributions = @user.contributions.limit(15)
		@breakdown = @contributions.type_breakdown
		@posts = @user.wall_posts.order(created_at: :desc)
		if user_signed_in? 
			@post_to_edit = Post.new(owner: current_user, wall: @user)
		end
		@video_game_recommendations = @user.video_game_recommendations.eager_load(:release_dates).page params[:page]
	end

	def mark_game_as_favorite
		user = User.friendly.find(params[:user_id])
		video_game = VideoGame.published.friendly.find(params[:video_game_id])
		if(current_user == user)
			current_user.favorite_game = video_game
			if(current_user.save)
				flash[:success] = "You successfully set your one and only favorite game!"
			else
				flash[:warning] = "Could not favorite this game, you must not love it enough"
			end		
		else
			flash[:warning] = "You scamp, trying to unfavorite someone else's game."
		end
		
		redirect_back(fallback_location: root_path)			
	end

	def unmark_game_as_favorite
		user = User.friendly.find(params[:user_id])
		if(current_user == user)
			current_user.favorite_game = nil
			if(current_user.save)
				flash[:success] = "You successfully unset your one and only favorite game!"
			else
				flash[:warning] = "Could not unfavorite this game, you must not love it enough"
			end		
		else
			flash[:warning] = "You scamp, trying to unfavorite someone else's game."
		end		
		redirect_back(fallback_location: root_path)
	end	

	def leaderboard
		@users = User.all.order(points: :desc).page params[:page]
	end

	def scorecard
		@user = current_user
		@user_rewards = @user.user_rewards.page params[:page]
	end

	def update_avatar
		if current_user == User.friendly.find(params[:user_id])
			current_user.assign_attributes(user_params)
			current_user.save
		end
		redirect_to current_user
	end

	def remove_avatar
		if current_user == User.friendly.find(params[:user_id])
      		current_user.remove_avatar!
      		current_user.save			      		
		end
		redirect_to current_user
	end

	def update_banner
		if current_user == User.friendly.find(params[:user_id])
			current_user.assign_attributes(user_params)
			current_user.save
		end	
		redirect_to current_user

	end

	def remove_banner
		if current_user == User.friendly.find(params[:user_id])
      		current_user.remove_cover_photo!
      		current_user.save			
		end	
		redirect_to current_user

	end


private 
	def user_params
    	params.require(:user).permit(:cover_photo, :avatar)
	end
end
