class ListVideoGamesController < ApplicationController
before_action :authenticate_user! #-> routes to the login / signup if not authenticated	
	def create 
		list = List.find(params[:list_id])
		new_game_list = list.list_video_games.new(list_video_game_params)
		new_game_list.user = current_user
		if new_game_list.save
			flash[:success] = "Succesfully Added #{new_game_list.video_game.name} to #{list.name}"
		else
			flash[:error] = new_game_list.errors.full_messages
		end
		redirect_to list		
	end

	def destroy

	end

	def show
		list_video_game = ListVideoGame.find(params[:id])
		redirect_to list_video_game.list
	end

private
	def list_video_game_params
	    params.require(:list_video_game).permit(:video_game_id)
	end
end
