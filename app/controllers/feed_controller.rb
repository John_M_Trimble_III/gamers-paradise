class FeedController < ApplicationController
	before_action :authenticate_user!	
	def index
		# Feed should be made up of the following
		# All the streams from all the followed objects
		ids = $redis.lrange "user-newsfeed-#{current_user.id}", 0, -1
		if ids.empty? 
			flash[:warning] = "Follow Your Favorite Games, Groups, And Pages To Populate NewsFeed"
			redirect_to video_games_path
		else
			@posts = Post.eager_load(:attachments).eager_load(:comments).where(id: ids).order(created_at: :desc).page(params[:page]).per 15
			# Grab the Post Ids from Redis				
		end
		
	end

end
