class StaticPagesController < ApplicationController
	# StaticPagesController.rb
	def sitemap
  		redirect_to 'https://gamersparadise.s3.amazonaws.com/sitemaps/sitemap.xml.gz'
	end

	def about_us
	end

	def privacy
	end	
end
