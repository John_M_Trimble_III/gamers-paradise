class GameModesController < ApplicationController
	include ControllerDraftable
	def show
		@game_mode = GameMode.find(params[:id])
		@video_games = @game_mode.video_games.page params[:page]
	end
	def index
		@game_modes = GameMode.published
	end
	def new
		@game_mode = GameMode.new
	end
	def edit 
		@game_mode = GameMode.find(params[:id])
	end
private
	def game_mode_params
		params.require(:game_mode).permit(:name, :description)
	end	
end
