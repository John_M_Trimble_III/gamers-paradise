class FollowsController < ApplicationController
	def index 
		@user = User.friendly.find(params[:user_id])
	end

	def create 
		follow = Follow.new
		follow.followable_id = params[:followable_id]
		follow.followable_type = params[:followable_type]
		follow.follower_id = params[:follower_id]
		follow.follower_type = params[:follower_type]
		
		if follow.save
			name = follow.followable.try(:name) ? follow.followable.try(:name) : follow.followable.try(:username)
			flash[:success] = "Succesfully Followed #{name}"
			redirect_back(fallback_location: root_path)
		else
			flash[:warning] = "Something went wrong"
		end
	end

	def destroy	
    	follow = Follow.find(params[:id])
    	name = follow.followable.try(:name) ? follow.followable.try(:name) : follow.followable.try(:username)    	
    	if follow.destroy
    		flash[:success] = "Succesfully Unfollowed #{name}"
    	else
			flash[:warning] = "Something Went Wrong"
    	end
		
		redirect_back(fallback_location: root_path)
  	end

  	def show
  		follow = Follow.find(params[:id])
  		if current_user == follow.followable
  			redirect_to follow.follower
  		else
  			redirect_to follow.followable
  		end
  	end

  	def followers
  		@user = User.friendly.find(params[:id])
  		@followers = @user.followers
  	end
end
