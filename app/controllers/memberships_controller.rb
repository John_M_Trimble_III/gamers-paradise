class MembershipsController < ApplicationController
	def index
		group = Group.friendly.find(params[:group_id])
		@memberships = group.memberships
	end

	def create
		group = Group.find(params[:group_id])
		user = User.find(params[:user_id])
		require_approval = group.require_approval

		membership = Membership.new
		membership.group = group
		membership.user = user
		membership.role = Membership.roles[:member]
		
		if require_approval
			membership.status = Membership.statuses[:pending_approval]
		else
			membership.status = Membership.statuses[:approved]
		end

		if(membership.save)
			if require_approval
				flash[:success] = "Successfully Requested Membership"
			else
				flash[:success] = "Successfully Joined"
			end			
			redirect_to group
		else
			flash[:warning] = membership.errors.full_messages
			redirect_back(fallback_location: root_path)
		end
	end
	
	def promote
		membership = Membership.find(params[:id])
		membership.role = :admin
		if membership.save
			flash[:success] = "Successfully Promoted Member"
		else
			flash[:warning] = "Error, something went wrong, self-destruct in 3.2.1...."
		end
		redirect_back(fallback_location: root_path)		
	end

	def demote
		membership = Membership.find(params[:id])
		membership.role = :member
		if membership.save
			flash[:success] = "Successfully Demoted Member"
		else
			flash[:warning] = "Error, something went wrong, self-destruct in 3.2.1...."
		end
		redirect_back(fallback_location: root_path)			
	end

	def approve
		membership = Membership.find(params[:id])
		membership.status = :approved
		if membership.save
			flash[:success] = "Successfully Approved Member"
		else
			flash[:warning] = "Error, something went wrong, self-destruct in 3.2.1...."
		end
		redirect_back(fallback_location: root_path)			
	end

	def revoke
		membership = Membership.find(params[:id])
		membership.status = :rejected
		if membership.save
			flash[:success] = "Successfully rejected member"
		else
			flash[:warning] = "Error, something went wrong, self-destruct in 3.2.1...."
		end
		redirect_back(fallback_location: root_path)			
	end
	
	def destroy
		membership = Membership.find(params[:id])
		if membership.destroy
			flash[:success] = "Successfully Left"			
		else
			flash[:warning] = "Something went wrong, you can't leave"
		end
		redirect_back(fallback_location: root_path)
	end

	def show
		membership = Membership.find(params[:id])
		redirect_to membership.group
	end

private 
	def membership_params
	    params.permit(:group_id, :user_id)
	end
end
