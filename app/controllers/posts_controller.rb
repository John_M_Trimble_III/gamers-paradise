class PostsController < ApplicationController
	before_action :is_owner?, only: [:edit, :update]	
	def create
		attachment_ids = params[:post][:attachments]
		params[:post].delete :attachments
		@post = Post.new(post_params)
		
		@post.content = params.select{|key,value| key.start_with?("mce_")}.values.first

		# We are creating the attachments first then syncing them afterwards, due to FileList limitations
		if attachment_ids
			attachment_ids.each do |attachment_id|
				@post.attachments << Attachment.find_by_id(attachment_id)
			end	
		end
		@post.save

		@post_to_edit = Post.new(owner: @post.owner, wall: @post.wall)
    	
    	respond_to do |format|
        	format.js
        end
	end

	def edit
		@post_to_edit = Post.find(params[:id])
    	respond_to do |format|
        	format.js
        end		
	end

	def update
		attachment_ids = params[:post][:attachments]
		params[:post].delete :attachments

		@post = Post.find(params[:id])
		@post.assign_attributes(post_params)
		@post.content = params.select{|key,value| key.start_with?("mce_")}.values.first
		
		if attachment_ids
			attachment_ids.each do |attachment_id|
				@post.attachments << Attachment.find_by_id(attachment_id) unless @post.attachment_ids.include?(attachment_id)
			end	
		end
		@post.save
    	respond_to do |format|
        	format.js
        end			
	end

	def destroy
		@post = Post.find(params[:id])
		@post.destroy
    	respond_to do |format|
        	format.js
        end				
	end

	def show
		post = Post.find(params[:id])
		redirect_to post.wall
	end

	private 
	def post_params
		params.require(:post).permit(:content, :owner_id, :owner_type, :wall_id, :wall_type)
	end

	def is_owner? 
		post = Post.find(params[:id])
		
		if post.owner != current_user
			flash[:warning] = "You are not the owner of this question."
			redirect_back(fallback_location: root_path)				
		end
	end	  
end
