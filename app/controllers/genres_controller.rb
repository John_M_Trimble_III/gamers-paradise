class GenresController < ApplicationController
	include ControllerDraftable
	def index
		@genres = Genre.published.order(name: :asc)
	end
	
	def show
		@genre = Genre.find(params[:id])
		@video_games = @genre.video_games.order(name: :asc).published.page params[:page]
	end

	def new
		@genre = Genre.new
	end

	def edit 
		@genre = Genre.find(params[:id])
	end
private
	def genre_params
		params.require(:genre).permit(:name)		
	end
end
