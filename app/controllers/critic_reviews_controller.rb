class CriticReviewsController < ApplicationController
	def new
		@video_game = VideoGame.friendly.find(params[:video_game_id])
	end

	def create
		video_game = VideoGame.friendly.find(params[:video_game_id])
		critic_review = CriticReview.new(critic_review_params)
		if(critic_review.save)
			flash[:success] = "Woohoo the review has been added kudos to you"
			redirect_to video_game
		else
			flash[:warning] = "Sorry something went wrong"
    		redirect_back(fallback_location: root_path)
		end

	end

	def edit
	end

	def update
	end

	def destroy
	end


private
	def critic_review_params
    	params.require(:critic_review).permit(:rating,:url, :summary, :company_id)		
	end

end
