class GroupsController < ApplicationController
	before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
	
	def index
		@groups = Group.all
	end
	def new
		@group = Group.new(owner: current_user)
	end

	def create
		group = Group.new(group_params)
		group.owner = current_user
		if(group.save)
			flash[:success] = "Successfully created new group"
			redirect_to group
		else
			flash[:warning] = "Error, something went wrong, abandon hope"
			redirect_back(fallback_location: root_path)
		end
	end 

	def edit
		@group = Group.friendly.find(params[:id])
	end

	def update
		group = Group.find(params[:id])
    	group.remove_banner! if "1" == params[:group][:delete_banner]
    	group.remove_avatar! if "1" == params[:group][:delete_avatar]		
    	if group.update_attributes(group_params)
      		flash[:success] = "Group updated"
      		redirect_to group
    	else
    		flash[:error] = "Did not update"
      		render 'edit'
    	end
	end

	def show
		@group = Group.friendly.find(params[:id])	
		@posts = @group.posts.order(created_at: :desc).page params[:page]
		@post_to_edit = Post.new(wall: @group, owner: current_user)
	end

	def destroy
		# Verify current user is admin or owner
		group = Group.friendly.find(params[:id])
		if ((current_user == group.owner) || group.admins.include?(current_user)) && group.destroy
			flash[:success] = "Group Has Been Successfully Destroyed"
			redirect_to root_path
		else
			flash[:warning] = "Group was unable to be deleted"
			redirect_back(fallback_location: root_path)
		end			

	end

private
	def group_params
    	params.require(:group).permit(:name, :banner, :avatar, :require_approval)
  	end
end
