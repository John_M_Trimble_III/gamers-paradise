class TopicsController < ApplicationController
   	before_action :authenticate_user! #-> routes to the login / signup if not authenticated	
	before_action :is_owner?, only: [:edit, :update, :destroy]
	
	def new
		if params[:topicable_type] && params[:topicable_id]
			model = params[:topicable_type].constantize.find(params[:topicable_id])
			@topic = model.topics.new		
		else
			@topic = Topic.new
		end
	end

	def create
		topic = Topic.new(topic_params)
		topic.user = current_user
		topic.is_poll = false
		if topic.save
			flash[:success] = "Topic successfully saved"
			redirect_to topic
		else
			flash[:error] = "Could not save topic"
			redirect_back(fallback_location: root_path)
		end
	end

	def edit
		@topic = Topic.find(params[:id])
	end

	def update
		topic = Topic.find(params[:id])
		topic.assign_attributes(topic_params)
		
		if topic.save
			redirect_to topic
		else
			redirect_back(fallback_location: root_path)
		end
	end

	def show
		@topic = Topic.find(params[:id])
		@topic_comments = @topic.topic_comments.order(created_at: :desc).page params[:page]
	end

	def index 
		if params[:topicable_type] and params[:topicable_id]
			@topicable = params[:topicable_type].constantize.find(params[:topicable_id])
			@topics = @topicable.topics
		else
			@topics = Topic.all
		end


	end

	def destroy
		topic = Topic.find(params[:id])
		if topic.destroy
			flash[:success] = "Topic Successfully Deleted"
			redirect_to topic.topicable
		else
			flash[:danger] = topic.errors.full_messages
			redirect_to 'edit'
		end

	end

private
	
	def topic_params
	    params.require(:topic).permit(:topic, :topicable_type, :topicable_id)
	end	

	def is_owner? 
		topic = Topic.find(params[:id])
		
		if topic.user != current_user
			flash[:warning] = "You are not the owner of #{topic.name}."
			redirect_back(fallback_location: root_path)				
		end
	end	
end
