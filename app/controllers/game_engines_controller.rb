class GameEnginesController < ApplicationController
	include ControllerDraftable
	def index
		@game_engines = GameEngine.published.order(name: :asc)
	end
	
	def show
		@game_engine = GameEngine.find(params[:id])
		@video_games = @game_engine.video_games.page params[:page]
	end
	
	def new
		@game_engine = GameEngine.new
	end

	def edit 
		@game_engine = GameEngine.find(params[:id])
	end


private
	def game_engine_params
		params.require(:game_engine).permit(:name, :description)
	end
end
