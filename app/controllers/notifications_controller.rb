class NotificationsController < ApplicationController
	before_action :authenticate_user!

	def show
		notification = Notification.find(params[:id])
		notification.update_column(:read_at, Time.new)
		redirect_to "#{url_for(notification.notifiable)}##{notification.notifiable.class.name.parameterize}-#{notification.notifiable.id}" if notification.notifiable
		redirect_back(fallback_location: root_path) unless notification.notifiable
	end

	def mark_all_as_read
		current_user.notifications.update_all(read_at: Time.new)
		redirect_back(fallback_location: root_path) 
	end

	def mark_as_read

	end
end
