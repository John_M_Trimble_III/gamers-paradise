class FriendshipsController < ApplicationController
	before_action :authenticate_user!

	def create 
		if current_user.id == params[:user_id].to_i
			friendship = Friendship.new
			friendship.user_id = params[:user_id]
			friendship.friend_id = params[:friend_id]
			
			# Follow user after sending friend request.
			friend = User.friendly.find_by_id(params[:friend_id]) 
			current_user.followed_users << User.find_by_id(params[:friend_id]) unless current_user.followed_users.include?(friend)

			if friendship.save && current_user.save
				flash[:success] = "Wohoo you've asked this gamer to be your friend"
			else
				flash[:warning] = friendship.errors.full_messages
			end
		else
			flash[:warning] = "You Aren't Allowed To Make Friend Requests For Other Users"
    	end
    	  	redirect_back(fallback_location: root_path)		
	end

	def accept
		friendship = Friendship.find(params[:id])
		friendship.confirmed = true
		if friendship.save 
			flash[:success] = "You are friends for life, unless they delete you"
		else
			flash[:warning] = "Oops something went wrong, abandon hope all yee who enter here"
		end
		redirect_back(fallback_location: root_path)		
	end

	def reject
	end

	def index
		@friends = current_user.friends
		@pending_friends = current_user.pending_friends
		@friend_requests = current_user.friend_requests
	end

	def destroy
		friendship = Friendship.find(params[:id])
		if(friendship.destroy)
			flash[:success] = "Friendship destroyed, ruined, abolished"
		else
			flash[:warning] = "Something went wrong"
		end

		redirect_back(fallback_location: root_path)		
	end
	
	def show
		friendship = Friendship.find(params[:id])
		if current_user == friendship.user
			redirect_to friendship.friend
		else
			redirect_to friendship.user
		end

	end
end
