class ShelvesController < ApplicationController
	def index
		@user = User.friendly.find(params[:user_id])
		@shelves = @user.shelves
		@shelved_games = Kaminari.paginate_array(@user.shelved_games.eager_load(:video_game).eager_load(video_game: :main_developers).eager_load(video_game: :ratings).eager_load(video_game: :member_reviews).eager_load(video_game: :rating_users).eager_load(video_game: :reviewing_members).to_a.uniq{|t| t.video_game_id }).page params[:page]
	end

	def show 
		@shelf = Shelf.find(params[:id])
		
		if params[:user_id]
			@user = User.friendly.find(params[:user_id])
		else
			@user = @shelf.user
			params[:user_id] = @user.id
		end
		
		@shelves = @user.shelves
		@shelved_games = @shelf.shelved_games.eager_load(:video_game).eager_load(video_game: :main_developers).eager_load(video_game: :ratings).eager_load(video_game: :member_reviews).eager_load(video_game: :rating_users).eager_load(video_game: :reviewing_members).page params[:page]
	end

	def edit 
		@shelf = Shelf.find(params[:id])
	end

	def create
		user = User.friendly.find(params[:user_id])
		shelf = user.shelves.new(shelf_params)
		if(shelf.save)
			flash[:success] = "You successfully added a new shelf"
			redirect_to [user, user.shelves.last]
		else
			flash[:danger] =  shelf.errors.full_messages
			redirect_back(fallback_location: root_path)		
		end
  	end

  	def update
  		shelf = Shelf.find(params[:id])
		shelf.assign_attributes(shelf_params)
		if(shelf.save)
			flash[:success] = "Succesfully changed your shelf"
			redirect_to [shelf.user, shelf]
		else
			flash[:danger] =  shelf.errors.full_messages
			redirect_back(fallback_location: root_path)
		end
    end

    def destroy
    	shelf = Shelf.find(params[:id])
    	if shelf.destroy
			flash[:success] = "Succesfully deleted your shelf"
			redirect_to user_shelves_url(current_user)
		else
			flash[:danger] =  shelf.errors.full_messages
			redirect_back(fallback_location: root_path)    			
		end
    end

 
private
  def shelf_params
    params.require(:shelf).permit(:name)
  end
end
