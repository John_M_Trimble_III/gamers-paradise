class RatingsController < ApplicationController
	def create
		rateable = params[:rating][:rateable_type].constantize.find(params[:rating][:rateable_id])
		@rating = rateable.ratings.new(rating_params)
		@rating.user = current_user
		if(rateable.save)
			flash[:success] = "Succesfully gave #{rateable.name} #{@rating.score} stars"
		else
			flash[:warning] = @rating.errors.full_messages
		end

		@messages = flash
		
		flash.discard
		respond_to do |format|
			format.js
    	end
	end

	def update
		@rating = Rating.find(params[:id])
		@rating.assign_attributes(rating_params)
		
		if(@rating.save)
			flash[:success] = "Succesfully changed your rating"
		else
			flash[:warning] = @rating.errors.full_messages
		end
		@messages = flash
		
		flash.discard		
		respond_to do |format|
			format.js
    	end			
	end
 
	def destroy
		@rating = Rating.find(params[:id])
		if @rating.destroy
			flash[:success] = "Succesfully removed your rating"
		else
			flash[:warning] = @rating.errors.full_messages
		end
		@messages = flash
		flash.discard		

		respond_to do |format|
			format.js
    	end		
	end

	def index
		@user = User.friendly.find(params[:user_id])
	end

	def show
		rating = Rating.find(params[:id])
		redirect_to rating.rateable
	end

private
  def rating_params
    params.require(:rating).permit(:score)
  end
end
