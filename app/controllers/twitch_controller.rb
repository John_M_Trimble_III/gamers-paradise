require 'net/http'
require 'uri'

class TwitchController < ApplicationController
	def get_videos
		video_game = VideoGame.friendly.find(params[:video_game_id])
		
  		uri = URI.parse("https://api.twitch.tv/helix/games?name=" + URI.encode(video_game.name))
  		request = Net::HTTP::Get.new(uri)
  		request["Client-ID"] = "q95fph16erztqjphszsmvsskbk9toz"

  		req_options = {
  			use_ssl: uri.scheme == "https",
  		}

  		response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  			http.request(request)
  		end
  		
  		if "200" == response.code &&  JSON.parse(response.body)['data'].length > 0
			game_id = JSON.parse(response.body)['data'][0]['id']


			uri = URI.parse("https://api.twitch.tv/helix/videos?game_id=" + game_id)
			request = Net::HTTP::Get.new(uri)
			request["Client-ID"] = "q95fph16erztqjphszsmvsskbk9toz"

			req_options = {
				use_ssl: uri.scheme == "https",
			}

			response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
				http.request(request)
			end

			respond_to do |format|
				format.html
		        format.json { render json: JSON.parse(response.body), status: response.code }
			end
		else

			respond_to do |format|
				format.html
		        format.json { render json: nil, status: 404}
			end
		end
	end
end
