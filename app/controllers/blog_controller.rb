class BlogController < ApplicationController
	def index 
		if BlogPost.any? 
			@categories = BlogCategory.all

			@blog_posts = BlogPost.all
			@blog_posts = @blog_posts.order(created_at: :desc) if (params[:most_recent] || (!params[:popular] && !params[:category_id]))
			@blog_posts = @blog_posts.order(created_at: :desc) if params[:popular]
			@blog_posts = @blog_posts.where(blog_category_id: params[:category_id]) if params[:category_id]
			@blog_posts = @blog_posts.page params[:page]
		else
			flash[:warning] = "Sorry, our blog is under construction, we will have it ready for you soon."
			redirect_to root_path
		end
	end

	def show
		@blog_post = BlogPost.friendly.find(params[:id])	
		@page_description = @blog_post.meta_description if @blog_post.meta_description
    	@page_keywords    = @blog_post.meta_keywords if @blog_post.meta_keywords
	end
end
