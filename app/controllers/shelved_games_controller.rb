class ShelvedGamesController < ApplicationController
	def show
		shelved_game = ShelvedGame.find(params[:id])
		redirect_to [shelved_game.shelf.user, shelved_game.shelf]
	end

	def create
		shelf_id = params[:shelf_id] || params[:shelved_game][:shelf_id]
		shelf = Shelf.find(shelf_id)
		video_game_id = params[:video_game_id] || params[:shelved_game][:video_game_id]
		video_game = VideoGame.find(video_game_id)


		user = current_user
		# Move game it's currently on a restricted shelf
		if (shelved_game = user.preexsiting_shelved_game(video_game)) && !shelf.editable
			shelved_game.shelf = shelf
			if shelved_game.save
				flash[:success] = "Successfully Moved Game To #{shelf.name}"
			else
				flash[:warning] = shelved_game.errors.full_messages
			end
		else
			shelved_game = ShelvedGame.new(shelf_id: shelf_id, video_game_id: video_game_id)
			if shelved_game.save
				flash[:success] = "Successfully Added Game To #{shelved_game.shelf.name}"
			else
				flash[:warning] = shelved_game.errors.full_messages
			end
		end
		redirect_back(fallback_location: root_path)
	end
	def destroy
		shelved_game = ShelvedGame.find(params[:id])
		if(shelved_game.destroy)
			flash[:success] = "Succesfully Removed Game From #{shelved_game.shelf.name}"
		else
			flash[:warning] = shelved_game.errors.full_messages
		end
		
		redirect_back(fallback_location: root_path)
	end

private
  def shelved_game_params
    params.require(:shelved_game).permit(:shelf_id, :video_game_id)
  end	
end
