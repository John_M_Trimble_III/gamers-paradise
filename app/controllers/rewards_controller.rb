class RewardsController < ApplicationController
	def mark_as_seen
		reward = Reward.find(params[:id])
    	respond_to do |format|
			if reward.update_column(:seen_at, Time.now)
				format.json do
					render json: {
						status: 200,
					}.to_json
				end		       				
			else
				format.json do
					render json: {
						status: 500,
					}.to_json
				end					
			end 
        end		
	end
	
	def mark_all_as_seen
		if user_signed_in?
			current_user.user_rewards.where(seen_at: nil).update_all(seen_at: Time.now)
			flash[:success] = "Succesfully marked messages as read"
		else
			flash[:danger] = "Unable to mark messages as read"
		end
		redirect_back(fallback_location: root_path)      		


	end
end
