class PlayerPerspectivesController < ApplicationController
	include ControllerDraftable
	def index
		@player_perspectives = PlayerPerspective.published
	end

	def show
		@player_perspective = PlayerPerspective.find(params[:id])
		@video_games = @player_perspective.video_games.page params[:page]
	end

	def new
		@player_perspective = PlayerPerspective.new
	end

	def edit
		@player_perspective = PlayerPerspective.find(params[:id])
	end

private
	def player_perspective_params
		params.require(:player_perspective).permit(:name)		
	end
end
