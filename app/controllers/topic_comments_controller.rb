class TopicCommentsController < ApplicationController
	def create	
		topic = Topic.find(params[:topic_id])
		comment = topic.topic_comments.new(topic_comment_params)
		comment.user = current_user
		if comment.save
			flash[:success] = "Comment successfully saved"
			redirect_to topic
		else
			flash[:error] = "Could not save comment"
			redirect_back(fallback_location: root_path)
		end

	end

	def show
		topic_comment = TopicComment.find(params[:id])
		redirect_to topic_comment.topic
	end

	def edit
		@topic_comment = TopicComment.find(params[:id])
	end

	def update
		topic_comment = TopicComment.find(params[:id])
		topic_comment.assign_attributes(topic_comment_params)

		if topic_comment.save
			flash[:success] = "Comment successfully saved"
			redirect_to topic_comment.topic
		else
			flash[:error] = topic_comment.errors.full_messages
			redirect_back(fallback_location: root_path)
		end

	end


private 
	def topic_comment_params
	    params.require(:topic_comment).permit(:comment)
	end	
end
