class AnswersController < ApplicationController
	before_action :authenticate_user! #-> routes to the login / signup if not authenticated	
	before_action :is_owner?, only: [:edit, :update, :destroy]

	def create
		answer = Answer.new(answer_params)
		answer.user = current_user
		answer.question = Question.find(params[:question_id])
		if answer.save
      		flash[:success] = "Saved Answer"
		else
      		flash[:error] = "error with saving answer"
		end
	    redirect_back(fallback_location: root_path)

	end

	def edit
		@question = Question.find(params[:question_id])
		@answer = Answer.find(params[:id])
	end

	def update
		answer = Answer.find(params[:id])
		answer.assign_attributes(answer_params)		
		if answer.save
      		flash[:success] = "Updated Answer"
      		redirect_to answer.question
		else
      		flash[:error] = "error with saving answer"
      		redirect_back(fallback_location: root_path)      		
		end
	end 

	def show
		answer = Answer.find(params[:id])
		redirect_to answer.question
	end

	def destroy
		answer = Answer.find(params[:id])
		if answer.destroy
			flash[:success] = "Successfully destroyed answer"
		else
			flash[:danger] = answer.errors.full_messages			
		end
		redirect_to answer.question

	end

private
	def answer_params
	    params.require(:answer).permit(:answer, :question_id)		
	end

	def is_owner? 
		answer = Answer.find(params[:id])
		
		if answer.user != current_user
			flash[:warning] = "You are not the owner of this question."
			redirect_back(fallback_location: root_path)				
		end
	end	
end
