class VideoGamesController < ApplicationController
	include ControllerDraftable
	
	def index
  		@video_games = VideoGame.eager_load(:release_dates).published.order(name: :asc) # creates an anonymous scope
  		@video_games = @video_games.platform_filter(params[:platform_ids]) if params[:platform_ids].present?
  		@video_games = @video_games.franchise_filter(params[:franchise_ids]) if params[:franchise_ids].present?
  		@video_games = @video_games.series_filter(params[:series_ids]) if params[:series_ids].present?  		
  		@video_games = @video_games.type_filter(params[:type_ids]) if params[:type_ids].present?  		
  		@video_games = @video_games.status_filter(params[:status_ids]) if params[:status_ids].present?  		
  		@video_games = @video_games.genres_filter(params[:genre_ids]) if params[:genre_ids].present?  		
  		@video_games = @video_games.game_modes_filter(params[:game_mode_ids]) if params[:game_mode_ids].present?  		
  		@video_games = @video_games.player_perspectives_filter(params[:player_perspective_ids]) if params[:player_perspective_ids].present?  		
		@video_games = @video_games.page(params[:page]).per(15)
	end

	def new

		if params[:video_game]
			@video_game = VideoGame.new(video_game_params)
		else
			@video_game = VideoGame.new
			@video_game.release_dates.build
			@video_game.companies_video_games.build
			@video_game.videos.build
			@video_game.screenshots.build
			@video_game.multiplayer_modes.build			
		end
	end

	def show 
		@video_game = VideoGame.published.friendly.find(params[:id])	
		@post_to_edit = Post.new(wall: @video_game, owner: current_user)
		set_meta_tags og: {
			title:    @video_game.name,
            url:      video_game_url(@video_game),
            image:    @video_game.box_art
        }
        @page_description = @video_game.description if @video_game.description

 	end

	def edit 
		@video_game = VideoGame.published.friendly.find(params[:id])
	end

	def explore
		@hide_ads = true
		@popular_games = VideoGame.left_joins(:follows).group(:id).order('COUNT(follows.id) DESC').group('drafts.id').published.limit(20)
		@recently_viewed_games = VideoGame.published.limit(10)
		@popular_lists = List.left_joins(:likes).group(:id).order('COUNT(likes.id) DESC').limit(5)
	end

    def select_search
		search_results = VideoGame.search(params[:search])
		
		video_games = Array.new

		search_results.each do |video_game|
			video_game_data = {}
			video_game_data["name"] = video_game.name
			video_game_data["id"] = video_game.id
			video_game_data["box_art"] = video_game.box_art.file.url if video_game.box_art.file		
			video_game_data["avg_rating"] = video_game.average_rating
			video_games.push(video_game_data)
		end

    	respond_to do |format|
  			format.json { render json: {results: video_games} }
		end
    end

private
  def video_game_params
  	if current_user.admin? 	
    	params.require(:video_game).permit(:name, :box_art, :official_website, :description, :release_date, :official_website, :type_of, :status, :story, :wikia_page, :wikipedia_page, :facebook_page, :twitter_profile, :twitch_channel, :instagram_profile, :youtube_channel, :itunes_page_iphone, :itunes_page_ipad, :google_play_store_page, :steam_store_page, :subreddit_page, :esrb_rating, :esrb_synopsis, :pegi_rating,  :pegi_synopsis, :videos_attributes => [:id, :video, :_destroy], :screenshots_attributes => [:id, :screenshot, :_destroy] , :release_dates_attributes => [:id, :platform_id, :date, :region, :upc, :asin, :_destroy], :multiplayer_modes_attributes => [:id, :platform_id, :online_co_op_max_players, :offline_co_op_max_players, :online_max_players, :offline_max_players, :is_online_co_op, :is_offline_co_op, :is_lan_co_op, :is_co_op_campaign, :is_online_split_screen, :is_offline_split_screen, :is_drop_in_drop_out, :_destroy], :companies_video_games_attributes => [:id, :company_id, :_destroy, :type_ofs => []], :esrb_themes => [], :pegi_themes => [],:game_engine_ids => [],  :platform_ids => [], :game_mode_ids => [], :player_perspective_ids => [], :franchise_ids => [], :series_ids => [], :genre_ids => [], :video_game_child_ids => [], :child_video_game_ids => [], :parent_video_game_ids => []
)
    else
    	params.require(:video_game).permit(:name, :box_art, :official_website, :description, :release_date, :official_website, :type_of, :status, :story, :wikia_page, :wikipedia_page, :facebook_page, :twitter_profile, :twitch_channel, :instagram_profile, :youtube_channel, :itunes_page_iphone, :itunes_page_ipad, :google_play_store_page, :steam_store_page, :subreddit_page, :esrb_rating,  :esrb_synopsis, :pegi_rating,  :pegi_synopsis, :videos_attributes => [:video, :_destroy], :screenshots_attributes => [:screenshot, :_destroy] ,:release_dates_attributes => [ :platform_id, :date, :region, :upc, :asin, :_destroy], :multiplayer_modes_attributes => [:platform_id, :online_co_op_max_players, :offline_co_op_max_players, :online_max_players, :offline_max_players, :is_online_co_op, :is_offline_co_op, :is_lan_co_op, :is_co_op_campaign, :is_online_split_screen, :is_offline_split_screen, :is_drop_in_drop_out, :_destroy], :companies_video_games_attributes => [:company_id, :_destroy, :type_ofs => []], :esrb_themes=> [], :pegi_themes=> [], :game_engine_ids => [], :platform_ids => [], :game_mode_ids => [], :player_perspective_ids => [], :franchise_ids => [], :series_ids => [], :genre_ids => [], :video_game_child_ids => [], :child_video_game_ids => [], :parent_video_game_ids => []
)
    end
  end
end
