class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  impressionist actions: [:show], unique: [:impressionable_type, :impressionable_id, :session_hash]
  before_action :set_current_user
  before_action :mark_rewards_as_seen
  before_action :set_raven_context
  before_action :get_user_notifications
  before_action :set_default_meta

  def set_current_user
  	User.current = current_user
  end  

  def mark_rewards_as_seen
  	if user_signed_in? 
  		@award_notices ||= current_user.user_rewards.where(seen_at: nil)
  	end
  end

  def after_sign_in_path_for(resource)
    feed_path
  end

  def get_user_notifications
    if user_signed_in? 
      @notifications = current_user.notifications.eager_load(:actor).order(created_at: :desc).page(params[:notifications_page])
    end
  end

  private

  def set_raven_context
    Raven.user_context(id: session[:current_user_id], foo: :bar)
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end

  def set_default_meta
    @page_title       = 'Gamer\'s Paradise Page'
    @page_description = 'Discover your next favorite game.'
    @page_keywords    = 'Video Games, Discover, Connect, Compete'
  end
end
