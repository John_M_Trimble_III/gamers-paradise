class CommentsController < ApplicationController
	def show
		comment = Comment.find(params[:id])
		redirect_to comment.commentable
	end

	def new
		@commentable = params[:commentable_type].constantize.find(params[:commentable_id])
    	@parent = Comment.find(params[:parent_id])

    	respond_to do |format|
        	format.js
        end
    end

	def create
		commentable = params[:comment][:commentable_type].constantize.find(params[:comment][:commentable_id])
			
		@comment = commentable.comments.build(comment_params)
		@comment.body = params.select{|key,value| key.start_with?("mce_")}.values.first

		if @comment.save
			flash[:success] = "Successfully Created Comment."
		else
			flash[:danger] = @comment.errors.full_messages
		end
    	
    	respond_to do |format|
        	format.js
        end


	end

    def edit
    	@comment = Comment.find(params[:id])
    	respond_to do |format|
        	format.js
        end
    end

    def update
    	@comment = Comment.find(params[:id])
    	@comment.assign_attributes(comment_params)
		@comment.body = params.select{|key,value| key.start_with?("mce_")}.values.first
    	@comment.save

    	respond_to do |format|
        	format.js
        end    	
    end


	def destroy
		@comment = current_user.comments.find(params[:id])
		@comment.destroy
    	respond_to do |format|
        	format.js
        end  		
	end


	private
  def comment_params
    params.require(:comment).permit(:body, :user_id, :parent_id)
  end
end
