class QuestionsController < ApplicationController
	before_action :authenticate_user! #-> routes to the login / signup if not authenticated	
	before_action :is_owner?, only: [:edit, :update, :destroy]

	def create
		question = Question.new(question_params)
		question.user = current_user
		if question.save
			flash[:success] = "Your Question Was Succesfully Saved"
		else
			flash[:warning] = "Something went wrong with your question"
		end
	    redirect_back(fallback_location: root_path)
	end


	def edit
		@question = Question.find(params[:id])
	end

	def update
		question = Question.find(params[:id])
		question.assign_attributes(question_params)
		question.user = current_user
		if question.save
			flash[:success] = "Your Question Was Updated Sucesfully"
		else
			flash[:warning] = "Your Question Was Unable To Be Updated"
		end
	    redirect_back(fallback_location: root_path)
	end

	def show
		@question = Question.find(params[:id])
	end

	def index
		if params[:questionable_type] && params[:questionable_id]		
			@questionable = params[:questionable_type].constantize.find(params[:questionable_id])
			@questions = @questionable.questions
		else
			@questions = Question.all
		end
	end



	def destroy
		question = Question.find(params[:id])
		if question.destroy
			flash[:success] == "Successfully Destroyed Question"
			redirect_to question.questionable
		else
			flash[:danger] = question.errors.full_messages
			render 'edit'
		end
	end

private

	def question_params
	    params.require(:question).permit(:question, :questionable_type, :questionable_id)
	end

	def is_owner? 
		question = Question.find(params[:id])
		
		if question.user != current_user
			flash[:warning] = "You are not the owner of this question."
			redirect_back(fallback_location: root_path)				
		end
	end	
end
