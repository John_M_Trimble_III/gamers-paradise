class LikesController < ApplicationController
	def like
		model = params[:likeable_type].constantize
		liked_model = model.find(params[:likeable_id])
		user = current_user
		like = Like.create(user: user, likeable: liked_model)
		@likeable = liked_model
		respond_to do |format|
			format.js
    	end
	end

	def unlike
		model = params[:likeable_type].constantize
		liked_model = model.find(params[:likeable_id])
		user = current_user
		like = user.likes.find_by(likeable_id: params[:likeable_id], likeable_type: params[:likeable_type])
		like.destroy!
		@likeable = liked_model
		respond_to do |format|
			format.js
    	end		
	end

	def show
		like = Like.find(params[:id])
		redirect_to like.likeable
	end
end
