class ApiOrdersController < BaseApiController
  skip_before_action :verify_authenticity_token

  before_action only: :create do
    unless @json.has_key?('order') && @json['order'].respond_to?(:[]) && @json['order']['order_number']
      render body: nil, status: :bad_request
    end
  end

  before_action only: :create do
    @order = Order.find_by_external_id(@json['order']['external_id'])
  end

  def create
    if @order.present?
      render body: nil, status: :conflict
    else
      @order = Order.new(@json['order'])
      # Need to find user
      @user = User.find_by_magento_id(@json['magento_user_id']) 
      @order.user = @user

      if @order.user && @order.save
        render json: @order
      else
         render nothing: true, status: :bad_request
      end
    end
  end

  def update
    # TODO: allow removal of reward if order is canceled
  end
 end