class VotesController < ApplicationController

	def upvote
		model = params[:likeable_type].constantize
		liked_model = model.find(params[:likeable_id])
		user = current_user
		like = Like.create(user: user, likeable: liked_model)
		@likeable = liked_model
		
		respond_to do |format|
			format.js
    	end
	end

	def unvote
		model = params[:likeable_type].constantize
		liked_model = model.find(params[:likeable_id])
		user = current_user
		like = user.likes.find_by(likeable_id: params[:likeable_id], likeable_type: params[:likeable_type])
		like.destroy!
		@likeable = liked_model
		
		respond_to do |format|
			format.js
    	end		
	end
end
