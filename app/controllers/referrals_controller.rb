class ReferralsController < ApplicationController
	def index
	end
	def show
		referral = Referral.find(params[:id])
		not_me = current_user == referral.referrer ? referral.referee : referral.referrer 
		redirect_to not_me
	end
end
