class SeriesController < ApplicationController
	include ControllerDraftable

	def index
		@series = Series.published.order(name: :asc)
	end

	def new
		@series = Series.new
	end

	def show
		@series = Series.find(params[:id])
		@video_games = @series.video_games.published.page params[:page]
	end

	def edit
		@series = Series.find(params[:id])		
	end	

private
	def series_params
		params.require(:series).permit(:name)
	end
end
