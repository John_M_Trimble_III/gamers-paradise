class ContributionsController < ApplicationController
	def show
		contribution = Contribution.find(params[:id])
		redirect_to contribution.contributable
	end
	
	def index 
		@user = User.friendly.find(params[:user_id])
		@contributions = @user.contributions.page params[:page]
	end
	
	def breakdown
		@user = User.friendly.find(params[:id])
		@contributions = @user.contributions.page params[:page]	
		@breakdown = @contributions.type_breakdown		
	end
end
