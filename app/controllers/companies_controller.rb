class CompaniesController < ApplicationController
	include ControllerDraftable

	def index
		@companies = Company.published.order(name: :asc)
	end

	def show
		@company = Company.find(params[:id])
		@video_games = @company.video_games.published.page params[:page]
	end
	
	def new
		@company = Company.new
	end

	def edit
		@company = Company.find(params[:id])
	end

private
	def company_params
		params.require(:company).permit(:name, :description, :website)		
	end


end
