class PlatformsController < ApplicationController
	include ControllerDraftable
	def index
		@platforms = Platform.published.order(name: :asc)
	end

	def show
		@platform = Platform.find(params[:id])
		@video_games = @platform.video_games.published.page params[:page]
	end
	def new
		@platform = Platform.new
	end

	def edit
		@platform = Platform.find(params[:id])
	end

private
	def platform_params
		params.require(:platform).permit(:name)
	end	
end
