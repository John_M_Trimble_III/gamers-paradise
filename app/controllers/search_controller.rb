class SearchController < ApplicationController
  def index
  	@results = Searchkick.search params[:q], index_name: [VideoGame], page: params[:page], per_page: 20
	set_meta_tags noindex: true, follow: true
  end
end
