module ControllerDraftable
  extend ActiveSupport::Concern
    included do
	  	before_action :authenticate_user!, only: [:create, :update, :destroy, :new, :edit]
	  	after_action :auto_approve_for_arbiters, only: [:create, :update, :destroy]
	end

	def create
		model = controller_name.classify.constantize
		draft = model.new.drafts.new
		draft.user = current_user
		params_method = controller_name.singularize + "_params"
		draft.type_of = "New"		
		draft.modified = model.new(self.send(params_method))
		if draft.modified.valid? && draft.save
			@draft = draft
			object = @draft.original ? @draft.original : @draft.modified			
			flash[:success] = "Draft For #{object.name} has been submitted for moderation."

			redirect_to video_games_path
		else
			flash[:warning] = draft.modified.errors.full_messages
			redirect_to action: :new, controller_name.singularize.to_sym => self.send(params_method)
    	end
  	end

  	def update
   		model = controller_name.classify.constantize
    	edited_model = model.find(params[:id])
		params_method = controller_name.singularize + "_params"

		draft = edited_model.drafts.new
		draft.user = current_user
		draft.type_of = "Update"
		draft.modified = model.new(self.send(params_method))
		# Need to copy existing screenshots that haven't been deleted
		# Special Cases
		if(params["video_game"] && params["video_game"]["screenshots_attributes"])
	    	params["video_game"]["screenshots_attributes"].each do |key, screenshot|
	    		id = screenshot[:id]
	    		is_destroy = screenshot[:_destroy]
	    		is_screenshot = screenshot[:screenshot]
	    		if("false" == is_destroy && !is_screenshot)
	    			original_screenshot = Screenshot.find(screenshot[:id])
	    			new_screenshot = original_screenshot.dup
					new_screenshot.remote_screenshot_url = original_screenshot.screenshot_url
	    			draft.modified.screenshots << new_screenshot
	    		end
	    	end
    	end

		draft.object  = edited_model.detect_changes(draft.modified)
		unless draft.object.empty?
    		if draft.modified.valid? && edited_model.save
    			@draft = draft
				object = @draft.original ? @draft.original : @draft.modified			
				flash[:success] = "Your edits for #{object.name} have been submitted for moderation."    			
				redirect_to video_games_path
			end
		else
			flash[:warning] = "You did not make any changes."
			redirect_to draft.original
		end
    end

    def destroy
  		model = controller_name.classify.constantize
    	edited_model = model.find(params[:id])
		params_method = controller_name.singularize + "_params"

		draft = edited_model.drafts.new
		draft.modified = edited_model
		draft.type_of = "Delete"
		draft.user = current_user
    	if edited_model.save
    		@draft = draft
			redirect_to root_path
		end
    end

    def history
		model = controller_name.classify.constantize
		@contributions = model.find(params[:id]).contributions
    end

	def auto_approve_for_arbiters
		 if (user_signed_in?) && (current_user.has_role? :arbiter) && (defined? @draft)
		 	# If succesfully accepted
		 	if @draft.modified.class.accept(@draft)
		 		object = @draft.original ? @draft.original : @draft.modified
		 		flash[:success] = "Draft For #{object.name} Has Been Approved Automatically Since You Are An Arbiter"
		 	end
		 end
	end

end











