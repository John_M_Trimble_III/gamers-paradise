class ListingsController < ApplicationController
	def index
		@video_game = VideoGame.friendly.find(params[:video_game_id])
		@listings = @video_game.listings
	end

	def new
		@video_game = VideoGame.friendly.find(params[:video_game_id])
		@listing = Listing.new
	end

	def show 
		@video_game = VideoGame.friendly.find(params[:video_game_id])
		@listing = Listing.find(params[:id])
	end

	def edit 
		@listing = Listing.find(params[:id])
	end

	def create
		@listing = Listing.new(listing_params)
		if @listing.save
			flash[:success] = "Your Copy Is Listed, enjoy a beer"
			@game = VideoGame.friendly.find(params[:video_game_id])
			redirect_to [@game, @listing]
		else
			flash[:error] = "You done fucked up"
		end


  	end

  	def update
    	@listing = Listing.find(params[:id])
    	if @listing.update_attributes(listing_params)
      		flash[:success] = "Game updated"
      		redirect_to @listing
    	else
    		flash[:error] = "Did not update"
      		render 'edit'
    	end
    end

 
private
  def listing_params
    params.require(:listing).permit(:price, :currency, :video_game_id, :platform_id, :user_id)
  end
end
