class WikisController < ApplicationController
	skip_before_action :verify_authenticity_token  
   	before_action :authenticate_user!, only: [:new, :create, :edit, :update]	
	include ReverseProxy::Controller

	def index
		@wikis = Wiki.all
	end

	def new
		@wiki = Wiki.new
	end

	def create
		# Need to tell the other service (the wiki service) to create a new wiki
		@wiki = Wiki.new(wiki_params)
		@wiki.user = current_user
		
		respond_to do |format|
			if @wiki.save
				# Now do the hard work of telling media wiki to add a family member
				@result = HTTParty.post("#{Rails.application.config.cerberus_url}/wiki", 
					:body => { 
						:action => 'CREATE', 
						:slug => @wiki.slug, 
	               		:name => @wiki.name, 
	               		:id => @wiki.id,
	               		:logo_url => @wiki.wiki_logo.media_wiki.url
	             	}.to_json,
	    		:headers => { 'Content-Type' => 'application/json' } )
				@result = JSON.parse(@result.response.body)
				if "success" == @result['status']
					format.json do
						render json: {
	    					status: 200,
	    					location: url_for(@wiki)
	    				}.to_json
	    			end			
	    		else
	    			flash[:danger] = "Sorry, something went wrong while we were creating #{@wiki.name} wiki"
	    			format.json do
	    				render json: {
		    				status: 500,
	    					location: new_wiki_path
	    				}, status: 500
	    			end	
	    		end
			else
				flash[:danger] = "Sorry, something went wrong while we were creating  #{@wiki.name} wiki"
				format.json do
					render json: {
						status: 500,
						location: new_wiki_path
					}, status: 500
				end	
			end
		end
	end

	def edit
		@wiki = Wiki.find(params[:id])
	end

	def update
		# Need to tell the other service (the wiki service) to create a new wiki
		@wiki = Wiki.find(params[:id])
		@wiki.assign_attributes(wiki_params)
		
		respond_to do |format|
			if @wiki.save
				# Now do the hard work of telling media wiki to add a family member
				@result = HTTParty.post("#{Rails.application.config.cerberus_url}/wiki", 
					:body => { 
						:action => 'EDIT', 
						:slug => @wiki.slug, 
	               		:name => @wiki.name, 
	               		:id => @wiki.id,
	               		:logo_url => @wiki.wiki_logo.media_wiki.url
	             	}.to_json,
	    		:headers => { 
	    			"Content-Type" => "application/json", 
	    			"X-External-ID" => "#{@wiki.id}" 
	    		} )
				@result = JSON.parse(@result.response.body)
				if "success" == @result['status']
					format.json do
						render json: {
	    					status: 200,
	    					location: url_for(@wiki)
	    				}.to_json
	    			end			
	    		else
	    			flash[:danger] = "Sorry, something went wrong while we were creating #{@wiki.name} wiki"
	    			format.json do
	    				render json: {
		    				status: 500,
	    					location: new_wiki_path
	    				}, status: 500
	    			end	
	    		end
			else
				flash[:danger] = "Sorry, something went wrong while we were creating  #{@wiki.name} wiki"
				format.json do
					render json: {
						status: 500,
						location: new_wiki_path
					}, status: 500
				end	
			end
		end

	end

	def destroy
		@wiki = Wiki.find(params[:id])
		if @wiki.destroy
			redirect_to wikis_path
		else
			redirect_to edit_wiki_path(@wiki)
		end
	end

	def show
		@wiki = Wiki.find(params[:id])
		reverse_proxy "#{Rails.application.config.cerberus_url}", path_name: "/wikis/#{params[:path]}",  headers: { "HOST" => request.host, "X-Forwarded-Host" => request.host, "X-Forwarded-Port" => request.port.to_s, "X-Forwarded-Proto" => request.scheme, "X-External-ID" => "#{@wiki.id}"}
	end

private
	def wiki_params
		params.require(:wiki).permit(:name, :wiki_logo, :video_game_ids => [])
	end	
end
