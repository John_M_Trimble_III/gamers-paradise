class PagesController < ApplicationController
	before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
	
	def index
		@pages = Page.all
	end

	def new
		@page = Page.new(owner: current_user)
	end

	def create
		page = Page.new(page_params)
		page.owner = current_user

		if(page.save)
			flash[:success] = "Successfully created new page"
			redirect_to page
		else
			flash[:warning] = page.errors.full_messages
			redirect_back(fallback_location: root_path)
		end

	end 

	def show
		@page = Page.friendly.find(params[:id])	
	    @posts = @page.posts.order(created_at: :desc).page(params[:page]).per(10)
	    @post_to_edit = Post.new(wall: @page, owner: current_user)
	end

	def edit
		@page = Page.friendly.find(params[:id])
	end

  	def update
    	page = Page.friendly.find(params[:id])
    	page.remove_banner! if "1" == params[:page][:delete_banner]
    	page.remove_avatar! if "1" == params[:page][:delete_avatar]
    	if page.update_attributes(page_params)
      		flash[:success] = "Page updated"
      		redirect_to page
    	else
    		flash[:error] = page.errors.full_messages
      		render 'edit'
    	end
    end

    def destroy
    	page = Page.friendly.find(params[:id])
    	if page.destroy
    		flash[:success] = "Successfully destroyed the page"
    	else
    		flash[:error] = page.errors.full_messages
    	end
    	redirect_back(fallback_location: root_path)
    end

private

	def page_params
    	params.require(:page).permit(:name, :rss_url, :banner, :avatar)
  	end
end
