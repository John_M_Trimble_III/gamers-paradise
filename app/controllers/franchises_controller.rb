class FranchisesController < ApplicationController
	include ControllerDraftable

	def index
		@franchises = Franchise.published.order(name: :asc)
	end

	def new
		@franchise = Franchise.new
	end

	def show
		@franchise = Franchise.find(params[:id])
		@video_games = @franchise.video_games.published.page params[:page]
	end

	def edit
		@franchise = Franchise.find(params[:id])
	end

private
	def franchise_params
		params.require(:franchise).permit(:name, :description)
	end

end
