class ListsController < ApplicationController
	before_action :authenticate_user!, except: [:index, :show] #-> routes to the login / signup if not authenticated	
	before_action :is_owner?, only: [:edit, :update, :destroy]

	def index
		@lists = List.all
		@lists = @lists.where(user_id: current_user.id) if params[:i_created].present?
		@lists = List.where(id: List.includes(:likes).includes(list_video_games: :likes).where('likes_list_video_games.user_id' => current_user.id).pluck(:id)) if params[:i_voted].present?
		@lists = @lists.includes(:likes).where('likes.user_id' => current_user.id)if params[:i_liked].present?
		@lists = @lists.left_joins(:likes).left_joins(:list_video_games).group(:id).order('COUNT(list_video_games.id) DESC').order('COUNT(likes.id) DESC').page(params[:page]).per(15)
	end

	def show
		@list = List.friendly.find(params[:id])	
		@list_video_game = ListVideoGame.new(user: current_user, list: @list)
	end
	
	def new
		@list = List.new
	end
	
	def create
		list = List.new(list_params)
		list.user = current_user
		if list.save
			redirect_to list
			flash[:success] = "Successfully created new list"
		else
			flash[:error] = list.errors.full_messages
			redirect_back(fallback_location: root_path)		
		end
	end
	
	def edit
		@list = List.friendly.find(params[:id])
	end
	
	def update
		list = List.friendly.find(params[:id])
		list.assign_attributes(list_params)
		if list.save
			flash[:success] = "Successfully edited list"
			redirect_to list
		else
			flash[:error] = list.errors.full_messages
			redirect_back(fallback_location: root_path)				
		end
	end
	
	def destroy
		list = List.find(params[:id])
		if list.destroy
			flash[:success] = "Successfully destroyed list"
			redirect_to lists_path
		else
			flash[:error] = list.errors.full_messages
			redirect_back(fallback_location: root_path)				
		end
	end

private
	def list_params
    	params.require(:list).permit(:name, :description)		
	end	
	
	def is_owner? 
		list = List.friendly.find(params[:id])
		
		if list.user != current_user
			flash[:warning] = "You are not the owner of this list."
			redirect_back(fallback_location: root_path)				
		end
	end
end
