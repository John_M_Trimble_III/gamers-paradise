class AttachmentsController < ApplicationController
	def create
		attachment = Attachment.new
		attachment.file = params[:file]
		respond_to do |format|
			if attachment.save
  				format.json do
    				render json: {
    					status: 200,
    					message: attachment
    				}.to_json
    			end			
    		else
    			format.json do
    				render json: {
    					status: 400,
    					message: attachment.errors.full_messages
    				}.to_json
    			end	
    		end
		end
	end
	
	def destroy
		attachment = Attachment.find(params[:id])
		

		respond_to do |format|
			if attachment.destroy
  				format.json do
    				render json: {
    					status: 200
    				}.to_json
    			end			
    		else
    			format.json do
    				render json: {
    					status: 400,
    					message: attachment.errors.full_messages
    				}.to_json
    			end	
    		end
		end		
	end

end
