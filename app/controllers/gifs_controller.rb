class GifsController < ApplicationController
	def index
		respond_to do |format|
			if params[:q]
				format.json do
					render json: {
						status: 200,
						gifs: Giphy.search(params[:q], {limit: 10, offset: params[:page] || 0})
					}.to_json
				end			
			else
				format.json do
					render json: {
						status: 200,
						gifs: Giphy.trending(limit: 15)
					}.to_json
				end				
			end	
		end
	end
end
