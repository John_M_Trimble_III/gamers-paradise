class MemberReviewsController < ApplicationController
	def create
		member_review = MemberReview.new(member_review_params)
		if(member_review.save)
			flash[:success] = "Woohooo Your totally awesome review has been saved and posted"
		else
			flash[:warning] = "Why does everyone help me"
		end
		redirect_back(fallback_location: root_path)
	end

	def index
		@user = User.friendly.find(params[:user_id])
	end

	def edit
		@member_review = MemberReview.find(params[:id])
		@video_game = @member_review.video_game
	end

	def update
		member_review = MemberReview.find(params[:id])
		member_review.assign_attributes(member_review_params)		
		if member_review.save
      		flash[:success] = "Updated Review"
      		redirect_to member_review.video_game
		else
      		flash[:error] = "Error Updating Your Review"
		    redirect_back(fallback_location: root_path)	
		end
	end


	def show
		@member_review = MemberReview.find(params[:id])
	end

	def destroy
		member_review = current_user.member_reviews.find_by_id(params[:id])
		if member_review.destroy
			flash[:success] = "You have succesfully eviscerated your review"
			redirect_to member_review.video_game
		else
			flash[:warning] = "Woops something went wrong you couldn't delete the review"
			redirect_back(fallback_location: root_path)	
		end
	end

	private
  	def member_review_params
    	params.require(:member_review).permit(:body, :user_id, :rating, :video_game_id)
  	end	
end
