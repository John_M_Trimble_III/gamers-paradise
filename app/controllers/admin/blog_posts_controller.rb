class Admin::BlogPostsController < Admin::AdminController
	def index
		@blog_posts = BlogPost.all.page params[:page]
	end

	def new
		@blog_post = BlogPost.new
	end
	
	def create
		@blog_post = BlogPost.new(blog_post_params)
		@blog_post.save
		redirect_to admin_blog_posts_path		
	end
	
	def edit
		@blog_post = BlogPost.find(params[:id])
	end

	def update
		@blog_post = BlogPost.find(params[:id])
		@blog_post.assign_attributes(blog_post_params)
		@blog_post.save
		redirect_to admin_blog_posts_path
	end

	def destroy
		@blog_post = BlogPost.find(params[:id])
		@blog_post.destroy
		redirect_to admin_blog_posts_path
	end

private 
	def blog_post_params		
		params.require(:blog_post).permit(:title, :content, :blog_category_id, :featured_image, :author_id,  :meta_description, :meta_keywords)
	end

end
