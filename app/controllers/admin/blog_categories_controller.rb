class Admin::BlogCategoriesController <  Admin::AdminController
	def index
		@categories = BlogCategory.all.page params[:page]
	end

	def new
		@category = BlogCategory.new
	end
	
	def create
		@category = BlogCategory.new(blog_post_category_params)
		@category.save

		redirect_to admin_blog_categories_path
	end
	
	def edit
		@category = BlogCategory.find(params[:id])
	end
	
	def update
		@category = BlogCategory.find(params[:id])
		@category.assign_attributes(blog_post_category_params)
		@category.save
		redirect_to admin_blog_categories_path
	end
	
	def destroy
		@category = BlogCategory.find(params[:id])
		@categroy.destroy
		redirect_to admin_blog_categories_path
	end

private
	def blog_post_category_params
		params.require(:blog_category).permit(:name)
	end
end
