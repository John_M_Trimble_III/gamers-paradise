# app/controllers/admin_controller.rb
class Admin::AdminController < ApplicationController
  layout 'admin/layouts/application'	
  before_action :authorized?

  private
  def authorized?
    if !user_signed_in? || !current_user.has_role?(:admin)
      flash[:error] = "You are not authorized to view that page."
      redirect_to root_path
    end
  end
end