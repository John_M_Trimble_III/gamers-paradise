class DraftsController < ApplicationController
  before_action :is_arbiter?
  after_action :send_notification, only: [:accept, :deny]
  def index
    @drafts = Draft.grouped_drafts
	end
	
  	def show
    	@draft = Draft.find(params[:id])
    	# Modified
    	@modified = @draft.modified

    	# Original
    	@original = @draft.original  
  end
      # Post draft ID here to publish it
    def accept
      @draft = Draft.find(params[:id]) # We are either creating a new model or updating one
      status = @draft.modified_type.constantize.accept(@draft)

      if status 
        flash[:success] = "Eureka, enjoy a beer"
        next_draft = @draft.get_next_draft
        if next_draft
          redirect_to draft_path(next_draft)
        else
          redirect_to drafts_path
        end
      else        
        flash[:warning] = "Something went wrong, abandon ship"
        redirect_to :back
      end
    end

    # Post draft ID here to revert it
    def deny
      @draft = Draft.find(params[:id]) # We are either creating a new model or updating one
      next_draft = @draft.get_next_draft
      modified = @draft.modified
      status = modified.destroy
      @draft.destroy
      status = status
      if status
        flash[:success] = "Eureka, enjoy a beer"

        if next_draft
          redirect_to draft_path(next_draft)
        else
          redirect_to drafts_path
        end
      
      else
        flash[:warning] = "Something went wrong, abandon ship"
        redirect_to :back
      end
    end

private
  def is_arbiter?
    if (!user_signed_in?) || !(current_user.has_role? :arbiter)
      flash[:warning] = "You Do Not Have Permissions To Access Drafts."
      redirect_to root_path
    end
  end

  def send_notification
    message = nil
    object = @draft.original ? @draft.original : @draft.modified
    if 'accept' == action_name
      message = "Your changes to #{object.name} have been accepted"
    elsif 'deny' == action_name
      message = "Your changes #{object.name} have been denied"
    end
    GenerateNotificationsWorker.perform_async(nil, nil, message, current_user.id, [@draft.user.id])    
  end

end
