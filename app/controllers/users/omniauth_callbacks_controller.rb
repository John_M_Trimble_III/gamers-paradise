class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    create
  end

  def twitch
    create
  end

  def finish_signup
  end

  def create_signup
    
    auth_params = params['omniauth_params']

    provider = AuthenticationProvider.where(name: auth_params['provider']).first
    authentication = provider.user_authentications.where(uid: auth_params['uid']).first
    auth_params['info']['email'] = params['email']
    if User.find_by_email(auth_params['info']['email'])
      flash[:warning] = "Sorry, that user is already in our system"
      redirect_to users_omniuath_finish_signup_path(request.parameters.except(:controller, :action, :email))
    else
      create_user_and_authentication_and_sign_in(auth_params, provider)
    end
  end

  private

    def create
      auth_params = request.env["omniauth.auth"]
      provider = AuthenticationProvider.where(name: auth_params.provider).first
      authentication = provider.user_authentications.where(uid: auth_params.uid).first
      existing_user = current_user || User.where('email = ?', auth_params['info']['email']).first
      
      if authentication
        sign_in_with_existing_authentication(authentication)
      elsif existing_user
        create_authentication_and_sign_in(auth_params, existing_user, provider)
      else
        create_user_and_authentication_and_sign_in(auth_params, provider)
      end
    end

    def sign_in_with_existing_authentication(authentication)
      sign_in_and_redirect(:user, authentication.user)
    end

    def create_authentication_and_sign_in(auth_params, user, provider)
      UserAuthentication.create_from_omniauth(auth_params, user, provider)
      sign_in_and_redirect(:user, user)
    end

    def create_user_and_authentication_and_sign_in(auth_params, provider)
      user = User.create_from_omniauth(auth_params)
      if user.valid?
        create_authentication_and_sign_in(auth_params, user, provider)
        
        if token = cookies['referrer_token']
          referring_user = User.find_by(referral_token: token)
          if referring_user 
            Referral.create(referrer: referring_user, referee: user, points: 100)
            cookies.delete :referrer_token
          end
        end
      else
        flash[:warning] = "You need to add your email as we were unable to retrieve it from #{provider.name}"
        redirect_to users_omniuath_finish_signup_path(omniauth_params: auth_params)
      end
    end
end
