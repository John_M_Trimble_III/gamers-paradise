# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
  before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    cookies['referrer_token'] = params[:referrer] if params[:referrer]
    super
  end

  # POST /resource
  def create
    super
    # After user is succesfuly created need to add referral
    if token = cookies['referrer_token']
      user = User.find_by(referral_token: token)
      if user 
        Referral.create(referrer: user, referee: @user, points: 100)
        cookies.delete :referrer_token

        unless @user.friend?(user)
          Friendship.create(user: @user, friend: user, created_at: Time.now, updated_at: Time.now, confirmed: true)
          User.current=@user
        end
      end
    end 
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  def update
    super
    if "1" == params[:user][:delete_cover_photo]
      @user.remove_cover_photo!
    end
    if "1" == params[:user][:delete_avatar]
      @user.remove_avatar!
    end    
    @user.save if @user.changed?
  end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:username, :avatar, :cover_photo, :birthday, :my_story, :facebook_profile, :twitter_profile, :twitch_profile, :instagram_profile, :youtube_profile, :steam_profile, :linkedin_profile, :pinterest_profile, :soundcloud_profile, :google_plus_profile, :reddit_profile, :battlenet_profile, :origin_profile, :uplay_profile, :discord_profile, :daily_summary_email_opt_in])
  end

  def update_resource(resource, params)
    # Require current password if user is trying to change password.
    return super if params["password"]&.present?

    # Allows user to update registration information without password.
    resource.update_without_password(params.except("current_password"))
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
