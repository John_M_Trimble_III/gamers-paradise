$(function() {
	if($('body.video_games.edit, body.video_games.new').length > 0)
	{
		// Deal with Select Boxes
		$('.select2').not('#video_game_child_video_game_ids + .select2, #video_game_parent_video_game_ids + .select2').select2({ width: '100%' });

		$( "#video_game_type_of" ).change(function(event){
			toggleDependentFields(event.target);
		});

		toggleDependentFields($( "#video_game_type_of" ));

		$('body').on('cocoon:after-insert', function(e, insertedItem) {
			insertedItem.find('.select2').select2({ width: '100%' });
		  });		

	}
	else if ($('body.video_games.show').length > 0)
	{

		//activate chart.js  
		var ratingCanvas = $('#member-rating');
  		var currRating = ratingCanvas.data('rating') * 10  || 0;
  		//use data and options
		//data variables defined
		var maxRating = 100;

		//data
		var pieData = [currRating, maxRating - currRating];

	    chartConfig = {
	    	type: 'doughnut',
	    	data: { datasets: [{
	    		data: pieData,
	    		 backgroundColor: [
                'green',
                '#F3F3F3',
                ]
	    	}], 
	    	},
	    	options: {
	    		tooltips: {
            		// Disable the on-canvas tooltip
            		enabled: false,
            	},
            	cutoutPercentage: 75
	
	    	}

	    }

  		ratingChart = new Chart(ratingCanvas, chartConfig); 

	  	$('#game-media').flickity({
			cellAlign: 'left',
			groupCells: true,
			imagesLoaded: true
		});
	}
	else if ($('body.video_games.index').length > 0)
	{
		$('.select2').select2({width: '100%'});

		$('.select2').on('select2:select select2:unselect', function (e) 
		{   
			var form = $(e.currentTarget).closest('form');
			var url = $(e.currentTarget).closest('form').attr('action');

			$.ajax({
				type: "GET",
				url: url,
				data: form.serialize(), // serializes the form's elements.
				dataType: 'html',
				headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},		           
				success: function(data)
				{

					history.pushState(null, null, this.url);
					
					$container.masonry( 'remove', $('.masonry-brick')).masonry('layout');
					$container.masonry('layout');

					// Replace Page Nav
					$pageNav = $('#page-nav').replaceWith($(data).find('#page-nav'));
					if ($container.data('infiniteScroll'))
					{
						$container.infiniteScroll('destroy')
					}					
					initializeInfiniteScroll();		
					// Destroy 
					var $bricks = $( data ).find('.masonry-brick');
					// append posts after images loaded
					
					$bricks.imagesLoaded(function(){
						// show elems now they're ready
		                $container.append($bricks);
		                $container.masonry( 'appended', $bricks, true );
		                $container.masonry('layout');                
					});  

				}
			});

		});

		$('body').on('click', '.masonry-brick a', function(e) {
			var previousUrl = $(e.currentTarget).closest('.masonry-brick').data('page');
			history.pushState(null, null,previousUrl);

		});

     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');		

	}

	// // Check if we got a thing here
	// $('.video-games-carousel').slick({
 //    	variableWidth: true,
	// 	infinite: true,
 //  		slidesToShow: 2,
 //  		autoplay: false,
 //  		variableWidth: true,
 //  		prevArrow: '<div class="previous-arrow"><span class="fa fa-angle-left"></span><span class="sr-only">Prev</span></div>',
 //  		nextArrow: '<div class="next-arrow"><span class="fa fa-angle-right"></span><span class="sr-only">Next</span></div>'
 //  	});	  	

	$('.video-games-carousel').flickity({
		// options
		cellAlign: 'left',
		wrapAround: true,
		groupCells: true,
		freeScroll: true,
		imagesLoaded: true
	});


});

var toggleDependentFields = function(el)
{
	var element = $(el);
	var type = element.val();
	var childIdsDropdown = $('#video_game_child_video_game_ids ~ span.select2');
	var parentIdsDropdown = $('#video_game_parent_video_game_ids ~ span.select2');
	var childIdsContainer = childIdsDropdown.parents('div.form-group');
	var parentIdsContainer = parentIdsDropdown.parents('div.form-group');
	if("main_game" == type)
	{
		// Hide and disable both dropdowns	
		childIdsDropdown.prop('disabled', true);
		childIdsDropdown.hide();

		parentIdsDropdown.prop('disabled', true);
		parentIdsDropdown.hide();

	}
	else if("dlc_addon" == type || "expansion" == type || "standalone_expansion" == type || "version" == type)
	{
		// Hide and disable both dropdowns	
		childIdsDropdown.prop('disabled', true);
		childIdsDropdown.hide();

		parentIdsDropdown.prop('disabled', false);
		parentIdsDropdown.show();
	}
	else if("bundle" == type)
	{
		// Hide and disable both dropdowns	
		childIdsDropdown.prop('disabled', false);
		childIdsDropdown.show();

		parentIdsDropdown.prop('disabled', true);
		parentIdsDropdown.hide();
	}
	else
	{
		console.log("Sorry something has gone wrong.");
	}

}