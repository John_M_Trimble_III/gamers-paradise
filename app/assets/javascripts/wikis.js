// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

$(function(){
	if($('body.wikis.new, body.wikis.edit').length > 0)
	{
		document.body.addEventListener('ajax:error', function(event) {
			window.location.href = event.detail[0].location;
		}); 		
		
		document.body.addEventListener('ajax:success', function(event) {
			window.location.href = event.detail[0].location;
		}); 		
	}
});