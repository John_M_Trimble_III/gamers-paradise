// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
$(function() {
	if($('body.questions.show').length > 0)
	{
		$('#answer_answer').keypress(function(e){
			if(e.which == 13){
				e.preventDefault();
				$(this).closest('form').submit();
			}
		});
	}	

})
