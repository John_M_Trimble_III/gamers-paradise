Dropzone.autoDiscover = false

var initializePostDropzone = function(parentElement) {

    Dropzone.prototype.defaultOptions['headers'] = {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}

    var previewNode = parentElement.find(".template");
    previewNode.id = "";
    var previewTemplate = previewNode.parent().html();
    previewNode.parent().html(null);     


    parentElement.find(".dZUpload").dropzone({
      url: "/attachments",
      thumbnailWidth: 100,
      thumbnailHeight: 100,
      clickable: ".add-files",
      previewsContainer: parentElement.find('.dropzone-previews')[0],
      previewTemplate: previewTemplate,
      success: function(file,response) {
        $(file.previewElement).find('.progress-bar').css("background-color", "green");
        $(file.previewElement).find('.progress').css("background-color", "green").fadeOut('slow');
        // Input hidden field to add attachments to post
        $('<input>').attr({
          type: 'hidden',
          name: 'post[attachments][]',
          value: response.message.id
        }).appendTo(file.previewElement);
      },
      removedfile: function (file) {
        // Remove From Server First Then Remove From List
        myDropzone = this;
        attachment_id = $(file.previewElement).find('input').val()
        $.ajax({
          url: myDropzone.options.url + '/' + attachment_id,
          type: 'DELETE',
          headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
          success: function() {
            file.previewElement.remove();
          }
        });
      }
    });

    var attachments = parentElement.find('.attachments').data('attachments');
    
    if(attachments)
    {
        attachments.forEach(function(element) {
        // Create the mock file:

        var myDropzone = Dropzone.forElement(parentElement.find('.dZUpload')[0]); // If you instantiated the dropzone with jQuery for example.

        var mockFile = { name: element.file.url.split('/').pop()};

        // Call the default addedfile event handler
        myDropzone.emit("addedfile", mockFile);

        // And optionally show the thumbnail of the file:
        myDropzone.emit("thumbnail", mockFile, element.file.url);

        myDropzone.emit("complete", mockFile);
        // Make sure that there is no progress bar, etc...
        myDropzone.emit("success", mockFile, {message: {id: element.id }});
      });
    }
}

$(function() {
  if($('.post-new-form').length > 0)
  {
    initializePostDropzone($('#new_post'));
  }
});



