isGifWindowActive = false;
ajaxRequest = null;

$(function() {


  // On gif window open
  $('body').on('click', '.gif-control .insert-gif', function() {
    insertGif = this;
    if ($(insertGif).siblings('.gif-selection-window').find('.gif-scroll img').length)
    {
      $(insertGif).siblings('.gif-selection-window').css('display', 'flex');
      isGifWindowActive = true;    
    }
    else
    {
      $.ajax({
        url: '/gifs',
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        success: function(response) {
          // Append to the gif list and show
          // get appropriate one
          gifSelectionWindow = $(insertGif).siblings('.gif-selection-window');
          gifScroll = gifSelectionWindow.find('.gif-scroll');
          response.gifs.forEach(function(gif) {
            var img = $('<img />', { 
              src: gif.hash.images.original.url,
              class: "img-fluid mb-1"
            });

            img.appendTo(gifScroll);
          });
          gifSelectionWindow.css('display', 'flex');
          isGifWindowActive = true;    
        }
      });      
    }
  });

  // On Keyup 
  var ajaxRequest = null;

  run_autocomplete = function(el) {
    ajaxRequest = $.ajax({
      url: '/gifs',
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      data: {q: el.currentTarget.value},    
      success: function(response) {
        // Append to the gif list and show
        var gifWindow = $('.gif-control .gif-selection-window .gif-scroll');
        
        response.gifs.forEach(function(gif) {
          var img = $('<img />', { 
            src: gif.hash.images.original.url,
            class: "img-fluid mb-1"
          });
          img.appendTo(gifWindow);
        });
      },
      beforeSend : function() {          
        if (ajaxRequest) { // if any previous ajaxRequest is running, abort
          ajaxRequest.abort();
        }        
        $('.gif-control .gif-selection-window .gif-scroll').find('img').remove();
        $('.gif-control .gif-selection-window .gif-scroll').find('.spinner').show();
      },
      complete: function() {
        $('.gif-control .gif-selection-window .gif-scroll').find('.spinner').hide();
      }     

    });
  }

  $('body').on('keyup', '.gif-search input', run_autocomplete.bind(this));


  // Now we need to insert a clicked gif into the text field. 
  $('.gif-scroll ').on('click', 'img', function() {
    el = this;
    tinymce.activeEditor.execCommand('mceInsertContent', false,  el.outerHTML);
    $('.gif-selection-window').css('display', 'none');

  });  

  $(document).click(function(e) {
    if (isGifWindowActive && !$(e.target).is('.gif-control .insert-gif') && !$(e.target).closest('.gif-selection-window').length) {
      $('.gif-selection-window').css('display', 'none');
      isGifWindowActive = false;
    }
  });

});