// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

$(function() {
	$('#shelved_game_video_game_id, #video_game_child_video_game_ids, #video_game_parent_video_game_ids, #list_video_game_video_game_id, #topic_topicable_id:visible, #wiki_video_game_ids').select2({
			ajax: {
  			url: '/select2_search',
        dataType: 'json',
  			data: function (params) {
    				var query = {
      				search: params.term
    				}
    				return query;
  			},          
			},
      templateResult: function(repo) {
        if(!repo.disabled)
        {
          var markup = 
            '<div class="media">' + 
              '<img class="mr-3" src="' + repo.box_art + ' " alt="Generic placeholder image">' + 
              '<div class="media-body">' + 
                '<h5 class="mt-0">' + repo.name + '</h5>' +
                  parseFloat(repo.avg_rating) + ' / 100.00 '+
              '</div>' +
            '</div>';

          if (repo.description) {
            markup += '<div>' + repo.description + '</div>';
          }

          markup += '</div></div>';

          return markup;
        }
      }, 
      templateSelection: function(repo) {
        return repo.name;
      }, 
      dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
      escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results         
	});
});


