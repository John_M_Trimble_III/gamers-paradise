$(function() {
	$("#notifications-navbar .dropdown-toggle").click(function(e) {
		$(e.currentTarget).removeClass('animated');
	});
	if ($('#notifications-page-nav').length)
	{
		var $container = $('#notification-flyout .notifications-container');
		$container.infiniteScroll(
		{
		    itemSelector : '.navbar-notification',     // selector for all items you'll retrieve
		    path: '#notifications-page-nav a',
		    hideNav: '#notifications-page-nav',
		    status: "#notification-flyout .page-load-status",
		    append: '.navbar-notification',
			history: false,
			elementScroll: '#notification-flyout'			

		}); 
	}   	
});