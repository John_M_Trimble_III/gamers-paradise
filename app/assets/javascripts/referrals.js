// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/
$(function() {
	if($('body.referrals').length > 0)
	{
		$('#copy-referral-code').click(function() {
			/* Get the text field */
			var copyText = document.getElementById("referral_link");

			/* Select the text field */
			copyText.select();

			/* Copy the text inside the text field */
			document.execCommand("copy");

			/* Alert the copied text */
			alert("Copied the referral code: " + copyText.value);
		})
	}

});

