$(function(){
	if($('body.users.show').length > 0)
	{
		$('a[data-toggle=tab]').each(function () {
			var $this = $(this);

			$this.on('shown.bs.tab', function () {
				$('.masonry').masonry('layout')

			});
		});

		$('#edit-username').click(function() {
			$('#username-container').hide();
			$('#username-form-edit').show();
		})

	}
});
