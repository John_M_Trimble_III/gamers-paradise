$(function() {
	if ($("meta[name='current-user']").length > 0) {
		App.rewards = App.cable.subscriptions.create("RewardsChannel",{
			connected: function() {

			},

			disconnected: function() {

			},

			received: function(data) {
				var points_container = $('#points-floater-container');
				var notifications_container = points_container.find('.notifications-container');		
				if (data.remove_notification)
				{
					notifications_container.find('#reward-' + data.id).fadeOut('slow', function(){ $(this).remove();});
				}
				else
				{
					$(data.notification).hide().prependTo(notifications_container).fadeIn();
				}

				points_container.find('.gamification').html(data.progress);
				points_container.find('.unread-rewards-count').html(data.unread_notices_count);
				points_container.find('.gamification-toggle').addClass('animated');
			},

		});
	}
});