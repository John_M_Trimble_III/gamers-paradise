$(function() {
  if ($("meta[name='current-user']").length > 0) {
    App.notifications = App.cable.subscriptions.create("NotificationsChannel", {
      connected: function() {
        // Called when the subscription is ready for use on the server
      },

      disconnected: function() {
        // Called when the subscription has been terminated by the server
      },

      received: function(data) {
        // Called when there's incoming data on the websocket for this channel
        $('#notification-flyout div.no-notifications').replaceWith("<div class=\"notifications-container\"></div>");
        
        if( $('#notification-flyout div.notifications-container').length);
        {
          $('#notification-flyout div.notifications-container').prepend(data.html);
        } 
        
        // swap notification counter
        $('#notifications-navbar span.badge').text(data.count);
        if ($('#notifications-navbar span.badge').hasClass("badge-secondary"))
        {
          $('#notifications-navbar span.badge').addClass("badge-danger");
          $('#notifications-navbar span.badge').removeClass("badge-secondary");
        }

        $('#notifications-navbar .dropdown-toggle').addClass('animated');
      }
    });
  }
});