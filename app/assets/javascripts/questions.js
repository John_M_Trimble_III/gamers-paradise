// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
$(function() {
	if($('body.video_games.show').length > 0)
	{
		enterListener();
	}
	else if($('body.questions.index').length > 0)
	{
		enterListener();

	}
	else if($('body.questions.show').length > 0)
	{
		enterListener();
	}	

})

function enterListener() {
	$('#question_question').keypress(function(e){
		if(e.which == 13){
			e.preventDefault();
			$(this).closest('form').submit();
		}
	});
}