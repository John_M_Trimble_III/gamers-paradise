// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery3
//= require tinymce-jquery
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require cocoon
//= require Chart.min
//= require slick.min
//= require dropzone
//= require bootstrap-table
//= require masonry/masonry.min
//= require masonry/jquery.imagesloaded.min
//= require infinite-scroll.pkgd.min
//= require flickity.pkgd.min
//= require_tree .

$(function() {

	$(".alert").fadeTo(2000, 500).slideUp(500, function(){
    	$(".alert").slideUp(500);
	});

	$(document).on('click', '#points-floater-container .notification', function(el) {
		var rewardId = $(el.currentTarget).data("id");
		var notification = $(el.currentTarget);
		$.ajax({
			url: "/mark_as_seen/" + rewardId, 
			type: "POST",
			data: { authenticity_token: $('[name="csrf-token"]')[0].content},
			success: function(result){
        		notification.fadeOut();
    		}
		});
	});

	$('#points-floater-container').find('.gamification-toggle').click(function(el) {
		//Hide toggle and display the points bar
		$(el.currentTarget).removeClass('animated');
		$(el.currentTarget).slideUp('slow', function() {
			$("#sidebar-content").css({'right':'0'});
		});
	});
	// Tracks Click to progress Bar
	$('#points-floater-container').find('.gamification, .close-sidebar').click(function(el) {
		//Hide toggle and display the points bar
		$("#sidebar-content").css({'right':'-1000px'});
		$('#points-floater-container').find('.gamification-toggle').slideDown();		
	});	



});