$(function(){
    $container = $('#masonry-container');

    initializePinterestScroll();  
});

    var $container = $('#masonry-container');
    var $grid = null;
    var msnry = null;  


    var initializePinterestScroll = function()
    {   
        $container.imagesLoaded(function(){
            $container.masonry({
                itemSelector: '.masonry-brick',
                gutter: '.gutter-sizer',
            });
        });   

        $container.on( 'load.infiniteScroll', function( event, response ) {
            // get posts from response
            var $bricks = $( response ).find('.masonry-brick');
            // append posts after images loaded
            $bricks.imagesLoaded(function(){
                // show elems now they're ready
                $container.append($bricks);
                $bricks.animate({ opacity: 1 });
                $container.masonry( 'appended', $bricks, true );
            });  
        });        

        initializeInfiniteScroll();
    }

    var initializeInfiniteScroll = function ()
    {
         if ($('#page-nav a').length)
        {
            $container.infiniteScroll(
            {
                itemSelector : '.masonry-brick',     // selector for all items you'll retrieve
                path: '#page-nav a',
                hideNav: '#page-nav',
                status: "#video-games-load-status",
            });        
        }
    }
