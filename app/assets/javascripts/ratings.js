// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

var initializeRating = function() {
    $('select[name="rating[score]"]').each(function(index,el){
      $(el).barrating(
      {
        theme: 'fontawesome-stars-o',
        allowEmpty: true,
        onSelect: function(value, text, event) 
        {
          if (typeof(event) !== 'undefined' && value && text) 
          {
            // rating was selected by a user
            if(confirm("Are you sure you want to give " + $(event.currentTarget).parents('form').data('rateable-name') +  ": " + value + " stars?"))
            {
              // Send Changes to the db
              var form = $(event.currentTarget).parents('form')[0];
              Rails.fire(form, 'submit');
            }
          } else {
          // rating was selected programmatically
          // by calling `set` method
          }
        },
      initialRating: $(el).data('starting-value') || 0
      });

    });
}

$(function() {
  if($('body.video_games.show, body.lists.show, body.shelves, body.member_reviews').length > 0)
  {
    initializeRating();

    $('#member_review_rating').barrating(
    {
      theme: 'fontawesome-stars-o',
      allowEmpty: true,
      initialRating: 0
  });     
  }
  else if($('body.shelves.index').length > 0 || $('body.shelves.show').length > 0) 
  {
    $('select.my-rating').each(function( index, el ) {
      $(el).barrating(
      {
        theme: 'fontawesome-stars-o',
        allowEmpty: true,
        initialRating: $(el).data('starting-value') || 0,
        readonly: true

      }); 
    });

  } 

});