// //# Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/
$(function() {
	$('body').on('keypress', '#new_comment textarea', function(e) {
		if(e.which == 13){
			e.preventDefault();
			$(this).closest('form').submit();
		}
	});

	//  the following simple make the textbox "Auto-Expand" as it is typed in
	$('body').on('input', '#new_comment textarea', function (e){
		autoExpandTextArea(e);
	});

	$('body').on('keypress', '.edit_comment textarea', function(e) {
		if(e.which == 13){
			e.preventDefault();
			$(this).closest('form').submit();
		}
	});

	//  the following simple make the textbox "Auto-Expand" as it is typed in
	$('body').on('input', '.edit_comment textarea', function (e){
		autoExpandTextArea(e);
	});	
});

var autoExpandTextArea = function(el)
{
	var textArea = el.currentTarget;
	$(textArea).css('height','auto');
	$(textArea).height(textArea.scrollHeight);
}