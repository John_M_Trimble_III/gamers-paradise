
  $(function() {
  		if($('body.video_games.show').length > 0)
  		{
  			$.ajax({ 
  				url: '/twitch/videos', 
  				data: { video_game_id: $('#video-game-id').data('video-game-id')}, 
  				dataType: 'json',

  				success: function (response) { 
  					$.each(response.data, function(index, element) {
		              // Inject thumbnail so we can load on click less demand on browser
		              var thumbnailUrl = element.thumbnail_url
		              thumbnailUrl = thumbnailUrl.replace("%{width}", "300");
		              thumbnailUrl = thumbnailUrl.replace("%{height}", "300");
		              var streamUrl = 'http://player.twitch.tv/?video=' + element.id + '&autoplay=false&muted=true';

		              $('div.row.twitch-feed').append(
		               '<div class="item' + (index == 0 ? " active" : "")  + ' twitch-stream-placeholder"><img src="' + thumbnailUrl+ '" data-iframe-url="' + streamUrl + '"></div>');
            });
  				}
  			});
  		}

     $(document).on('click','div.twitch-stream-placeholder img',function()
     {
      var url = $(this).data('iframe-url');
      $(this).replaceWith(
       '<iframe src="' + url + '" height="725"width="1170" frameborder="0" scrolling="no" allowfullscreen="true" autoplay="false"></iframe>');

    });


   });