$(function() {
	if($('body.feed.index, body.pages.show, body.groups.show').length > 0)
	{
		if ($('#page-nav a').length)
        {
        	var $container = $('#posts-container');

            $container.infiniteScroll(
            {
                itemSelector : '.post',     // selector for all items you'll retrieve
                path: '#page-nav a',
                hideNav: '#page-nav',
                status: "#feed-load-status",
  				append: '.post',
            });        
        }
	}
});