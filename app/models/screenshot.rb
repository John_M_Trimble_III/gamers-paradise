class Screenshot < ApplicationRecord
	belongs_to :screenshotable, polymorphic: true
	mount_uploader :screenshot, ImageUploader
end