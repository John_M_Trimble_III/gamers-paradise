class Order < ApplicationRecord
	include Rewardable
	def get_user
		user
	end	
	def get_points
		total_value.floor
	end	

	belongs_to :user
end
