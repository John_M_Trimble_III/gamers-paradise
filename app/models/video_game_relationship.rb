class VideoGameRelationship < ApplicationRecord
	belongs_to :parent, :class_name => 'VideoGame', :foreign_key => 'parent_id', optional: true 
  	belongs_to :child, :class_name => 'VideoGame', :foreign_key => 'child_id', optional: true
end
