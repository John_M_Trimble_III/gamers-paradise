class GameMode < ApplicationRecord
  	include ModelDraftable	
	has_many :game_modes_video_games, dependent: :destroy
	has_many :video_games, through: :game_modes_video_games
end
