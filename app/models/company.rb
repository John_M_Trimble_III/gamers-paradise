class Company < ApplicationRecord
	include ModelDraftable
	is_impressionable
	
	has_many :companies_video_games, dependent: :destroy
	has_many :video_games, through: :companies_video_games


	has_many :drafts, as: :original
	has_many :parent_drafts, as: :modified, class_name: "Draft"
	
	has_many :modified_companies, through: :drafts, source: :modified, source_type: "Company"
	has_many :originals, through: :drafts, source: :original, source_type: "Company"

end
