class ShelvedGame < ApplicationRecord

	validates :video_game_id, uniqueness: {scope: :shelf_id, message: "Only Allowed On The Same Shelf Once"}
	belongs_to :shelf
	belongs_to :video_game

	# Rewards
	include Rewardable
	def get_points
		50
	end
	def get_user
		shelf.user
	end
	# End of Rewards



	def after_save
	  self.update_counter_cache
	end

	def after_destroy
	  self.update_counter_cache
	end

	def update_counter_cache
	  self.shelf.user.video_games_count = shelf.user.video_games.uniq.count
	  self.shelf.video_games_count = shelf.video_games.uniq.count
	  self.shelf.user.save
	  self.shelf.save
	end

end
