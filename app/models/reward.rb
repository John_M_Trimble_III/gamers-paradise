class Reward < ApplicationRecord
	belongs_to :user
	belongs_to :rewardable, polymorphic: true
	after_commit :update_points, on: [:create, :destroy]

private
	def update_points
	    if transaction_include_any_action?([:create])
	    	remove_notification = false
			user.increment!(:points, points)
	    elsif user && transaction_include_any_action?([:destroy])
	    	remove_notification = true
			user.decrement!(:points, points) unless user.destroyed?
	    end	
		
		SyncPointsToUserWorker.perform_async(user.id)
		
		notification = ApplicationController.render(   partial: 'rewards/notification',   locals: { reward: self } )
		progress = ApplicationController.render(   partial: 'rewards/progress', locals: {current_user: user} )
      	RewardsChannel.broadcast_to(user, { id: self.id, notification: notification, progress: progress, remove_notification: remove_notification, unread_notices_count: user.user_rewards.where(seen_at: nil).count })


	end  	
end
