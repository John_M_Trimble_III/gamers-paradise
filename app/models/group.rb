class Group < ApplicationRecord
	include Notifiable
	
	# Rewards
	include Rewardable
  	def get_points
  		75
  	end
  	def get_user
  		if "User" == owner.class.name
  			owner
  		else
  			nil
  		end
  	end
	# End of Rewards
  	extend FriendlyId
  	friendly_id :name,  use: [:slugged, :finders] 


  	attribute :delete_banner, :boolean
  	attribute :delete_avatar, :boolean	
	
	is_impressionable
  	mount_uploader :avatar, AvatarUploader
  	mount_uploader :banner, BannerUploader

  	has_many :memberships, dependent: :destroy
  	has_many :members, through: :memberships, source: :user
	
	has_many :approved_memberships, -> { where(status: Membership.statuses[:approved]) }, class_name: "Membership", dependent: :destroy
	has_many :approved_members, through: :approved_memberships  , source: :user		

	has_many :pending_memberships, -> { where(status: Membership.statuses[:pending_approval]) }, class_name: "Membership", dependent: :destroy
	has_many :pending_members, through: :pending_memberships  , source: :user		

	has_many :rejected_memberships, -> { where(status: Membership.statuses[:rejected]) }, class_name: "Membership", dependent: :destroy
	has_many :rejected_members, through: :rejected_memberships  , source: :user		

	has_many :admin_memberships, -> { where(role: Membership.roles[:admin]) }, class_name: "Membership", dependent: :destroy
	has_many :admins, through: :admin_memberships, source: :user		

	belongs_to :owner, polymorphic: true
	has_many :posts, as: :wall, dependent: :destroy

	has_many :follows, as: :followable, class_name: "Follow", dependent: :destroy
	has_many :followers, through: :follows, as: :follower, source_type: "User"


	def get_affected_users
		users = []

		users << approved_members.pluck(:id)
		users << owner_id

		# Need to get followers as well
		users << User.includes(:follows).where('follows.followable_id' => self.id).where('follows.followable_type' => self.class.name).pluck(:id)

		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    current_user = User.current

	    if transaction_include_any_action?([:create])
			message = "#{current_user.username} has created #{name}."
	    elsif transaction_include_any_action?([:update])
			message = "#{current_user.username} has modified #{name}."
	    end

		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)	    
	end	
end