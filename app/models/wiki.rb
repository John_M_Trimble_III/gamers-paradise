class Wiki < ApplicationRecord
  extend FriendlyId
  friendly_id :name,  use: [:slugged, :finders] 
  has_many :video_game_wikis, dependent: :destroy
  has_many :video_games, through: :video_game_wikis	
  validates :name, uniqueness: true
  mount_uploader :wiki_logo, WikiLogoUploader
  belongs_to :user, optional: true
end
