class Topic < ApplicationRecord
	include Notifiable
	# Rewards
	include Rewardable
	def get_points
		45
	end
	# End of Rewards

	is_impressionable	
	belongs_to :user
	belongs_to :topicable, polymorphic: true
	has_many :topic_comments

	def get_affected_users
		users = []

		# Users that follow the topicable
		users << User.includes(:follows).where('follows.followable_id' => topicable_id).where('follows.followable_type' => topicable_type).pluck(:id)

		# Users that have made a topic comment
		users << topic_comments.pluck(:user_id)

		# Users that Have commented on a topic comment
		users << topic_comments.collect{|topic_comment| topic_comment.comments}.flatten.pluck(:user_id)

		# User that have liked a topic comment comment
		users << topic_comments.collect{|topic_comment| topic_comment.comments}.flatten.collect{|comment| comment.likes}.flatten.pluck(:user_id)

		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    current_user = User.current
	    if transaction_include_any_action?([:create])
			message = "#{current_user.username} has created a new topic about #{topicable.name}."
	    elsif transaction_include_any_action?([:update])
			message = "#{current_user.username} has modied a new topic about #{topicable.name}."
	    end
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)	    
	end		
end
