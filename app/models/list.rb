class List < ApplicationRecord
	include Notifiable	
	include Likeable
	# Rewards
	include Rewardable
  	def get_points
  		75
  	end
	# End of Rewards
  	extend FriendlyId
  	friendly_id :name,  use: [:slugged, :finders] 

	belongs_to :user
	has_many :list_video_games, dependent: :destroy
	has_many :video_games, through: :list_video_games

	def get_affected_users
		users = []

		# Users that liked list
		users << likes.pluck(:user_id)

		# Users that contributed to list
		users << list_video_games.pluck(:user_id)

		# Users that voted on contributions to the list
		users << list_video_games.collect{|list_video_game| list_video_game.likes}.flatten.pluck(:user_id)

		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?
	def create_notifications
		if transaction_include_any_action?([:create])
			message = "#{user.username} has created a new list called #{name}."
		elsif transaction_include_any_action?([:update])
			message = "#{user.username} has edited the  #{name} list."
		end

		current_user = User.current
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)
	end	
end
