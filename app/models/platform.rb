class Platform < ApplicationRecord
  include ModelDraftable
	has_many :release_dates, dependent: :destroy
	has_many :video_games, through: :release_dates, source: :releaseable, source_type: 'VideoGame'
	has_many :drafts, as: :original
end
