class BlogPost < ApplicationRecord
	include Likeable	
  	extend FriendlyId
  	friendly_id :title,  use: [:slugged, :finders] 

	validates :title, :content, :blog_category_id, :featured_image, presence: true	
	mount_uploader :featured_image, FeaturedImageUploader	
	belongs_to :blog_category
	belongs_to :author, class_name: "User", optional: true

	has_many :comments, as: :commentable, dependent: :destroy

end
