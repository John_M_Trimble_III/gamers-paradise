class SimilarVideoGame < ApplicationRecord
	belongs_to :video_game
	belongs_to :recommendation, class_name: "VideoGame", foreign_key: "recommendation_id"
end
