class Comment < ApplicationRecord
	include Notifiable	
	include Likeable
	# Rewards
	include Rewardable
	def get_points
		45
	end
	# End of Rewards
	has_ancestry
	belongs_to :commentable, polymorphic: true
	has_many :comments, as: :commentable
	belongs_to :user

	def get_affected_users
		users = []
		# commentable is either Post, TopicComment or Answer right now
		owner = commentable.try(:user) || commentable.try(:owner)
		
		# commentable owner
		users << owner.id if owner

		# other comment owners
		users << commentable.comments.pluck(:user_id)

		# Users that have liked other comments
		users << commentable.comments.collect{|comment| comment.likes }.flatten.pluck(:user_id)

		# Potentially likes of commentable
		 if commentable.respond_to?(:likes)
		 	users << commentable.likes.pluck(:user_id)
		 end

		# If commentable is a post and wall is a user's wall get the user
		if commentable.class.name == "Post" && commentable.wall.class.name == "User"
			users << commentable.wall.id
		end

		users.flatten.uniq
	end

	def create_notifications
		current_user = User.current		
	    
	    if transaction_include_any_action?([:create])
			message = "#{user.username} has submitted a new comment."
	    elsif transaction_include_any_action?([:update])
			message = "#{user.username} has edited their comment."
	    end
	    
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)
	end	
end
