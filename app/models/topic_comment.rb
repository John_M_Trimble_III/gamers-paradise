class TopicComment < ApplicationRecord
	include Notifiable
	# Rewards
	include Rewardable
	def get_points
		45
	end
	# End of Rewards

	belongs_to :topic
	belongs_to :user
	has_many :comments, as: :commentable

	def get_affected_users
		users = []

		users << topic.user.id

		# Users that follow the topicable
		users << User.includes(:follows).where('follows.followable_id' => topic.topicable_id).where('follows.followable_type' => topic.topicable_type).pluck(:id)

		# Users that have made a topic comment
		users << topic.topic_comments.pluck(:user_id)

		# Users that Have commented on a topic comment
		users << topic.topic_comments.collect{|topic_comment| topic_comment.comments}.flatten.pluck(:user_id)

		# User that have liked a topic comment comment
		users << topic.topic_comments.collect{|topic_comment| topic_comment.comments}.flatten.collect{|comment| comment.likes}.flatten.pluck(:user_id)

		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    current_user = User.current

	    if transaction_include_any_action?([:create])
			message = "#{current_user.username} added a Topic Comment about #{topic.topicable.name}"
	    elsif transaction_include_any_action?([:update])
			message = "#{current_user.username} modified their Topic Comment about #{topic.topicable.name}"
	    end
	    
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)	    
	end	
end
