class Answer < ApplicationRecord
	include Likeable
	include Notifiable
	# Rewards
	include Rewardable
  	def get_points
  		45
  	end
	# End of Rewards

	belongs_to :question
	belongs_to :user
	has_many :comments, as: :commentable
	is_impressionable

	def get_affected_users
		users = []

		# Question User		
		users << question.user_id
		
		# Other Answer User		
		users << question.answers.pluck(:user_id)
		
		# Question Like User
		users << question.likes.pluck(:user_id)

		# Answer Like User 
		users << question.answers.collect{|answer| answer.likes}.flatten.pluck(:user_id)
		
		# Answer Comment User	
		users << question.answers.collect{|answer| answer.comments}.flatten.pluck(:user_id)
		
		# Answer Comments Likes
		users << question.answers.collect{|answer| answer.comments}.flatten.collect{|comment| comment.likes }.flatten.pluck(:user_id)

		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    current_user = User.current
	    if transaction_include_any_action?([:create])
			message = "#{current_user.username} has submitted a new answer."
	    elsif transaction_include_any_action?([:update])
			message = "#{current_user.username} has edited their answer."
	    end
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)
	end
end
