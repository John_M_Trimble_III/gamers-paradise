class Notification < ApplicationRecord
	belongs_to :actor, class_name: "User", optional: true
	belongs_to :recipient, class_name: "User"
	belongs_to :notifiable, polymorphic: true, optional: true
end
