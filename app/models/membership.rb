class Membership < ApplicationRecord
	include Notifiable
	
	validates_uniqueness_of :user_id, scope: [:group_id]

	belongs_to :user
	belongs_to :group

	enum role: {admin: 1, member: 2}
	enum status: {pending_approval: 1, approved: 2, rejected: 3}


	def get_affected_users
		users = []
		# Def the user
		users << user.id

		# All other group members? 
		users << group.approved_members.pluck(:id)

		users << group.owner_id
		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    if transaction_include_any_action?([:create])
	    	if group.require_approval?
				message = "#{user.username} has requested access to #{group.name}."
			else
				message = "#{user.username} has joined #{group.name}."
			end

	    elsif transaction_include_any_action?([:update])
			message = "#{user.username} membership to #{group.name} has been set to #{self.status} and #{self.role}."
	    end
	    current_user = User.current
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)	
	end		
end
