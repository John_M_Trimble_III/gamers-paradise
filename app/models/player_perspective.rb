class PlayerPerspective < ApplicationRecord
	include ModelDraftable
	has_many :player_perspectives_video_games, dependent: :destroy
	has_many :video_games, through: :player_perspectives_video_games
end
