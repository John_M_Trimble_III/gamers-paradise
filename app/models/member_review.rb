class MemberReview < ApplicationRecord
	include Notifiable
	
	validates :rating, presence: true
	validates :body, presence: true
	validates :user_id, uniqueness: {scope: :video_game_id}

	belongs_to :user
	belongs_to :video_game, counter_cache: true
	
	after_commit :update_rateable

	def get_affected_users
		users = []
		users << User.includes(:follows).where('follows.followable_id' => video_game.id).where('follows.followable_type' => video_game.class.name).pluck(:id)
		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    if transaction_include_any_action?([:create])
			message = "#{user.username} has added a member review for #{video_game.name}."
	    elsif transaction_include_any_action?([:update])
			message = "#{user.username} has updated a member review for #{video_game.name}."
	    end
	    current_user = User.current
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)
	end		


	def update_rateable
		video_game.update_column(:average_rating, video_game.calculate_average_rating)
	end

end
