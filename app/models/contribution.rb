class Contribution < ApplicationRecord
	# Rewards
	include Rewardable
  	def get_points
  		if field == "new_#{contributable.class.name}".parameterize.underscore
			 150		
  		else
			 65 		
  		end
  	end
	# End of Rewards

	belongs_to :draft, optional: true
	belongs_to :user
	belongs_to :contributable, polymorphic: true
  	enum action: { add: 0, remove: 1, modify: 2}	

  	scope :type_breakdown, -> { select("contributable_type").group(:contributable_type) }

  	def self.get_things_contributed_to
  		all.map{|contribution| contribution.contributable }.compact.uniq
  	end

end
