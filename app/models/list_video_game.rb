class ListVideoGame < ApplicationRecord
	include Notifiable
	# Rewards
	include Rewardable
	def get_points
		45
	end
	# End of Rewards

	validates :video_game_id, uniqueness: { scope: [:list_id], message: "Already On List" }	
	
	has_many :likes, as: :likeable, dependent: :destroy

	belongs_to :video_game
	belongs_to :list
	belongs_to :user


	def get_affected_users
		users = []

		users << list.user.id

		# Users that liked list
		users << list.likes.pluck(:user_id)

		# Users that contributed to list
		users << list.list_video_games.pluck(:user_id)

		# Users that voted on contributions to the list
		users << list.list_video_games.collect{|list_video_game| list_video_game.likes}.flatten.pluck(:user_id)

		users.flatten.uniq
	end

	def create_notifications
	    current_user = User.current

	    if transaction_include_any_action?([:create])
			message = "#{user.username} has added #{video_game.name} to #{list.name}."
	    elsif transaction_include_any_action?([:update])
			message = "#{user.username} has edited #{video_game.name} on #{list.name}."
	    end

		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)
	end		
end
