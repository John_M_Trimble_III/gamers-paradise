class Friendship < ApplicationRecord
  include Notifiable
  # Rewards
  include Rewardable
  def get_points
    35
  end
  def get_user
    [user, friend]
  end
  # End of Rewards

  belongs_to :user
  belongs_to :friend, :class_name => "User"

  def get_affected_users
    users = []  

    users << friend.id

    users << user.id

    users.flatten.uniq
  end

	def create_notifications
    current_user = User.current
    if transaction_include_any_action?([:create])
      message = "#{user.username} has sent you a friend request."         
    
    elsif transaction_include_any_action?([:update])
      message = "You and #{friend.username} are now friends."
    end

    GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)
  end  	


  def after_save
    self.update_counter_cache
  end

  def after_destroy
    self.update_counter_cache
  end

  def update_counter_cache
    self.user.friends_count = self.user.friends.count
    self.friend.friends_count = self.friend.friends.count
    self.user.save
    self.friend.save
  end  
end
