class Listing < ApplicationRecord
	belongs_to :user
	belongs_to :video_game
	belongs_to :platform
end
