class CriticReview < ApplicationRecord
	include Notifiable
	
	belongs_to :video_game
	belongs_to :company, optional: true

	def get_affected_users
		User.includes(:follows).where('follows.followable_id' => video_game.id).where('follows.followable_type' => video_game.class.name)		
	end

	def create_notifications(action)
		# Get All Recipients
	    current_user = User.current
	    message = "Critic review has been edited or created"
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)
	end	
end
