class Genre < ApplicationRecord
	include ModelDraftable
	has_many :genres_video_games, dependent: :destroy
	has_many :video_games, through: :genres_video_games
end
