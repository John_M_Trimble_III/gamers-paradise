class Page < ApplicationRecord
	include Notifiable
	
	# Rewards
	include Rewardable
  	def get_points
  		75
  	end
  	def get_user
  		if "User" == owner.class.name
  			owner
  		else
  			nil
  		end
  	end
	# End of Rewards
  	extend FriendlyId
  	friendly_id :name,  use: [:slugged, :finders] 

  	attribute :delete_banner, :boolean
  	attribute :delete_avatar, :boolean	

	is_impressionable	
	belongs_to :owner, polymorphic: true
	has_many :posts, as: :wall
	belongs_to :company, optional: true
  	mount_uploader :banner, BannerUploader
  	mount_uploader :avatar, AvatarUploader
	has_many :follows, as: :followable, class_name: "Follow"
	has_many :followers, through: :follows, as: :follower, source_type: "User"

	def get_affected_users
		users = []

		users << followers.pluck(:id)

		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    if transaction_include_any_action?([:create])
			message = "#{name} has been created."
	    elsif transaction_include_any_action?([:update])
			message = "#{name} has been modified."
	    end

	    current_user = User.current
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)
	end	
end
