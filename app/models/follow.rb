class Follow < ApplicationRecord
	include Notifiable
	# Rewards
	include Rewardable
	def get_points
		35
	end
	def get_user
		follower
	end
	# End of Rewards	
	belongs_to :followable, polymorphic: true
	belongs_to :follower, polymorphic: true

	after_create :resync_user_newsfeed
	after_destroy :resync_user_newsfeed

	def resync_user_newsfeed
		AdjustNewsFeedAfterFollowWorker.perform_async(follower_id, follower_type)
	end

	def get_affected_users
		# For pages and groups let's notify the admin
		# For users let's notify the user
		users = []

		if 'User' == followable.class.name
			# Alert the user they are being followed
			users << followable.id
		elsif 'Page' == followable.class.name
			users << followable.owner_id
		elsif 'Group' == followable.class.name
			users << followable.admins.pluck(:id)
			users << followable.owner_id
		end
		users.flatten.uniq
	end

	def create_notifications
		followable_name = followable.try(:username) || followable.try(:name)
	    current_user = User.current

	    if transaction_include_any_action?([:create])

			if 'User' == followable.class.name 
				message = "#{current_user.username} started following you."				
			else
				message = "#{current_user.username} started following #{followable_name}."
			end
	    end		

		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)	    
	end

end
