class Series < ApplicationRecord
	include ModelDraftable
	has_many :series_video_games, dependent: :destroy
	has_many :video_games, through: :series_video_games
end
