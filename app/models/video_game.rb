class VideoGame < ApplicationRecord
  searchkick callbacks: :queue
  include Filterable
  include ModelDraftable
  include Questionable
  is_impressionable
  extend FriendlyId
  friendly_id :name,  use: [:slugged, :finders] 

  validates :name, presence: true
	# Things That Don't Need Tracking
  has_many :listings, dependent: :destroy
  has_many :shelved_games, dependent: :destroy
  has_many :shelves, through: :shelved_games  
  
  has_many :ratings, as: :rateable, dependent: :destroy
  has_many :rating_users, through: :ratings, source: :user

  has_many :member_reviews, dependent: :destroy
  has_many :reviewing_members, through: :member_reviews, source: :user

  has_many :follows, as: :followable, class_name: "Follow", dependent: :destroy
  has_many :followers, through: :follows, source_type: "User"

  has_many :critic_reviews, dependent: :destroy
  has_many :wall_posts, as: :wall, class_name: "Post", dependent: :destroy
  has_many :list_video_games, dependent: :destroy
  
  mount_uploader :box_art, ImageUploader
  enum type_of: {main_game: 0, dlc_addon: 1, expansion: 2, bundle: 3, standalone_expansion: 4, version: 5}
  enum status: {released: 0, alpha: 1, beta: 2, early_access: 3, offline: 4, cancelled: 5, not_yet_released: 6}

  enum esrb_rating: {rp: 0, ec: 1, e: 2, e10: 3, t: 4, m: 5, ao: 6}
  enum esrb_theme: {
    animated_blood: 0, 
    blood: 1, 
    blood_and_gore: 2, 
    cartoon_violence: 3, 
    comic_mischief: 4, 
    crude_humor: 5, 
    drug_reference: 6, 
    fantasy_violence: 7, 
    intense_violence: 8, 
    language: 9, 
    lyrics: 10, 
    mature_humor: 11, 
    nudity: 12, 
    partial_nudity: 13, 
    real_gambling: 14, 
    sexual_content: 15, 
    sexual_themes: 16, 
    sexual_violence: 17, 
    simulated_gambling: 18, 
    strong_language: 19, 
    strong_lyrics: 20, 
    strong_sexual_content: 21, 
    strong_lyrics: 22, 
    strong_sexual_content: 23, 
    suggestive_themes: 24, 
    tobacco_reference: 25, 
    use_of_alcohol: 26, 
    use_of_drugs: 27, 
    use_of_tobacco: 28, 
    violence: 29, 
    violent_references: 30, 
    animated_violence: 31, 
    mild_language: 32, 
    mild_violence: 33, 
    use_of_drugs_and_alcohol: 34,
    drug_and_alcohol_reference: 35, 
    mild_suggestive_themes: 36, 
    mild_cartoon_violence: 37,
    mild_blood: 38, 
    realistic_blood_and_gore: 39, 
    realistic_violence: 40, 
    alcohol_and_tobacco_reference: 41,
    mature_sexual_themes: 42, 
    mild_animated_violence: 43, 
    mild_sexual_themes: 44, 
    use_of_alcohol_and_tobacco: 45, 
    animated_blood_and_gore: 46, 
    mild_fantasy_violence: 47, 
    mild_lyrics: 48, 
    realistic_blood: 49
  }, _suffix: true


  enum pegi_rating: {three: 0, seven: 1, twelve: 2, sixteen: 3, eighteen: 4}
  enum pegi_theme: {
    violence: 0, 
    sex: 1, 
    drugs: 2, 
    fear: 3, 
    discrimination: 4, 
    bad_language: 5, 
    gambling: 6, 
    online_gameplay: 7
  }, _suffix: true

  # End of Things That Don't Need Tracking

	has_many :genres_video_games, dependent: :destroy
	has_many :genres, through: :genres_video_games

	has_many :game_engines_video_games, dependent: :destroy
	has_many :game_engines, through: :game_engines_video_games

  has_many :multiplayer_modes, inverse_of: :video_game, dependent: :destroy 
  accepts_nested_attributes_for :multiplayer_modes, allow_destroy: true, reject_if:  :check_multiplayer_modes

  has_many :release_dates, as: :releaseable, inverse_of: :releaseable, dependent: :destroy
  accepts_nested_attributes_for :release_dates, reject_if: :all_blank, allow_destroy: true
  
  has_many :platforms, through: :release_dates

  has_many :videos, as: :videoable, inverse_of: :videoable, dependent: :destroy
  accepts_nested_attributes_for :videos, reject_if: :all_blank, allow_destroy: true

  has_many :screenshots, as: :screenshotable, inverse_of: :screenshotable, dependent: :destroy
  accepts_nested_attributes_for :screenshots, reject_if: :all_blank, allow_destroy: true  

	has_many :companies_video_games, inverse_of: :video_game, dependent: :destroy
  accepts_nested_attributes_for :companies_video_games,  allow_destroy: true, reject_if:  :check_companies
	
  has_many :companies, through: :companies_video_games
  
  # Grouping Companies
  has_many :publisher_company_video_games, -> { publisher }, :class_name => 'CompaniesVideoGame'
  has_many :publishers, :source => :company, :through => :publisher_company_video_games

  has_many :main_developers_companies_video_games, -> { main_developer }, :class_name => 'CompaniesVideoGame'
  has_many :main_developers, :source => :company, :through => :main_developers_companies_video_games

  has_many :supporting_developer_companies_video_games, -> { supporting_developer }, :class_name => 'CompaniesVideoGame'
  has_many :supporting_developers, :source => :company, :through => :supporting_developer_companies_video_games

  has_many :porting_developer_companies_video_games, -> { porting_developer }, :class_name => 'CompaniesVideoGame'
  has_many :porting_developers, :source => :company, :through => :porting_developer_companies_video_games
  # End Grouping Companies

  has_many :video_game_parents_relations, :foreign_key => 'child_id',  :class_name => 'VideoGameRelationship', dependent: :destroy
  has_many :video_game_child_relations, :foreign_key => 'parent_id', :class_name => 'VideoGameRelationship', dependent: :destroy

  has_many :parent_video_games, :through => :video_game_parents_relations, :source => :parent 
  has_many :child_video_games, :through => :video_game_child_relations, :source => :child 

  has_many :game_modes_video_games, dependent: :destroy
  has_many :game_modes, through: :game_modes_video_games

  has_many :player_perspectives_video_games, dependent: :destroy
  has_many :player_perspectives, through: :player_perspectives_video_games

  has_many :series_video_games, dependent: :destroy
  has_many :series, through: :series_video_games
  has_many :other_games_in_series, through: :series, source: :video_games

  has_many :franchises_video_games, dependent: :destroy
  has_many :franchises, through: :franchises_video_games
  has_many :other_games_in_franchise, through: :franchises, source: :video_games

  has_many :expansions, -> { where type_of: VideoGame.type_ofs['expansion']}, :through => :video_game_child_relations, :source => :child 
  
  has_many :bundled_games, -> { where type_of: VideoGame.type_ofs['main_game']}, :through => :video_game_child_relations, :source => :child

  has_many :topics, as: :topicable, dependent: :destroy

  has_many :similar_video_games, dependent: :destroy
  has_many :similar_games, through: :similar_video_games, source: :recommendation

  has_many :video_game_wikis, dependent: :destroy
  has_many :wikis, through: :video_game_wikis



  scope :platform_filter, -> (platform_ids) { joins(:platforms).where(platforms: { id: platform_ids }) }
  scope :franchise_filter, -> (franchise_ids) { joins(:franchises).where(franchises: { id: franchise_ids }) }
  scope :series_filter, -> (series_ids) { joins(:series).where(series: { id: series_ids }) }
  scope :type_filter, -> (type_ids) { where(type_of: type_ids) }
  scope :status_filter, -> (status_ids) { where(status: status_ids) }
  scope :genres_filter, -> (genre_ids)  { joins(:genres).where(genres: { id: genre_ids }) }
  scope :game_modes_filter, -> (game_mode_ids) { joins(:game_modes).where(game_modes: {id: game_mode_ids})}
  scope :player_perspectives_filter, -> (player_perspective_ids) { joins(:player_perspectives).where(player_perspectives: {id: player_perspective_ids})}
  

  scope :search_import, -> {   includes(:parent_drafts).where(drafts: { id: nil })}     

  def detect_changes(modified)
    changed_attributes = []

    main_changed_attributes = (self.attributes.to_a - modified.attributes.to_a).map(&:first)
    main_changed_attributes.delete("id")
    main_changed_attributes.delete("created_at")
    main_changed_attributes.delete("updated_at")
    main_changed_attributes.delete("box_art")    
    changed_attributes += main_changed_attributes

    changed_attributes << "genres" if (self.genres != modified.genres)
    changed_attributes << "game_engines" if (self.game_engines != modified.game_engines)
    changed_attributes << "parent_video_games" if (self.parent_video_games != modified.parent_video_games)
    changed_attributes << "child_video_games" if (self.child_video_games != modified.child_video_games)
    changed_attributes << "game_modes" if (self.game_modes != modified.game_modes)
    changed_attributes << "player_perspectives" if (self.player_perspectives != modified.player_perspectives)
    changed_attributes << "series" if (self.series != modified.series)
    changed_attributes << "franchises" if (self.franchises != modified.franchises)
  
    # Special Test Cases 
    changed_attributes << "companies_video_games" if (self.companies_video_games.to_a.pluck(:company_id, :type_ofs) != modified.companies_video_games.to_a.pluck(:company_id, :type_ofs))
    changed_attributes << "multiplayer_modes" if (self.multiplayer_modes.to_a.pluck(:platform_id, :online_co_op_max_players, :offline_co_op_max_players, :online_max_players, :offline_max_players, :is_online_co_op, :is_offline_co_op, :is_lan_co_op, :is_co_op_campaign, :is_online_split_screen, :is_offline_split_screen, :is_drop_in_drop_out) != modified.multiplayer_modes.to_a.pluck(:platform, :online_co_op_max_players, :offline_co_op_max_players, :online_max_players, :offline_max_players, :is_online_co_op, :is_offline_co_op, :is_lan_co_op, :is_co_op_campaign, :is_online_split_screen, :is_offline_split_screen, :is_drop_in_drop_out))
    changed_attributes << "release_dates" if (self.release_dates.to_a.pluck(:platform_id, :region, :date, :upc, :asin) != modified.release_dates.to_a.pluck(:platform_id, :region, :date, :upc, :asin) )
    changed_attributes << "videos" if (self.videos.to_a.pluck(:video) != modified.videos.to_a.pluck(:video))
    changed_attributes << "screenshots" if (self.screenshots.to_a.pluck(:screenshot) != modified.screenshots.to_a.pluck(:screenshot))
    # End of Special Test Cases
    changed_attributes << "box_art" if modified.box_art?
    return changed_attributes
  end
  # We are going down a different route now only modifying the modified fields
  def self.accept(draft)
      if  "Delete" == draft.type_of
        # We've got some deleting to do
        # Either pick the original or modified they should be the same
        return draft.original.destroy && draft.destroy
      elsif original = draft.original # We updating bitch
        contributions = []
        # We want to only copy game fields not any of the important associations, that would be too much. 
        modified = draft.modified
        
        modified_fields = draft.object

        if(modified_fields.include? 'name')
          original.name = modified.name
          contributions << Contribution.new({contributable: original, user: draft.user, field: "name", content: original.name, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'description')
          original.description = modified.description
          contributions << Contribution.new({contributable: original, user: draft.user, field: "description", content: original.description, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'story')
          original.story = modified.story 
          contributions << Contribution.new({contributable: original, user: draft.user, field: "story", content: original.story, action: :modify, points: 1})
        end

        if(modified_fields.include? 'box_art')
          original.box_art = modified.box_art 
          contributions << Contribution.new({contributable: original, user: draft.user, field: "box_art", content: nil, action: :modify, points: 1})
        end

        # Shallow Fields
        if(modified_fields.include? 'official_website')
          original.official_website = modified.official_website
          contributions << Contribution.new({contributable: original, user: draft.user, field: "official_website", content: original.official_website, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'wikia_page')
          original.wikia_page = modified.wikia_page
          contributions << Contribution.new({contributable: original, user: draft.user, field: "wikia_page", content: original.wikia_page, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'wikipedia_page')
          original.wikipedia_page == modified.wikipedia_page
          contributions << Contribution.new({contributable: original, user: draft.user, field: "wikipedia_page", content: original.wikipedia_page, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'facebook_page')
          original.facebook_page = modified.facebook_page
          contributions << Contribution.new({contributable: original, user: draft.user, field: "facebook_page", content: original.facebook_page, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'twitch_profile')
          original.twitter_profile = modified.twitter_profile
          contributions << Contribution.new({contributable: original, user: draft.user, field: "twitch_profile", content: original.twitter_profile, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'twitch_channel')
          original.twitch_channel = modified.twitch_channel  
          contributions << Contribution.new({contributable: original, user: draft.user, field: "twitch_channel", content: original.twitch_channel, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'instagram_profile')
          original.instagram_profile = modified.instagram_profile
          contributions << Contribution.new({contributable: original, user: draft.user, field: "instagram_profile", content: original.instagram_profile, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'youtube_channel')
          original.youtube_channel = modified.youtube_channel
          contributions << Contribution.new({contributable: original, user: draft.user, field: "youtube_channel", content: original.youtube_channel, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'itunes_page_iphone')
          original.itunes_page_iphone = modified.itunes_page_iphone
          contributions << Contribution.new({contributable: original, user: draft.user, field: "itunes_page_iphone", content: original.itunes_page_iphone, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'itunes_page_ipad')
          original.itunes_page_ipad = modified.itunes_page_ipad
          contributions << Contribution.new({contributable: original, user: draft.user, field: "itunes_page_ipad", content: original.itunes_page_ipad, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'google_play_store_page')
          original.google_play_store_page = modified.google_play_store_page
          contributions << Contribution.new({contributable: original, user: draft.user, field: "google_play_store_page", content: original.google_play_store_page, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'steam_store_page')
          original.steam_store_page = modified.steam_store_page
          contributions << Contribution.new({contributable: original, user: draft.user, field: "steam_store_page", content: original.steam_store_page, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'subreddit_page')
          original.subreddit_page = modified.subreddit_page
          contributions << Contribution.new({contributable: original, user: draft.user, field: "subreddit_page", content: original.subreddit_page, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'type_of')
          original.type_of = modified.type_of          
          contributions << Contribution.new({contributable: original, user: draft.user, field: "type_of", content: original.type_of, action: :modify, points: 1})
        end
        
        if(modified_fields.include? 'status')
          original.status = modified.status          
          contributions << Contribution.new({contributable: original, user: draft.user, field: "status", content: original.status, action: :modify, points: 1})
        end      

        if(modified_fields.include? 'esrb_rating')
          original.esrb_rating = modified.esrb_rating          
          contributions << Contribution.new({contributable: original, user: draft.user, field: "esrb_rating", content: original.esrb_rating, action: :modify, points: 1})
        end   

        if(modified_fields.include? 'esrb_themes')
          original.esrb_themes = modified.esrb_themes          
          contributions << Contribution.new({contributable: original, user: draft.user, field: "esrb_themes", content: original.esrb_themes, action: :modify, points: 1})
        end   

        if(modified_fields.include? 'esrb_synopsis')
          original.esrb_synopsis = modified.esrb_synopsis          
          contributions << Contribution.new({contributable: original, user: draft.user, field: "esrb_synopsis", content: original.esrb_synopsis, action: :modify, points: 1})
        end   

        if(modified_fields.include? 'pegi_rating')
          original.pegi_rating = modified.pegi_rating          
          contributions << Contribution.new({contributable: original, user: draft.user, field: "pegi_rating", content: original.pegi_rating, action: :modify, points: 1})
        end   

        if(modified_fields.include? 'pegi_themes')
          original.pegi_themes = modified.pegi_themes          
          contributions << Contribution.new({contributable: original, user: draft.user, field: "pegi_themes", content: original.pegi_themes, action: :modify, points: 1})
        end   

        if(modified_fields.include? 'pegi_synopsis')
          original.pegi_synopsis = modified.pegi_synopsis          
          contributions << Contribution.new({contributable: original, user: draft.user, field: "pegi_synopsis", content: original.pegi_synopsis, action: :modify, points: 1})
        end                                                             

        if(modified_fields.include? 'genres')
          modified.genres.each do |genre| # add
            unless original.genres.include?(genre) #  
              original.genres << genre
              contributions << Contribution.new({contributable: original, user: draft.user, field: "genre", content: genre.name, action: :add, points: 1})
            end
          end
          original.genres.each do |genre| # remove
            unless modified.genres.include?(genre) #  
              original.genres.delete(genre.id)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "genre", content: genre.name, action: :remove, points: 1})
            end
          end
        end


        if(modified_fields.include? 'game_modes')
          modified.game_modes.each do |game_mode| # add
            unless original.game_modes.include?(game_mode) #  
              original.game_modes << game_mode
              contributions << Contribution.new({contributable: original, user: draft.user, field: "game_mode", content: game_mode.name, action: :add, points: 1})
            end
          end
          original.game_modes.each do |game_mode| # remove
            unless modified.game_modes.include?(game_mode) #  
              original.game_modes.delete(game_mode.id)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "game_mode", content: game_mode.name, action: :remove, points: 1})
            end
          end          
        end


        if(modified_fields.include? 'game_engines')
          modified.game_engines.each do |game_engine| # add
            unless original.game_engines.include?(game_engine) #  
              original.game_engines << game_engine
              contributions << Contribution.new({contributable: original, user: draft.user, field: "game_engine", content: game_engine.name, action: :add, points: 1})
            end
          end
          original.game_engines.each do |game_engine| # remove
            unless modified.game_engines.include?(game_engine) #  
              original.game_engines.delete(game_engine.id)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "game_engine", content: game_engine.name, action: :remove, points: 1})
            end
          end    
        end
     
        if(modified_fields.include? 'player_perspectives')
          modified.player_perspectives.each do |player_perspective| # add
            unless original.player_perspectives.include?(player_perspective) #  
              original.player_perspectives << player_perspective
              contributions << Contribution.new({contributable: original, user: draft.user, field: "player_perspective", content: player_perspective.name, action: :add, points: 1})
            end
          end
          original.player_perspectives.each do |player_perspective| # remove
            unless modified.player_perspectives.include?(player_perspective) #  
              original.player_perspectives.delete(player_perspective.id)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "player_perspective", content: player_perspective.name, action: :remove, points: 1})
            end
          end  
        end

        if(modified_fields.include? 'series')
          modified.series.each do |serie| # add
            unless original.series.include?(serie) #  
              original.series << serie
              contributions << Contribution.new({contributable: original, user: draft.user, field: "series", content: serie.name, action: :add, points: 1})
            end
          end
          original.series.each do |serie| # remove
            unless modified.series.include?(serie) #  
              original.sereis.delete(serie.id)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "series", content: serie.name, action: :remove, points: 1})
            end
          end          
        end
        
        if(modified_fields.include? 'franchises')
          modified.franchises.each do |franchise| # add
            unless original.franchises.include?(franchise) #  
              original.franchises << franchise
              contributions << Contribution.new({contributable: original, user: draft.user, field: "franchise", content: franchise.name, action: :add, points: 1})
            end
          end
          original.franchises.each do |franchise| # remove
            unless modified.franchises.include?(franchise) #  
              original.franchises.delete(franchise.id)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "franchise", content: franchise.name, action: :remove, points: 1})
            end
          end  
        end



        # Handling of the Self-refrential This is for bundles etc.
        if(modified_fields.include? 'parent_video_games')
          modified.parent_video_games.each do |parent_video_game| # add
            unless original.parent_video_games.include?(parent_video_game) #  
              original.parent_video_games << parent_video_game
              contributions << Contribution.new({contributable: original, user: draft.user, field: "parent_video_game", content: parent_video_game.name, action: :add, points: 1})
            end
          end
          original.parent_video_games.each do |parent_video_game| # remove
            unless modified.parent_video_games.include?(parent_video_game) #  
              original.parent_video_games.delete(parent_video_game.id)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "parent_video_game", content: parent_video_game.name, action: :remove, points: 1})
            end
          end            
        end
        
        if(modified_fields.include? 'child_video_games')
          modified.child_video_games.each do |child_video_game| # add
            unless original.child_video_games.include?(child_video_game) #  
              original.child_video_games << child_video_game
              contributions << Contribution.new({contributable: original, user: draft.user, field: "child_video_game", content: child_video_game.name, action: :add, points: 1})
            end
          end
          original.child_video_games.each do |child_video_game| # remove
            unless modified.child_video_games.include?(child_video_game) #  
              original.child_video_games.delete(child_video_game.id)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "child_video_game", content: child_video_game.name, action: :remove, points: 1})
            end
          end         
        end
        # End of the Self-refrential

  
        # Coccon Stuff 
        # What we are doing is copying the modified ones to the original and then deleting the originls associated records.
        # Exclude id, created_at, updated_at, video_game_id
        # Resolve Deltas
        if(modified_fields.include? 'companies_video_games')
          modified.companies_video_games.each do |companies_video_game| # this will work for additions but not subtractions, unless we copy only the differences and iterate through both
            # If original has it we are good User find_by
            unless original.companies_video_games.find_by(company: companies_video_game.company, type_ofs: companies_video_game.type_ofs) # Did not find so need to add
              original.companies_video_games << companies_video_game.dup
              contributions << Contribution.new({contributable: original, user: draft.user, field: "companies_video_games", content: "#{companies_video_game.company.name} as #{companies_video_game.type_ofs}", action: :add, points: 1})
            end
          end
          # Inverse operation deleting items that aren't in 
          original.companies_video_games.each do |companies_video_game| # this will work for additions but not subtractions, unless we copy only the differences and iterate through both
            # If original has it we are good User find_by
            unless modified.companies_video_games.find_by(company: companies_video_game.company, type_ofs: companies_video_game.type_ofs) # Did not find so need to remove
              companies_video_game.delete
              contributions << Contribution.new({contributable: original, user: draft.user, field: "companies_video_game", content: "#{companies_video_game.company.name} as #{companies_video_game.type_ofs}", action: :remove, points: 1})
            end
          end          
        end 

        # Exclude video_game_id, updated_at, created_at, id
        if(modified_fields.include? 'multiplayer_modes')
          modified.multiplayer_modes.each do |multiplayer_mode|
            unless original.multiplayer_modes.find_by(platform: multiplayer_mode.platform, online_co_op_max_players: multiplayer_mode.online_co_op_max_players, offline_co_op_max_players: multiplayer_mode.offline_co_op_max_players, online_max_players: multiplayer_mode.online_max_players, offline_max_players: multiplayer_mode.offline_max_players, is_online_co_op: multiplayer_mode.is_online_co_op, is_offline_co_op: multiplayer_mode.is_offline_co_op, is_lan_co_op: multiplayer_mode.is_lan_co_op, is_co_op_campaign: multiplayer_mode.is_co_op_campaign, is_online_split_screen: multiplayer_mode.is_online_split_screen, is_offline_split_screen: multiplayer_mode.is_offline_split_screen, is_drop_in_drop_out: multiplayer_mode.is_drop_in_drop_out)
              original.multiplayer_modes << multiplayer_mode.dup
              contributions << Contribution.new({contributable: original, user: draft.user, field: "multiplayer_modes", content: "Added Multiplayer Mode For #{multiplayer_mode.platform.name}", action: :add, points: 1})
            end          
          end
          original.multiplayer_modes.each do |multiplayer_mode|
            unless modified.multiplayer_modes.find_by(platform: multiplayer_mode.platform, online_co_op_max_players: multiplayer_mode.online_co_op_max_players, offline_co_op_max_players: multiplayer_mode.offline_co_op_max_players, online_max_players: multiplayer_mode.online_max_players, offline_max_players: multiplayer_mode.offline_max_players, is_online_co_op: multiplayer_mode.is_online_co_op, is_offline_co_op: multiplayer_mode.is_offline_co_op, is_lan_co_op: multiplayer_mode.is_lan_co_op, is_co_op_campaign: multiplayer_mode.is_co_op_campaign, is_online_split_screen: multiplayer_mode.is_online_split_screen, is_offline_split_screen: multiplayer_mode.is_offline_split_screen, is_drop_in_drop_out: multiplayer_mode.is_drop_in_drop_out)
              multiplayer_mode.delete
              contributions << Contribution.new({contributable: original, user: draft.user, field: "multiplayer_mode", content: "Removed Multiplayer Mode For #{multiplayer_mode.platform.name}", action: :remove, points: 1})
            end          
          end          
        end 

        # Exclude releasable_id, releasable_type, updated_at, created_at, id
        if(modified_fields.include? 'release_dates')        
          modified.release_dates.each do |release_date|
            unless original.release_dates.find_by(platform: release_date.platform, region: release_date.region, date: release_date.date, upc: release_date.upc, asin: release_date.asin)
              original.release_dates << release_date.dup              
              contributions << Contribution.new({contributable: original, user: draft.user, field: "release_date", content: "Added release date of #{release_date.date} on #{release_date.platform.name}", action: :add, points: 1})
            end                  
          end
          original.release_dates.each do |release_date|
            unless modified.release_dates.find_by(platform: release_date.platform, region: release_date.region, date: release_date.date, upc: release_date.upc, asin: release_date.asin)           
              contributions << Contribution.new({contributable: original, user: draft.user, field: "release_date", content: "Removed release date of #{release_date.date} on #{release_date.platform.name}", action: :remove, points: 1})
              release_date.delete             
            end                  
          end          
        end          


        # Exclude id,videoable_id, videoable_type, created_at, updated_at
        if(modified_fields.include? 'videos')                
          modified.videos.each do |video|
            unless original.videos.find_by(video: video.video)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "Video", content: "Added video #{video.video}", action: :add, points: 1})
              original.videos << video.dup                          
            end           
          end
          original.videos.each do |video|
            unless modified.videos.find_by(video: video.video)
              contributions << Contribution.new({contributable: original, user: draft.user, field: "Video", content: "Added video #{video.video}", action: :remove, points: 1})
              video.delete                         
            end           
          end          
        end 

        # Exclude screenshotable_id, screenshotable_type, id, created_at, updated_at
        if(modified_fields.include? 'screenshots')                        
          modified.screenshots.each do |screenshot|
            unless original.screenshots.find_by(screenshot: screenshot.screenshot.file.filename)
              duplicate_screenshot = screenshot.dup

              duplicate_screenshot.remote_screenshot_url = screenshot.screenshot_url
              original.screenshots << duplicate_screenshot                    
              contributions << Contribution.new({contributable: original, user: draft.user, field: "screenshot", content: "Added #{screenshot.screenshot.file.filename}", action: :add, points: 1})
            end              
          end
          original.screenshots.each do |screenshot|
            unless modified.screenshots.find_by(screenshot: screenshot.screenshot.file.filename)
              screenshot.delete                         
              contributions << Contribution.new({contributable: original, user: draft.user, field: "screenshot", content: "Removed #{screenshot.screenshot.file.filename}", action: :remove, points: 1})
            end              
          end          
        end           
        # End of Coccon Stuff

        status = original.save
        if status 
          contributions.each do |contribution|
            contribution.save
          end 
        end
        modified.destroy # Get rid of the modified instance of original

      else # This should always be brand new video games
        # With a status of true the controller will delete the draft making this published
        Contribution.create({contributable: draft.modified, user: draft.user, field: "new_#{draft.modified.class.name}".parameterize.underscore, content: draft.modified.name, action: :add, points: 25}) # We are going to award 50 points to brand new additions        
        status = true
      end 
      draft.modified ? draft.modified.create_notifications : draft.original.create_notifications
      draft.destroy if status
      # If original available reindex model
      draft.original ? draft.original.reindex : draft.modified.reindex
    return status
  end


  def get_affected_users
    users = []
    # Users that follow me
    users << User.includes(:follows).where('follows.followable_id' => self.id).where('follows.followable_type' => self.class.name).pluck(:id)
    
    users.flatten.uniq
  end

  #  How to get affected users after record is deleted?

  def create_notifications
    message = "#{name} has been updated"
    current_user = User.current
    GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)  
  end 


  def should_index?
    published? 
  end

  def check_companies(attributes)
    active_field = false
    attributes.each do |key, value| 
      if (value.kind_of?(Array) && value.all?(&:blank?)) || value.empty? ||   "_destroy" == key
        next
      else 
        return false
      end   
    end   
    true
  end

  def check_multiplayer_modes(attributes)
    attributes.each do |key, value| 
      if value.nil? || false == value || value.empty? || "0" == value || "_destroy" == key
        next
      else
        return false
      end
    end
    true
  end

  def calculate_average_rating
    avg_user_rating = 100 * ((self.ratings.sum(:score).to_f + self.member_reviews.sum(:rating).to_f) / (10 * (self.ratings.count + self.member_reviews.count)))
    return (avg_user_rating.nan? ? 0 : avg_user_rating).round(2) 
  end

end
