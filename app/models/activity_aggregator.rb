class ActivityAggregator
  def initialize(streams)
    @streams = streams
  end

  def get_activities
    activities = []
    while more_activities? do
      activities << next_activity
    end
    activities
  end

private
  def next_activity
    @streams.select{ |s| has_next?(s) }.
      sort_by{ |s| s.first.created_at }.
      last.next
  end

  def more_activities?
    @streams.any?{ |s| has_next?(s) }
  end

  def has_next?(stream)
    stream.peek
    true
  rescue
    false
  end
end