class Post < ApplicationRecord
	include Notifiable
	include Likeable	
	# Rewards
	include Rewardable
	def get_points
		35
	end
	def get_user
		if "User" == owner.class.name
			owner
		else
			nil
		end
	end
	# End of Rewards	
	is_impressionable	
	belongs_to :owner, polymorphic: true
	belongs_to :wall, polymorphic: true
	has_many :comments, as: :commentable, dependent: :destroy
	has_many :attachments, as: :attachable, dependent: :destroy
	
	after_create :insert_post_in_users_newsfeeds
	after_destroy :remove_post_from_users_newsfeeds

	def insert_post_in_users_newsfeeds
		NewsFeedFanOutOnWriteWorker.perform_async(wall_id, wall_type, id, 'create')
	end
	
	def remove_post_from_users_newsfeeds
		NewsFeedFanOutOnWriteWorker.perform_async(wall_id, wall_type, id, 'destroy')
	end	


	def get_affected_users
		users = []

		# Wall owner
		if 'User' == wall.class.name
			users << wall.id
		elsif 'Group' == wall.class.name  || 'Page' == wall.class.name
			# This might be way too many notifications
			users << User.includes(:follows).where('follows.followable_id' => wall.id).where('follows.followable_type' => wall.class.name).pluck(:id)
		end
		# people who liked post
		users << likes.pluck(:user_id)
		
		# People who commented on post
		users << comments.pluck(:user_id)
		
		# People who liked comments
		users << comments.collect{|comment| comment.likes}.flatten.pluck(:user_id)
	
		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
		wall_name = wall.try(:name) || wall.try(:username)
		owner_name = owner.try(:username) || wall.try(:username)

	    if transaction_include_any_action?([:create])
			message = "#{owner.username} has created a post on #{wall_name}'s wall."
	    elsif transaction_include_any_action?([:update])
			message = "#{owner.username} has modified a post on #{wall_name}'s wall."
	    end
	    
	    current_user = User.current

		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)

	end		
end
