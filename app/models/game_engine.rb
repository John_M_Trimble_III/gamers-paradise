class GameEngine < ApplicationRecord
	include ModelDraftable
	has_many :game_engines_video_games, dependent: :destroy
	has_many :video_games, through: :game_engines_video_games
end
