class Draft < ApplicationRecord
	belongs_to :original, polymorphic: true, optional: true
	belongs_to :modified, polymorphic: true, optional: true
	belongs_to :user, optional: true

	has_many :contributions

	scope :new_games, -> { where(:type_of => "New").distinct }

	# if original_id is null use id instead
	scope :grouped_drafts, -> { select('MIN(id) as id, COUNT(*) AS count, MIN(original_id) AS original_id, MIN(original_type) AS original_type, MIN(type_of) AS type_of, MIN(modified_type) AS modified_type, MIN(modified_id) AS modified_id, MIN(updated_at) AS updated_at').group('(CASE WHEN (original_id IS NOT NULL) THEN original_id ELSE id END), (CASE WHEN (original_id IS NOT NULL) THEN original_type END)') }

	def get_next_draft
		original = self.original
		if original 
			drafts = original.drafts
			drafts.find{|draft| draft.id != self.id }
		else
			nil
		end
	end
end
