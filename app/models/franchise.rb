class Franchise < ApplicationRecord
	include ModelDraftable	
	is_impressionable
	
	has_many :franchises_video_games, dependent: :destroy
	has_many :video_games, through: :franchises_video_games
end
