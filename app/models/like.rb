class Like < ApplicationRecord
	include Notifiable	
	# Rewards
	include Rewardable
  	def get_points
  		45
  	end
	# End of Rewards
	validates :user_id, :uniqueness => { :scope => [:likeable_type, :likeable_id] }
	
	belongs_to :user
	belongs_to :likeable, polymorphic: true

	def get_affected_users
		users = []

		owner = likeable.try(:owner) || likeable.try(:user)
		users << owner.id if owner

		users.flatten.uniq
	end

	def create_notifications
	    if transaction_include_any_action?([:create])
	    	if "ListVideoGame" == likeable.class.name
				message = "#{user.username} has upvoted #{likeable.class.name.underscore.humanize.titleize} on #{likeable.list.name}."
	    	else
				message = "#{user.username} has liked your #{likeable.class.name.underscore.humanize.titleize}."
	    	end
	    elsif transaction_include_any_action?([:update])
			message = "#{user.username} has somehow edited their like of your #{likeable.class.name.underscore.humanize.titleize}."
	    end
	    
	    current_user = User.current
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)	
	end
end
