class CompaniesVideoGame < ApplicationRecord
  belongs_to :video_game
  belongs_to :company
  enum type_of: [:publisher, :main_developer, :supporting_developer, :porting_developer]

  scope :main_developer, -> {where('? = ANY(type_ofs)', :main_developer) }
  scope :publisher, -> {where('? = ANY(type_ofs)', :publisher) }
  scope :supporting_developer, -> {where('? = ANY(type_ofs)', :supporting_developer) }
  scope :porting_developer, -> {where('? = ANY(type_ofs)', :porting_developer) }

end
