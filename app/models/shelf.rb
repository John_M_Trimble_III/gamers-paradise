class Shelf < ApplicationRecord
	# Rewards
	include Rewardable
	def get_points
		35
	end
	# End of Rewards

	validates :name, presence: true
	validates :name, uniqueness: { scope: :user}
	
	belongs_to :user
	
	has_many :shelved_games, dependent: :destroy
	has_many :video_games, through: :shelved_games

	scope :played, -> { where(name: "Played")}
	scope :playing, -> { where(name: "Playing")}
	scope :will_play, -> { where(name: "Will Play")}

	def find_shelved_game(video_game)
		shelved_games.find_by_video_game_id(video_game.id)
	end

end
