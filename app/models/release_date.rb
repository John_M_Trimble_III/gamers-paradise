class ReleaseDate < ApplicationRecord
	belongs_to :releaseable, polymorphic: true
	belongs_to :platform
end
