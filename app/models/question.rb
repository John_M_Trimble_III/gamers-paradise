class Question < ApplicationRecord
	include Likeable
	include Notifiable
	# Rewards
	include Rewardable
	def get_points
		45
	end
	# End of Rewards	
	is_impressionable	
	belongs_to :questionable, polymorphic: true
	belongs_to :user
	has_many :answers, dependent: :destroy

	scope :answered, -> { left_outer_joins(:answers).where("answers.id IS NOT NULL").uniq }
	scope :unanswered, -> { left_outer_joins(:answers).where("answers.id IS NULL").uniq }	

	def get_affected_users
		users = []

		# Users that follow questionable
		users << User.includes(:follows).where('follows.followable_id' => questionable.id).where('follows.followable_type' => questionable.class.name).pluck(:id)

		# Users who liked the question
		users << likes.pluck(:user_id)

		# Users who have liked the answer
		users << answers.collect{|answer| answer.likes}.flatten.pluck(:user_id)

		# Users who have answered the question
		users << answers.pluck(:user_id)

		# Users who have commented on the answer
		users << answers.collect{|answer| answer.comments}.flatten.pluck(:user_id)

		# Users who have liked answer comments
		users << answers.collect{|answer| answer.comments}.flatten.collect{|comment| comment.likes}.flatten.pluck(:user_id)

		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    current_user = User.current

	    if transaction_include_any_action?([:create])
			message = "#{current_user.username} has asked a question about #{questionable.name}."
	    elsif transaction_include_any_action?([:update])
			message = "#{current_user.username} has modified their question about #{questionable.name}."
	    end
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)	    
	end		
end
