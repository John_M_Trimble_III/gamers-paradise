class Referral < ApplicationRecord
	# uses the default 100 points
	include Rewardable
	def get_user
		[referrer, referee]
	end

	belongs_to :referrer, class_name: 'User'
	belongs_to :referee, class_name: 'User'
end
