class Rating < ApplicationRecord
	include Notifiable
	
	# Rewards
	include Rewardable
  	def get_points
  		55
  	end
	# End of Rewards

	belongs_to :user
	belongs_to :rateable, polymorphic: true, counter_cache: true

	validates :user_id, :uniqueness => { :scope => [:rateable_type, :rateable_id] }

	after_commit :update_rateable

	def get_affected_users
		users = []

		users << User.includes(:follows).where('follows.followable_id' => rateable_id).where('follows.followable_type' => rateable_type).pluck(:id)

		users.flatten.uniq
	end

	#  How to get affected users after record is deleted?

	def create_notifications
	    current_user = User.current
	    if transaction_include_any_action?([:create])
			message = "#{current_user.username} has rated #{rateable.name} #{score} out of 10."
	    elsif transaction_include_any_action?([:update])
			message = "#{current_user.username} has changed their rating of #{rateable.name} to #{score} out of 10."
	    end
		GenerateNotificationsWorker.perform_async(self.class.name, self.id, message, current_user.id)	    
	end	

	def update_rateable
		rateable.update_column(:average_rating, rateable.calculate_average_rating)
	end

end
