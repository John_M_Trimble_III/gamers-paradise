class User < ApplicationRecord 
  # Rewards
  include Rewardable
  def get_points
    50
  end
  def get_user
    self
  end
  # End of Rewards
  extend FriendlyId
  friendly_id :username,  use: [:slugged, :finders] 

  validates :username, presence: true, uniqueness: true
  attribute :delete_cover_photo, :boolean
  attribute :delete_avatar, :boolean
  has_secure_token :referral_token
  has_secure_token :api_token

  def self.create_from_omniauth(params)
    attributes = {
      email: params['info']['email'],
      password: Devise.friendly_token,
      username: "gamer-#{User.last.id + 1}"
    }

    create(attributes)
  end


  def self.serialize_from_session key, salt
    record = where(id: key).eager_load(:friendships).eager_load(friendships: :friend).eager_load(inverse_friendships: :user).first
    record if record && record.authenticatable_salt == salt
  end


  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  after_create :after_user_created
  after_initialize :set_default_role, :if => :new_record?
  is_impressionable


  mount_uploader :avatar, AvatarUploader
  mount_uploader :cover_photo, BannerUploader

  devise :omniauthable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,  omniauth_providers: [:facebook, :twitch]

  has_many :authentications, class_name: 'UserAuthentication', dependent: :destroy

  has_many :listings, dependent: :destroy
  
  has_many :shelves, dependent: :destroy
  has_many :shelved_games, through: :shelves
  has_many :video_games, through: :shelved_games

  has_one :playing_shelf, -> { where(name: "Playing") }, class_name: "Shelf"
  has_one :played_shelf, -> { where(name: "Played") }, class_name: "Shelf"
  has_one :will_play_shelf,  -> { where(name: "Will Play") }, class_name: "Shelf"
  has_many :custom_shelves, -> {where(editable: true)}, class_name: "Shelf"

  has_many :playing_games, through: :playing_shelf, source: :video_games
  has_many :played_games, through: :played_shelf, source: :video_games
  has_many :planned_games, through: :will_play_shelf, source: :video_games

  has_many :ratings, dependent: :destroy
  has_many :rated_games, through: :ratings, source: :rateable, source_type: "VideoGame"
  has_many :member_reviews, dependent: :destroy
  has_many :reviewed_games, through: :member_reviews, source: :video_game

  enum role: {:user => 0, :arbiter => 1, :admin => 2}

  has_many :follows, as: :follower, dependent: :destroy
  has_many :followed_users, through: :follows, source: :followable, source_type: "User"
  has_many :followed_video_games, through: :follows, source: :followable, source_type: "VideoGame"
  has_many :followed_pages, through: :follows, source: :followable, source_type: "Page"
  has_many :followed_groups, through: :follows, source: :followable, source_type: "Group"

  has_many :following_users, as: :followable, class_name: "Follow"
  has_many :followers, through: :following_users, source_type: "User"

  has_many :friendships, dependent: :destroy
  has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"

  has_many :comments, dependent: :destroy

  has_many :posts, as: :owner, dependent: :destroy
  has_many :wall_posts, as: :wall, class_name: "Post", dependent: :destroy

  belongs_to :favorite_game, class_name: "VideoGame", optional: true

  has_many :contributions, dependent: :destroy

  has_many :likes, dependent: :destroy

  has_many :user_video_game_recommendations, dependent: :destroy
  has_many :video_game_recommendations, through: :user_video_game_recommendations, source: :video_game

  has_many :memberships, dependent: :destroy
  has_many :groups, through: :memberships

  has_many :notifications, foreign_key: :recipient_id, dependent: :destroy
  has_many :unread_notifications,   -> { where read_at: nil }, class_name: "Notification",  foreign_key: :recipient_id

  has_many :referees, through: :referrals, source: :referee
  
  has_many :referrals, foreign_key: :referrer_id, dependent: :destroy
  has_many :referreds, foreign_key: :referee_id, class_name: "Referral", dependent: :destroy

  has_many :orders

  has_many :user_rewards,  class_name: "Reward"

  def after_user_created
    AfterUserCreatedWorker.perform_async(self.id)
  end

  def set_default_role
    self.role ||= :user
  end

  def create_default_shelves
    self.shelves.create(name: "Played", editable: false)
    self.shelves.create(name: "Playing", editable: false)
    self.shelves.create(name: "Will Play", editable: false)
  end

  def make_friends_with_owners
    owners = User.with_role(:owner)
    owners.each do |owner|
      Friendship.create(user: self, friend: owner, created_at: Time.now, updated_at: Time.now, confirmed: true)
      User.current=self
    end
  end
  
  def follow_all_pages_by_default
    Page.all.each do |page|
      Follow.create(follower: self, followable: page)
    end
  end

 def friends
    friends_array = friendships.map{|friendship| friendship.friend if friendship.confirmed}
    friends_array = friends_array + inverse_friendships.map{|friendship| friendship.user if friendship.confirmed}
    friends_array.compact
  end

  # Users who have yet to confirm friend requests
  def pending_friends
    friendships.map{|friendship| friendship.friend if !friendship.confirmed}.compact
  end

  # Users who have requested to be friends
  def friend_requests
    inverse_friendships.map{|friendship| friendship.user if !friendship.confirmed}.compact
  end

  def confirm_friend(user)
    friendship = inverse_friendships.find{|friendship| friendship.user == user}
    friendship.confirmed = true
    friendship.save
  end

  def friend?(user)
    friends.include?(user)
  end

  def preexsiting_shelved_game(game)
    # First grab list of shelves
    shelved_games = self.playing_shelf.shelved_games + self.played_shelf.shelved_games + self.will_play_shelf.shelved_games
    shelved_game = shelved_games.select do |v| v.video_game_id == game.id end
    if shelved_game.empty?
      nil
    else
      shelved_game.first
    end
  end

  def get_points_deprectated
    points = self.contributions.sum(:points)
    points += self.referrals.sum(:points)
  end


  def self.current
    Thread.current[:user]
  end
  
  def self.current=(user)
    Thread.current[:user] = user
  end

  def get_magento_admin_token 
    response = HTTParty.post("#{Rails.application.config.magento_url}/rest/V1/integration/admin/token", :headers => { "Content-Type" => "application/json"}, 
      :body => {
        username: ENV['MAGENTO_ADMIN_USERNAME'],
        password: ENV['MAGENTO_ADMIN_PASSWORD']
      }.to_json
    )    

    return response.parsed_response
  end

  def create_or_connect_user_in_magento
    # Try to make the user in Magento
    auth_token = get_magento_admin_token

    query = "searchCriteria[filterGroups][0][filters][0][field]=email"
    query += "&searchCriteria[filterGroups][0][filters][0][value]=#{self.email}"
    # First let's check if the customer already exists
    # GET /V1/customers/search
    response = HTTParty.get("#{Rails.application.config.magento_url}/rest/V1/customers/search?#{query}",   
      headers: {
        "Authorization" => "Bearer #{auth_token}"
      }      
    )

    if response.parsed_response['total_count'] > 0
      customer = response.parsed_response['items'].first
    else
      customer_data = {
        "customer" => {
          "email" => self.email,
          "firstname" => self.username,
          "lastname" => self.username,
          "storeId" => 1,
          "websiteId" => 1
        }
      }

      response = HTTParty.post("#{Rails.application.config.magento_url}/rest/V1/customers", :headers => { "Authorization" => "Bearer #{auth_token}", "Content-Type" => "application/json" }, 
        :body => customer_data.to_json
      )
      return if response.code != 200
      customer = JSON.parse(response.body)
    end

    token = customer['custom_attributes'].select do |custom_attribute| 
      custom_attribute["attribute_code"] == "gp_token" 
    end

    self.update_columns(magento_token: token.first['value'], magento_id: customer['id'])    
  end


  def update_user_points_and_level_in_magento
    # Obtain Admin token
    auth_token = get_magento_admin_token
    # Grab the customer first

    query = "searchCriteria[filterGroups][0][filters][0][field]=email"
    query += "&searchCriteria[filterGroups][0][filters][0][value]=#{self.email}"
    # First let's check if the customer already exists
    # GET /V1/customers/search
    response = HTTParty.get("#{Rails.application.config.magento_url}/rest/V1/customers/search?#{query}",   
      headers: {
        "Authorization" => "Bearer #{auth_token}"
      }      
    )

    customer_data = {
        "customer" => response.parsed_response['items'].first
      }

    customer_data['customer']['custom_attributes'] = [
      {
        "attributeCode" => "gp_points",
        "value" => "#{self.points}"
      },
      {   
        "attributeCode" => "gp_level",
        "value" => "#{self.get_current_level}"
      }
    ]

    # PUT /V1/customers/{id}
    HTTParty.put("#{Rails.application.config.magento_url}/rest/V1/customers/#{self.magento_id}", :headers => { "Authorization" => "Bearer #{auth_token}", "Content-Type" => "application/json" }, 
      :body => customer_data.to_json
    )

  end


  # Gamification
  def get_current_level
    (0.05 * Math.sqrt(points)).floor
  end

  def get_points_to_level_up
    (((get_current_level + 1) / 0.05) ** 2).floor
  end

  def get_current_level_min 
        (((get_current_level) / 0.05) ** 2).floor
  end
  def get_percent_progress
    min = get_current_level_min.to_f
    (((points - min) / (get_points_to_level_up - min)) * 100.0).round(2)
  end
  # End Gamification

end
