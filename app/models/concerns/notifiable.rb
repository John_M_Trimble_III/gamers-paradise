module Notifiable
  extend ActiveSupport::Concern
  
	included do 
		after_commit :create_notifications, on: [:create, :update]
	end 

end