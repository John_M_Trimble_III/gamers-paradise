module Rewardable
  extend ActiveSupport::Concern
  
	included do 
		has_many :rewards, as: :rewardable, dependent: :destroy
		after_commit :reward!, on: [:create, :update]		
  	end 

	#  default 100 points
  	def get_points
  		100
  	end
  	def get_user
  		user
  	end

	def reward!
		# Either grab current reward or update to new points
		if rewards.any?
			rewards.each do |reward|
				reward.points = get_points
				reward.save				
			end
		else
			if (users = get_user) && (users.is_a?(Array))
				users.each do |user|
					reward = rewards.build
					reward.points = get_points
					reward.user = user
					reward.save		
				end
			elsif user = get_user
				reward = rewards.build
				reward.points = get_points
				reward.user = user
				reward.save	
			end
		end
	end
end