module Questionable
  extend ActiveSupport::Concern
  
	included do 
		has_many :questions, as: :questionable	
  	end 

end