module ModelDraftable
  	extend ActiveSupport::Concern

	included do 
		has_many :drafts, as: :original
		has_many :parent_drafts, as: :modified, class_name: "Draft"
  		has_many :contributions, as: :contributable, dependent: :destroy
  		has_many :contributors, through: :contributions, source: :user  		
  		scope :published, -> {   includes(:parent_drafts).where(drafts: { id: nil })}  		
  		scope :search_import, -> {   includes(:parent_drafts).where(drafts: { id: nil })}     
  	end 


	def published? 
		self.parent_drafts.empty?
	end

	def should_index?
		published? # only index active records
	end  

	# This should easily handle instances where stuff is ok
	def detect_changes(modified)
	    changed_attributes = []

	    main_changed_attributes = (self.attributes.to_a - modified.attributes.to_a).map(&:first)
	    main_changed_attributes.delete("id")
	    main_changed_attributes.delete("created_at")
	    main_changed_attributes.delete("updated_at")
	    changed_attributes += main_changed_attributes
	    return changed_attributes
	end		

	class_methods do
		def accept(draft)
			modified = draft.modified
	  		if original = draft.original # We updating bitch
	  			changes = draft.object
	  			if changes
	  				changes.each do |change|
	  					original[change] = modified[change]
	  				end
	  			end

	  			if status = original.save
	  				if changes
	  					changes.each do |change|
	  						# We need to apply each change to the original
          					Contribution.create({contributable: original, user: draft.user, field: change, content: modified[change], action: :modify, points: 5})
	  					end
	  				end
	  				modified.destroy
	  			end
	  		else
				Contribution.create({contributable: modified, user: draft.user, field: "new_#{modified.class.name}".parameterize.underscore, content: modified.name, action: :add, points: 25}) # We are going to award 50 points to brand new additions
	    		status = true
	  		end
	  		draft.destroy if status
			return status
		end
	end	

end