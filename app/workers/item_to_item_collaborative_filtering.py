from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey, DateTime
from sqlalchemy.orm import sessionmaker
import pandas as pd
import os 
from surprise import Reader
from surprise import Dataset
from surprise.model_selection import cross_validate
from surprise import NormalPredictor
from surprise import SVD, evaluate
from collections import defaultdict
import numpy as np
import datetime
import pdb
import os

def get_top_n(predictions, n=50):
    '''Return the top-N recommendation for each user from a set of predictions.

    Args:
        predictions(list of Prediction objects): The list of predictions, as
            returned by the test method of an algorithm.
        n(int): The number of recommendation to output for each user. Default
            is 10.

    Returns:
    A dict where keys are user (raw) ids and values are lists of tuples:
        [(raw item id, rating estimation), ...] of size n.
    '''

    # First map the predictions to each user.
    top_n = defaultdict(list)
    for uid, iid, true_r, est, _ in predictions:
        top_n[uid].append((iid, est))

    # Then sort the predictions for each user and retrieve the k highest ones.
    for uid, user_ratings in top_n.items():
        user_ratings.sort(key=lambda x: x[1], reverse=True)
        top_n[uid] = user_ratings[:n]

    return top_n


db_url = os.environ.get('DATABASE_URL')

# Returns the list top 3 elements or entire list; whichever is more.
engine = create_engine(db_url)

ratings = pd.read_sql_query("SELECT score, user_id, rateable_id as video_game_id FROM ratings WHERE rateable_type = 'VideoGame'",con=engine)

users = pd.read_sql_query("SELECT id FROM users",con=engine)
video_games = pd.read_sql_query("SELECT video_games.id FROM video_games LEFT OUTER JOIN drafts ON drafts.modified_id = video_games.id AND drafts.modified_type = 'VideoGame' WHERE drafts.id IS NULL",con=engine)

games_to_predict = []
for index, row in users.iterrows():
	for inner_index, inner_row in video_games.iterrows():
		games_to_predict.append([row.id, inner_row.id])

df1 = pd.DataFrame(games_to_predict, columns=['user_id','video_game_id'])

df2 = pd.merge(df1, ratings[['user_id','video_game_id']], how='left', indicator='Exist')
df2['Exist'] = np.where(df2.Exist == 'both', True, False)
df2 = df2[df2.Exist==False][['user_id', 'video_game_id']]
df2['score'] = 0.0

reader = Reader(rating_scale=(0, 10))

# The columns must correspond to user id, item id and ratings (in that order).
data = Dataset.load_from_df(ratings[['user_id', 'video_game_id', 'score']], reader)
trainset = data.build_full_trainset()

algo = SVD()
algo.fit(trainset)



predictions = algo.test(df2.values)

top_n = get_top_n(predictions, n=50)

metadata = MetaData()

user_video_game_recommendations = Table('user_video_game_recommendations', metadata,
    Column('id', Integer, primary_key=True),
    Column('user_id', Integer),
    Column('video_game_id', Integer),
    Column('created_at', DateTime), 
    Column('updated_at', DateTime)
    )


# Build insertion list
recs_to_add = []
for uid, user_ratings in top_n.items():
    for user_rating in user_ratings:
        recs_to_add.append({'user_id': int(uid), 'video_game_id': int(user_rating[0]), 'created_at' : 'NOW()', 'updated_at' : 'NOW()'})


Session = sessionmaker(bind=engine)
session = Session()
session.execute('''TRUNCATE TABLE user_video_game_recommendations''')
session.commit()
session.close()
conn = engine.connect()
conn.execute(user_video_game_recommendations.insert(),recs_to_add)




















