class SyncPointsToUserWorker
  include Sidekiq::Worker

  def perform(user_id)
  	user = User.find(user_id)
  	user.update_user_points_and_level_in_magento
  end
end
