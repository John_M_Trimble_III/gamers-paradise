class GenerateNotificationsWorker
  include Sidekiq::Worker

  def perform(notifiable_type, notifiable_id, message, actor_id, user_ids = [])    
    

    if (notifiable_type) && (notifiable = notifiable_type.constantize.find_by_id(notifiable_id))
      notifiable.get_affected_users.each do |recipient_id|
      # For production if recipient_id and actor_id are equal call next
        next if recipient_id == actor_id

        notification = Notification.create({
          actor_id: actor_id,
          recipient_id: recipient_id,
          action: message,
          notifiable_type: notifiable_type,
          notifiable_id: notifiable_id
        })
      
        # Succesfully created notification
        if notification.valid? && recipient = User.find_by_id(recipient_id)
          html = ApplicationController.render(   partial: 'notifications/notification',   locals: { notification: notification } )
          NotificationsChannel.broadcast_to(recipient, { html: html, count: recipient.unread_notifications.count})
        end      

      end

    else
      user_ids.each do |recipient_id|
        notification = Notification.create({
          actor_id: actor_id,
          recipient_id: recipient_id,
          action: message,
          notifiable_type: nil,
          notifiable_id: nil        
        })
        
        # Succesfully created notification
        if notification.valid? && recipient = User.find_by_id(recipient_id)
          html = ApplicationController.render(   partial: 'notifications/notification',   locals: { notification: notification } )
          NotificationsChannel.broadcast_to(recipient, { html: html, count: recipient.unread_notifications.count})
        end        
      end
    end
  end

end
