class SitemapGeneratorWorker
	require 'rake'
  	include Sidekiq::Worker

  	def perform(*args)
	    # Do something
	    GamersParadise::Application.load_tasks
	    Rake::Task['sitemap:refresh'].invoke('s')
  	end
end
