from sqlalchemy import create_engine,  Table, Column, Integer, String, MetaData, ForeignKey, DateTime
from sqlalchemy.orm import sessionmaker
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import pdb
import os

def create_soup(x):
	val = ' '.join(x['name_genre']) + ' ' + ' '.join(x['name_game_engine']) + ' ' + ' '.join(x['name_company']) + ' ' + ' '.join(x['name_game_mode']) + ' ' + ' '.join(x['name_player_perspective']) + ' ' + ' '.join(x['name_series']) + ' ' + ' '.join(x['name_franchise'])
	return val

db_url = os.environ.get('DATABASE_URL')
# Returns the list top 3 elements or entire list; whichever is more.
engine = create_engine(db_url)

video_games = pd.read_sql_query("SELECT video_games.* FROM video_games LEFT OUTER JOIN drafts ON drafts.modified_id = video_games.id AND drafts.modified_type = 'VideoGame' WHERE drafts.id IS NULL",con=engine)
 
genres_video_games = pd.read_sql_query('select video_game_id, genre_id from genres_video_games',con=engine)
genres = pd.read_sql_query('select id, name from genres',con=engine)
genres = genres_video_games.merge(genres, left_on='genre_id', right_on='id').groupby('video_game_id').agg(lambda x: list(x))

game_engines_video_games = pd.read_sql_query('select video_game_id, game_engine_id from game_engines_video_games',con=engine)
game_engines = pd.read_sql_query('select id, name from game_engines',con=engine)
game_engines = game_engines_video_games.merge(game_engines, left_on='game_engine_id', right_on='id').groupby('video_game_id').agg(lambda x: list(x))

companies_video_games = pd.read_sql_query('select video_game_id, company_id from companies_video_games',con=engine)
companies = pd.read_sql_query('select id, name from companies',con=engine)
companies = companies_video_games.merge(companies, left_on='company_id', right_on='id').groupby('video_game_id').agg(lambda x: list(x))

game_modes_video_games = pd.read_sql_query('select video_game_id, game_mode_id from game_modes_video_games',con=engine)
game_modes = pd.read_sql_query('select id, name from game_modes',con=engine)
game_modes = game_modes_video_games.merge(game_modes, left_on='game_mode_id', right_on='id').groupby('video_game_id').agg(lambda x: list(x))

player_perspectives_video_games = pd.read_sql_query('select player_perspective_id, video_game_id from player_perspectives_video_games',con=engine)
player_perspectives = pd.read_sql_query('select id, name from player_perspectives',con=engine)
player_perspectives = player_perspectives_video_games.merge(player_perspectives, left_on='player_perspective_id', right_on='id').groupby('video_game_id').agg(lambda x: list(x))

series_video_games = pd.read_sql_query('select video_game_id, series_id from series_video_games',con=engine)
series = pd.read_sql_query('select id, name from series',con=engine)
series = series_video_games.merge(series, left_on='series_id', right_on='id').groupby('video_game_id').agg(lambda x: list(x))

franchises_video_games = pd.read_sql_query('select video_game_id, franchise_id from franchises_video_games',con=engine)
franchises = pd.read_sql_query('select id, name from franchises',con=engine)
franchises = franchises_video_games.merge(franchises, left_on='franchise_id', right_on='id').groupby('video_game_id').agg(lambda x: list(x))

video_games_complete = video_games.merge(genres, left_on='id', right_on='video_game_id', how='left', suffixes=('','_genre')).merge(game_engines, left_on='id', right_on='video_game_id', how='left', suffixes=('','_game_engine')).merge(companies, left_on='id', right_on='video_game_id', how='left', suffixes=('','_company')).merge(game_modes, left_on='id', right_on='video_game_id', how='left', suffixes=('','_game_mode')).merge(player_perspectives, left_on='id', right_on='video_game_id', how='left', suffixes=('','_player_perspective')).merge(series, left_on='id', right_on='video_game_id', how='left', suffixes=('','_series')).merge(franchises, left_on='id', right_on='video_game_id', how='left', suffixes=('','_franchise'))
cleaned_video_games = video_games_complete[['id', 'name', 'description', 'name_genre', 'name_game_engine', 'name_company', 'name_game_mode', 'name_player_perspective', 'name_series', 'name_franchise']]
cleaned_video_games = cleaned_video_games.replace(np.nan, '', regex=True)
cleaned_video_games['soup'] = cleaned_video_games.apply(create_soup, axis=1)

count = CountVectorizer(stop_words='english')
count_matrix = count.fit_transform(cleaned_video_games['soup'])
cosine_sim2 = cosine_similarity(count_matrix, count_matrix)

# Function that takes in movie title as input and outputs most similar movies
def get_recommendations(id, cosine_sim=cosine_sim2, num_results=25):
    # Get the index of the movie that matches the title

    idx = indices[id]

    # Get the pairwsie similarity scores of all movies with that movie
    sim_scores = list(enumerate(cosine_sim2[idx]))

    # Sort the movies based on the similarity scores
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)

    sim_scores = sim_scores[1:num_results]

    # Get the movie indices
    movie_indices = [i[0] for i in sim_scores]

    # Return the top 10 most similar movies
    return cleaned_video_games['id'].iloc[movie_indices]

# Reset index of your main DataFrame and construct reverse mapping as before
cleaned_video_games = cleaned_video_games.reset_index()

indices = pd.Series(cleaned_video_games.index, index=cleaned_video_games['id'])

similar_games = []

for index, row in video_games.iterrows():
    for rec in get_recommendations(row.id):
        similar_games.append({'video_game_id' : row.id, 'recommendation_id' : rec, 'created_at' : 'NOW()', 'updated_at' : 'NOW()'})

metadata = MetaData()

similar_video_games = Table('similar_video_games', metadata,
    Column('id', Integer, primary_key=True),
    Column('video_game_id', Integer),
    Column('recommendation_id', Integer),
    Column('created_at', DateTime), 
    Column('updated_at', DateTime)
    )


conn = engine.connect()
Session = sessionmaker(bind=engine)
session = Session()
session.execute('''TRUNCATE TABLE similar_video_games''')
session.commit()
session.close()

conn.execute(similar_video_games.insert(), similar_games)










