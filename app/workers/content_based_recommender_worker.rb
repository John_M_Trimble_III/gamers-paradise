class ContentBasedRecommenderWorker
  include Sidekiq::Worker

  def perform(*args)
    # Do something
	script_path = File.join(File.dirname(__FILE__), 'content_based_recommender.py')
	system("python #{script_path}")      
  end
end
