class NewsAggregatorWorker
  include Sidekiq::Worker
  require 'rss'

  def perform(*args)
    # Do something
	pages = Page.all
	pages.each do |page|

		rss_url = page.rss_url
		retries = 0

		if rss_url
			begin
				response = HTTParty.get(rss_url)
				feed = Feedjira::Feed.parse(response.body)
			rescue
		  		retry if (retries += 1) < 5 # Retry 5 times
			end
			if feed.try(:entries)
				feed.entries.each do |item|
					retries = 0
					begin 
						# Post.find_by(external_id: )
						# Use either GUID or ID depends on what is available
						id = item.id ? item.id : item.guid
						unless Post.find_by(external_id: id, owner: page)
							post = Post.new
							post.title = item.title			
							body = item.content ? item.content : item.summary	
							post.content = body.gsub(/<img\/?[^>]*>/, "").gsub(/<video\/?[^>]*>/, "").gsub(/<iframe\/?[^>]*>/, "")

							body = Nokogiri::HTML( body )

							images = body.css('img').map{|img| img['src']}# Array of strings
							# Add media:content to images as well
							if images.empty? and item.respond_to?(:image) and  not item.image.nil?
								images << item.image
							end
							images.each do |img| # img source
								unless img.include? "http"
									img = img.prepend("http:")
								end
								img = URI.encode(img)
								file_name = URI.unescape(File.basename(img)).gsub(/[\x00\/\\:\*\?\"<>\|\(\)]/, '_')
								file_path = Rails.root.join("tmp", "images", file_name)
								dirname = File.dirname(file_path)
			  					FileUtils.mkdir_p(dirname) unless File.directory?(dirname)
								File.open(file_path,'wb'){ |f| f.write(open(img, "User-Agent" => RandomUserAgent.randomize()).read) }	
								image_size = FastImage.size(file_path)
								if image_size.first > 1 and image_size.second  > 1
									new_attachment = post.attachments.new
									new_attachment.remote_file_url = img
									new_attachment.attachable = post
								end
							end

							videos = body.css('iframe').map{ |i| i['src'] } # Array of strings
							videos.each do |video|
								new_attachment = post.attachments.new
								youtube_id = video.split('/').last
								new_attachment.youtube_id = youtube_id
								new_attachment.attachable = post								
							end				

							post.date_published = item.published
							post.author = item.author

							# Model Related Stuff
							post.owner = page
							post.wall = page
							post.external_id = id
							post.external_link = item.url
							
							retries = 0
							post.save		
						end				
					rescue
						retry if (retries += 1) < 10 # Retry 5 times
					end
				end
			end
		end
	end
  end
end
