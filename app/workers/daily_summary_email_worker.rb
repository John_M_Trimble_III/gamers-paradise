class DailySummaryEmailWorker
  include Sidekiq::Worker

  def perform(*args)
    # Do something
    user_summary_mailer = UserSummaryMailer.new
    User.where(daily_summary_email_opt_in: true).each do |user|
    	p user_summary_mailer.daily_summary_email(user)
    end
  end
end
