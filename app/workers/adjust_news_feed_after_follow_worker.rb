class AdjustNewsFeedAfterFollowWorker
  include Sidekiq::Worker

  # User is obvious, followable is the thing follow and action is either a follow or unfollow
  def perform(follower_id, follower_type)
    # Do something
      
    if follower_type == "User"
    	user = follower_type.constantize.find_by_id(follower_id)
      if user 
        $redis.del "user-newsfeed-#{user.id}"
        # Need To Rerun ActivityAggregator to get things in the right order
        p_stream = Post.where(wall: user.followed_pages).order("created_at DESC").select(:id, :created_at).to_enum
        g_stream = Post.where(wall: user.followed_groups).order("created_at DESC").select(:id, :created_at).to_enum
        v_stream = Post.where(wall: user.followed_video_games).order("created_at DESC").select(:id, :created_at).to_enum 
        u_stream = Post.where(owner: user.followed_users).or(Post.where(wall: user.followed_users)).or(Post.where(wall: user)).order("created_at DESC").select(:id, :created_at).to_enum 
        # All the streams from all the followed objects
        sorted_posts = ActivityAggregator.new([p_stream, g_stream, v_stream, u_stream]).get_activities
        sorted_ids = sorted_posts.collect(&:id)
        $redis.lpush "user-newsfeed-#{user.id}", sorted_ids
      end
    else
    	"Something Bad This Way Comes"
    end
  end
end
