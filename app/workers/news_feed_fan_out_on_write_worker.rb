class NewsFeedFanOutOnWriteWorker
  include Sidekiq::Worker

  def perform(wall_id, wall_type, post_id, action)
    # Do something
	wall = wall_type.constantize.find(wall_id)   
	affected_users = User.includes(:follows).where('follows.followable_type' => wall.class.name).where('follows.followable_id' => wall.id)
	affected_users.each do |user|
		redis_list = "user-newsfeed-#{user.id}"
		if 'create' == action
			$redis.lpush redis_list, post_id
		elsif 'destroy' == action
			$redis.lrem redis_list, 0, post_id
		end
	end
  end
end
