class SearchkickIndexerWorker
  include Sidekiq::Worker

  def perform(*args)
    # Do somethin
    Searchkick::ProcessQueueJob.perform_later(class_name: "VideoGame")    
  end
end
