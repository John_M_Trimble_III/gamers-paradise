class ItemToItemCollaborativeFilteringWorker
  include Sidekiq::Worker

  def perform(*args)
    # Do something
	script_path = File.join(File.dirname(__FILE__), 'item_to_item_collaborative_filtering.py')
	system("python #{script_path}")    
  end
end
