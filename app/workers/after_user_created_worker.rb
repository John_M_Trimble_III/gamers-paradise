class AfterUserCreatedWorker
	include Sidekiq::Worker

	def perform(user_id)
		user = User.find(user_id)
		User.current = user
		user.create_default_shelves
		user.make_friends_with_owners
		user.follow_all_pages_by_default
		user.create_or_connect_user_in_magento	
	end
end
