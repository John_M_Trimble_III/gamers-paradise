class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@gamersparadise.io'
  layout 'mailer'
  def initialize
    @client = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY']).client
  end  
end
