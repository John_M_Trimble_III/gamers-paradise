class UserSummaryMailer < ApplicationMailer
	default from: 'no-reply@gamersparadise.io'
   	def daily_summary_email(user)
   		@user = user
      @url  = new_user_session_url
    	data = {
        personalizations: [
          {
            to: [
              {
                email: @user.email
              },
            ],
            dynamic_template_data: {
              username: @user.username,
              unread_notifications_count: @user.unread_notifications.count,
              pending_friend_requests_count: @user.friend_requests.count,
              pending_rewards_count: @user.user_rewards.where(seen_at: nil).count,
              current_level: @user.get_current_level,
              points: @user.points,
              points_remaining: @user.get_points_to_level_up - @user.points,
              new_posts_count: Post.where(created_at: (Time.now - 24.hours)..Time.now).count

            },
            subject: "Greetings From The World Of Yesterday"
          }
        ],
        from: {
          email: "no-reply@gamersparadise.io",
          name: "Gamer's Paradise"
        },
        template_id: "d-f0733237e28e431a90f2934915736974"
    	}
	    response = @client.mail._('send').post(request_body: data)
   end	
end
