module PostsHelper
	def get_owner_name(owner)
		if(owner.has_attribute?(:name) and owner.name)
			return owner.name
		elsif(owner.has_attribute?(:username) and owner.username)
			return owner.username
		elsif (owner.has_attribute?(:email) and owner.email)
			return owner.email
		else
			return "Anonymous"
		end
			
  	end
end
