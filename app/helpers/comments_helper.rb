module CommentsHelper
	def nested_comments(comments)
		if comments
	    	comments.map do |comment|
	      		(render partial: "comments/comment", locals: {comment: comment, commentable: comment.commentable}) + content_tag(:div, nested_comments(comment.children), :class => "nested-comments")
	    	end.join.html_safe
	  	end	
	end	
end