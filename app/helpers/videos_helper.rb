module VideosHelper
	def get_youtube_id(youtube_url)
		uri    = URI.parse(youtube_url)
		params = CGI.parse(uri.query)
		# params is now {"id"=>["4"], "empid"=>["6"]}
		params['v'].first
	end
end
