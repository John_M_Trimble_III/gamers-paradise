module AffiliatesHelper
	def generate_affiliate_link(release_date)
		if !release_date.asin.empty?
			# Generate Amazon Affiliate Link
			link_to "Buy on Amazon for #{release_date.platform.name}", "http://www.amazon.com/dp/#{release_date.asin}/?tag=gamerspara0a4-20", target: :_blank, class: "d-block"
		else
			# Not yet available on other platforms
		end
	end
end
