module MembershipsHelper
	def member?(group)
		(user_signed_in? && group.approved_members.include?(current_user)) || current_user == group.owner
	end

	def get_join_button(group)
		if group.pending_members.include?(current_user)
			link_to "Cancel Request", membership_path(Membership.find_by(user_id: current_user.id, group_id: @group.id)), method: :delete, class: "btn btn-secondary btn-block"
		elsif group.approved_members.include?(current_user)
			link_to "Leave", membership_path(Membership.find_by(user_id: current_user.id, group_id: @group.id)), method: :delete, class: "btn btn-danger btn-block"
		elsif group.rejected_members.include?(current_user)
			"Sorry your application was denied you are not allowed to request joining again"
		elsif group.owner == current_user
			nil
		else
			link_to "Join", group_memberships_path(group, user_id: current_user.id), method: :post, class: "btn btn-primary btn-block"
		end
	end

	def group_admin?(group)
		(group.admins.include?(current_user)) || (group.owner == current_user)
	end
end
