module FriendshipsHelper
	def find_friendship(friend)
		friendship = Friendship.where(user: current_user, friend: friend).first
		friendship = Friendship.where(friend: current_user, user: friend).first unless friendship
		friendship.id ||= nil
	end

	def get_friendship_button(friend)

		@friends ||= current_user.friends

		@pending_friends ||= current_user.pending_friends
		
		@friend_requests ||= current_user.friend_requests

		if (@friends.include?(friend)) # We friends
			content_tag :div, class: "dropdown" do
				button = button_tag class: "btn btn-block btn-secondary dropdown-toggle", data: {toggle: "dropdown"}, 'aria-haspopup'=> "true", 'aria-expanded' => "false" do
					fa_icon "check", text: "Friends"
				end 
				div = content_tag :div, class: "dropdown-menu" do
					link_to "Unfriend", { controller: :friendships, action: :destroy, id: find_friendship(friend) }, method: :delete, data: { confirm: 'Are you sure?' }, class: "dropdown-item"
				end
				button + div
			end
		elsif (@pending_friends.include?(friend)) # I've asked them to be my friend
			return button_to "Cancel Request", { controller: :friendships, action: :destroy, id: find_friendship(friend) }, method: :delete, data: { confirm: 'Are you sure?' }, class: "btn btn-secondary btn-block"
		elsif (@friend_requests.include?(friend)) # They asked me to be a friend
			return button_to "Confirm Friend", accept_friendship_path(find_friendship(friend)), class: 'btn btn-secondary btn-block'
		else
			return button_to friendships_path, class: 'btn btn-secondary btn-block', params: {user_id: current_user.id, friend_id: friend.id} do
				fa_icon 'user-plus', text: "Add Friend"
			end
		end	
	end
end
