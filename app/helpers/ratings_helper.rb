module RatingsHelper
	def getRatingObject(rateable)
		if (current_user && (rating = rateable.ratings.detect { |rating| rating.user_id == current_user.id }))
 			return rating
 		else
 			return Rating.new
 		end
	end
	def generateStars(rating,numStars)
		html = "<div class=\"br-wrapper br-theme-fontawesome-stars-o\"><div class=\"br-widget\">"
		numStars.times do |index|
			html += "<a href=\"#\" class=\""
			html += "br-selected" if index < rating 
			html += "\"></a>"
		end
		html += "</div></div>"
	end
end
