module VideogamesHelper
	def get_user_rating(video_game)
		avg_user_rating = (video_game.ratings.sum(:score).to_f + video_game.member_reviews.sum(:rating).to_f) / (10 * (video_game.ratings.count + video_game.member_reviews.count))
		return (avg_user_rating.nan? ? 0 : avg_user_rating).round(2)
	end
	def has_user_rating?(video_game)
		return (video_game.ratings.count > 0 or video_game.member_reviews.count > 0)
	end
	def get_avg_rating(video_game)
		avg_user_rating = 100 * ((video_game.ratings.sum(:score).to_f + video_game.member_reviews.sum(:rating).to_f) / (10 * (video_game.ratings.count + video_game.member_reviews.count)))
		return (avg_user_rating.nan? ? 0 : avg_user_rating).round(2)
	end		
end
